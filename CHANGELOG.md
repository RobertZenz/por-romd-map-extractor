Changelog
=========


1.2
---

 * Added GUI to view files.
 * CNK support.
 * INK support.
 * NAT support ("static" models only).
 * WMD support.

### Known Issues

 * Zooming too far out in the NAT/3D view will make the GUI unresponsive.
 * GUI might not be updated anymore after closing tab with NAT/3D view, resize
   the window once to fix this.
 * NAT/model support is limited to "static" models. The viewer will show all
   parts of all models as best as it can, the export will only contain "static"
   portions of these models.
 * WMD/map view is unoptimized and might lack. Also note that large maps will
   consume a lot of memory.
 * JVM process might not close after closing the viewer and must be ended
   through a process manager (Task Manager or MATE System Monitor, for example).


1.1
---

 * WMD support.


1.0
---

 * MCZ support.
