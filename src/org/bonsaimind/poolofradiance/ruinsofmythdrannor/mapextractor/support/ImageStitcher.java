/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.cnk.Cnk;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz.Mcz;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.Wmd;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ImageMosaic;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ImageMosaic.Bounds;

public class ImageStitcher {
	public ImageStitcher() {
		super();
	}
	
	public BufferedImage stitchImage(Cnk cnk) {
		ImageMosaic imageMosaic = new ImageMosaic();
		
		for (int entryIndex = 0; entryIndex < cnk.getEntriesCount(); entryIndex++) {
			Ink entryInk = cnk.getEntryInk(entryIndex);
			BufferedImage entryImage = entryInk.getImage();
			
			imageMosaic.add(entryImage);
		}
		
		imageMosaic.packAtlas();
		
		return stitchImage(imageMosaic);
	}
	
	public BufferedImage stitchImage(ImageMosaic imageMosaic) {
		ImageMosaic.Bounds mosaicBounds = imageMosaic.getMosaicBounds();
		
		BufferedImage image = createEmptyImage(
				mosaicBounds.getWidth(),
				mosaicBounds.getHeight());
		
		for (int index = 0; index < imageMosaic.getImagesCount(); index++) {
			BufferedImage singleImage = imageMosaic.getImage(index);
			Bounds singleImageBounds = imageMosaic.getImageBounds(index);
			
			int[] singleImageArgb = singleImage.getRGB(
					0,
					0,
					singleImage.getWidth(),
					singleImage.getHeight(),
					null,
					0,
					singleImage.getWidth());
			
			image.setRGB(
					singleImageBounds.getX() - mosaicBounds.getX(),
					singleImageBounds.getY() - mosaicBounds.getY(),
					singleImageBounds.getWidth(),
					singleImageBounds.getHeight(),
					singleImageArgb,
					0,
					singleImageBounds.getWidth());
		}
		
		return image;
	}
	
	public BufferedImage stitchImage(Mcz mcz) {
		BufferedImage image = createEmptyImage(
				Mcz.MCZ_IMAGE_WIDTH,
				Mcz.MCZ_IMAGE_HEIGHT);
		
		stitchMczInto(mcz, image, 0, 0);
		
		return image;
	}
	
	public BufferedImage stitchImage(Wmd wmd) {
		BufferedImage image = createEmptyImage(
				wmd.getMapColumns() * Mcz.MCZ_IMAGE_WIDTH,
				wmd.getMapRows() * Mcz.MCZ_IMAGE_HEIGHT);
		
		for (int mapTileEntryIndex = 0; mapTileEntryIndex < wmd.getMapTilesEntriesCount(); mapTileEntryIndex++) {
			int mapTileIndex = wmd.getMapTileMapping(mapTileEntryIndex);
			
			if (mapTileIndex >= 0) {
				Mcz mcz = wmd.getMapTileEntry(mapTileIndex).getMcz();
				int mczX = (mapTileEntryIndex % wmd.getMapColumns()) * Mcz.MCZ_IMAGE_WIDTH;
				int mczY = (mapTileEntryIndex / wmd.getMapColumns()) * Mcz.MCZ_IMAGE_HEIGHT;
				
				stitchMczInto(
						mcz,
						image,
						mczX,
						mczY);
			}
		}
		
		return image;
	}
	
	protected BufferedImage createEmptyImage(int width, int height) {
		BufferedImage image = new BufferedImage(
				width,
				height,
				BufferedImage.TYPE_INT_RGB);
		
		Graphics graphics = image.getGraphics();
		graphics.setColor(Color.PINK);
		graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
		
		return image;
	}
	
	protected void stitchMczInto(Mcz mcz, BufferedImage targetImage, int offsetX, int offsetY) {
		for (int entryIndex = 0; entryIndex < mcz.getEntriesCount(); entryIndex++) {
			BufferedImage entryImage = mcz.getEntryImage(entryIndex);
			int[] entryImageArgb = entryImage.getRGB(
					0,
					0,
					entryImage.getWidth(),
					entryImage.getHeight(),
					null,
					0,
					entryImage.getWidth());
			
			targetImage.setRGB(
					((entryIndex % Mcz.MCZ_IMAGE_COLUMNS) * Mcz.ENTRY_IMAGE_WIDTH) + offsetX,
					((entryIndex / Mcz.MCZ_IMAGE_COLUMNS) * Mcz.ENTRY_IMAGE_HEIGHT) + offsetY,
					entryImage.getWidth(),
					entryImage.getHeight(),
					entryImageArgb,
					0,
					entryImage.getWidth());
		}
	}
}
