/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ObjBundle {
	protected FileContent<String> mdlFileContent = null;
	protected FileContent<String> objFileContent = null;
	protected List<FileContent<byte[]>> textureFileContents = null;
	
	public ObjBundle(
			FileContent<String> objFileContent,
			FileContent<String> mdlFileContent,
			List<FileContent<byte[]>> textureFileContents) {
		super();
		
		this.objFileContent = objFileContent;
		this.mdlFileContent = mdlFileContent;
		this.textureFileContents = Collections.unmodifiableList(new ArrayList<>(textureFileContents));
	}
	
	public FileContent<String> getMdlFileContent() {
		return mdlFileContent;
	}
	
	public FileContent<String> getObjFileContent() {
		return objFileContent;
	}
	
	public List<FileContent<byte[]>> getTextureFileContents() {
		return textureFileContents;
	}
}
