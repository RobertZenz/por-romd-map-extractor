/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public final class ExceptionUtil {
	private ExceptionUtil() {
	}
	
	public static final String getAsText(Throwable throwable) {
		try (ByteArrayOutputStream stackTraceByteArrayOutputStream = new ByteArrayOutputStream()) {
			try (PrintWriter stackTracePrintWriter = new PrintWriter(stackTraceByteArrayOutputStream)) {
				throwable.printStackTrace(stackTracePrintWriter);
			}
			
			return new String(stackTraceByteArrayOutputStream.toByteArray(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	public static final Throwable getRootCause(Throwable throwable) {
		Throwable rootCause = throwable;
		
		while (rootCause.getCause() != null) {
			rootCause = rootCause.getCause();
		}
		
		return rootCause;
	}
}
