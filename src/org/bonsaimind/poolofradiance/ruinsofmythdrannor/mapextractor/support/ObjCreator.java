/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Face;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometrySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Nat;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.TextureCoordinate;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Transform;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Vertice;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support.PathUtil;

public class ObjCreator {
	
	public ObjCreator() {
		super();
	}
	
	public ObjBundle createObj(Nat nat, String baseFileName) {
		return new ObjBundle(
				createObjFileContent(nat, baseFileName),
				createMdlFileContent(nat, baseFileName),
				createTextureFileContents(nat));
	}
	
	protected void appendLine(StringBuilder stringBuilder, String format, Object... arguments) {
		stringBuilder.append(String.format(format, arguments));
		stringBuilder.append("\n");
	}
	
	protected FileContent<String> createMdlFileContent(Nat nat, String baseFileName) {
		StringBuilder mdlBuilder = new StringBuilder();
		
		appendLine(mdlBuilder, "# NAT %s",
				nat.getName());
		mdlBuilder.append("\n");
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			appendLine(mdlBuilder, "# Geometry Segment %s %s",
					geometrySegment.getName(),
					geometrySegment.getTexture());
			
			appendLine(mdlBuilder, "newmtl %s",
					geometrySegment.getTexture().replace(".", "_"));
			
			appendLine(mdlBuilder, "    Ka 1.000 1.000 1.000");
			appendLine(mdlBuilder, "    Kd 1.000 1.000 1.000");
			appendLine(mdlBuilder, "    Ks 1.000 1.000 1.000");
			appendLine(mdlBuilder, "    illum 2");
			
			appendLine(mdlBuilder, "    map_Kd %s.png",
					PathUtil.stripExtension(geometrySegment.getTexture()));
		}
		
		return new FileContent<>(
				baseFileName + ".mtl",
				mdlBuilder.toString());
	}
	
	protected FileContent<String> createObjFileContent(Nat nat, String baseFileName) {
		StringBuilder objBuilder = new StringBuilder();
		
		appendLine(objBuilder, "# NAT %s",
				nat.getName());
		objBuilder.append("\n");
		appendLine(objBuilder, "mtllib %s",
				baseFileName + ".mtl");
		objBuilder.append("\n");
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			appendLine(objBuilder, "# Geometry segment vertices %s",
					geometrySegment.getName());
			
			Transform boneGlobalTransform = nat.getBoneGlobalTransform(geometrySegment.getName());
			
			for (int verticeIndex = 0; verticeIndex < geometrySegment.getVerticesCount(); verticeIndex++) {
				Vertice vertice = geometrySegment.getVertice(verticeIndex);
				
				appendLine(objBuilder, "v %f %f %f 1.0",
						Double.valueOf(vertice.getX() + boneGlobalTransform.getX()),
						Double.valueOf(vertice.getY() + boneGlobalTransform.getY()),
						Double.valueOf(vertice.getZ() + boneGlobalTransform.getZ()));
			}
			
			objBuilder.append("\n");
		}
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			appendLine(objBuilder, "# Geometry segment texture coordinates %s",
					geometrySegment.getName());
			
			for (int textureCoordinateIndex = 0; textureCoordinateIndex < geometrySegment.getTextureCoordinatesCount(); textureCoordinateIndex++) {
				TextureCoordinate textureCoordinate = geometrySegment.getTextureCoordinate(textureCoordinateIndex);
				
				appendLine(objBuilder, "vt %f %f 0.0",
						Double.valueOf(textureCoordinate.getU()),
						Double.valueOf(1.0d - textureCoordinate.getV()));
			}
			
			objBuilder.append("\n");
		}
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			appendLine(objBuilder, "# Geometry segment normals %s",
					geometrySegment.getName());
			
			for (int faceIndex = 0; faceIndex < geometrySegment.getFacesCount(); faceIndex += 3) {
				Face firstFace = geometrySegment.getFace(faceIndex + 0);
				Face secondFace = geometrySegment.getFace(faceIndex + 1);
				Face thirdFace = geometrySegment.getFace(faceIndex + 2);
				
				Vertice firstVertice = geometrySegment.getVertice(firstFace.getVerticeIndex());
				Vertice secondVertice = geometrySegment.getVertice(secondFace.getVerticeIndex());
				Vertice thirdVertice = geometrySegment.getVertice(thirdFace.getVerticeIndex());
				
				double aX = secondVertice.getX() - firstVertice.getX();
				double aY = secondVertice.getY() - firstVertice.getY();
				double aZ = secondVertice.getZ() - firstVertice.getZ();
				
				double bX = thirdVertice.getX() - firstVertice.getX();
				double bY = thirdVertice.getY() - firstVertice.getY();
				double bZ = thirdVertice.getZ() - firstVertice.getZ();
				
				double normalX = (aY * bZ) - (aZ * bY);
				double normalY = (aZ * bX) - (aX * bZ);
				double normalZ = (aX * bY) - (aY * bX);
				
				appendLine(objBuilder, "vn %f %f %f",
						Double.valueOf(normalX),
						Double.valueOf(normalY),
						Double.valueOf(normalZ));
				appendLine(objBuilder, "vn %f %f %f",
						Double.valueOf(normalX),
						Double.valueOf(normalY),
						Double.valueOf(normalZ));
				appendLine(objBuilder, "vn %f %f %f",
						Double.valueOf(normalX),
						Double.valueOf(normalY),
						Double.valueOf(normalZ));
			}
			
			objBuilder.append("\n");
		}
		
		int verticeGroupOffset = 0;
		int textureCoordinateGroupOffset = 0;
		int normalsGroupOffset = 0;
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			objBuilder
					.append("# Geometry segment faces ")
					.append(geometrySegment.getName())
					.append("\n");
			
			appendLine(objBuilder, "g %s",
					geometrySegment.getName());
			appendLine(objBuilder, "    usemtl %s",
					geometrySegment.getTexture().replace(".", "_"));
			
			for (int faceIndex = 0; faceIndex < geometrySegment.getFacesCount(); faceIndex += 3) {
				Face firstFace = geometrySegment.getFace(faceIndex + 0);
				Face secondFace = geometrySegment.getFace(faceIndex + 1);
				Face thirdFace = geometrySegment.getFace(faceIndex + 2);
				
				appendLine(objBuilder, "    f %d/%d/%d %d/%d/%d %d/%d/%d",
						Integer.valueOf(1 + firstFace.getVerticeIndex() + verticeGroupOffset),
						Integer.valueOf(1 + firstFace.getTextureCoordinateIndex() + textureCoordinateGroupOffset),
						Integer.valueOf(1 + faceIndex + normalsGroupOffset),
						Integer.valueOf(1 + secondFace.getVerticeIndex() + verticeGroupOffset),
						Integer.valueOf(1 + secondFace.getTextureCoordinateIndex() + textureCoordinateGroupOffset),
						Integer.valueOf(1 + faceIndex + normalsGroupOffset),
						Integer.valueOf(1 + thirdFace.getVerticeIndex() + verticeGroupOffset),
						Integer.valueOf(1 + thirdFace.getTextureCoordinateIndex() + textureCoordinateGroupOffset),
						Integer.valueOf(1 + faceIndex + normalsGroupOffset));
			}
			
			verticeGroupOffset = verticeGroupOffset + geometrySegment.getVerticesCount();
			textureCoordinateGroupOffset = textureCoordinateGroupOffset + geometrySegment.getTextureCoordinatesCount();
			normalsGroupOffset = normalsGroupOffset + geometrySegment.getFacesCount();
		}
		
		return new FileContent<>(
				baseFileName + ".obj",
				objBuilder.toString());
	}
	
	protected List<FileContent<byte[]>> createTextureFileContents(Nat nat) {
		List<FileContent<byte[]>> textureFileContents = new LinkedList<>();
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			try (ByteArrayOutputStream contentOutputStream = new ByteArrayOutputStream()) {
				ImageIO.write(geometrySegment.getTextureInk().getImage(), "png", contentOutputStream);
				
				textureFileContents.add(new FileContent<>(
						PathUtil.stripExtension(geometrySegment.getTexture()) + ".png",
						contentOutputStream.toByteArray()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return textureFileContents;
	}
}
