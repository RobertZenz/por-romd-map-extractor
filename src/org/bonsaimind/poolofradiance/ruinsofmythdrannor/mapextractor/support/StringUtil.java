/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support;

public final class StringUtil {
	private StringUtil() {
	}
	
	public static final String getNumberPostfix(String value) {
		if (value == null) {
			return null;
		}
		
		if (value.isEmpty()) {
			return "";
		}
		
		int postfixStart = value.length();
		
		while (Character.isDigit(value.codePointBefore(postfixStart))) {
			postfixStart = postfixStart - Character.charCount(value.codePointBefore(value.length()));
		}
		
		return value.substring(postfixStart);
	}
	
	public static final String toTwoByteBitMask(int value) {
		return new StringBuilder(4 * 4 + 3)
				.append((value >> 15) & 1)
				.append((value >> 14) & 1)
				.append((value >> 13) & 1)
				.append((value >> 12) & 1)
				.append(".")
				.append((value >> 11) & 1)
				.append((value >> 10) & 1)
				.append((value >> 9) & 1)
				.append((value >> 8) & 1)
				.append(" ")
				.append((value >> 7) & 1)
				.append((value >> 6) & 1)
				.append((value >> 5) & 1)
				.append((value >> 4) & 1)
				.append(".")
				.append((value >> 3) & 1)
				.append((value >> 2) & 1)
				.append((value >> 1) & 1)
				.append((value >> 0) & 1)
				.toString();
	}
}
