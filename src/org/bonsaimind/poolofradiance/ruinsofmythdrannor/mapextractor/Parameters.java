/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.ClassnameTagLevel;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.LevelTagLevel;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.LogLevel;

public class Parameters {
	protected List<String> additionalParameters = new LinkedList<>();
	protected ClassnameTagLevel classnameTagLevel = ClassnameTagLevel.NONE;
	protected Path dataDirectory = null;
	protected boolean help = false;
	protected String imageType = "jpeg";
	protected LevelTagLevel levelTagLevel = LevelTagLevel.NONE;
	protected LogLevel logLevel = LogLevel.INFO;
	protected OperationMode operationMode = OperationMode.VIEWER;
	protected Path targetDirectory = null;
	
	public Parameters(String[] arguments) {
		super();
		
		if (arguments != null && arguments.length > 0) {
			for (String argument : arguments) {
				if (argument.startsWith("--data-directory=")) {
					dataDirectory = Paths.get(argument.substring("--data-directory=".length()));
				} else if (argument.startsWith("--debug")) {
					classnameTagLevel = ClassnameTagLevel.SIMPLE_CLASSNAME;
					levelTagLevel = LevelTagLevel.FULL;
					logLevel = LogLevel.DEBUG;
				} else if (argument.startsWith("--errors")) {
					classnameTagLevel = ClassnameTagLevel.NONE;
					levelTagLevel = LevelTagLevel.NONE;
					logLevel = LogLevel.ERROR;
				} else if (argument.equals("--export")) {
					operationMode = OperationMode.EXPORT;
				} else if (argument.equals("--help")) {
					help = true;
				} else if (argument.startsWith("--image-type=")) {
					imageType = argument.substring("--image-type=".length());
				} else if (argument.startsWith("--log-classname-tag-level=")) {
					classnameTagLevel = ClassnameTagLevel.valueOf(argument.substring("--log-classname-tag-level=".length()));
				} else if (argument.startsWith("--log-level-tag-level=")) {
					levelTagLevel = LevelTagLevel.valueOf(argument.substring("--log-level-tag-level=".length()));
				} else if (argument.startsWith("--log-level=")) {
					logLevel = LogLevel.valueOf(argument.substring("--log-level=".length()));
				} else if (argument.startsWith("--quiet")) {
					classnameTagLevel = ClassnameTagLevel.NONE;
					levelTagLevel = LevelTagLevel.NONE;
					logLevel = LogLevel.NONE;
				} else if (argument.startsWith("--target-directory=")) {
					targetDirectory = Paths.get(argument.substring("--target-directory=".length()));
				} else if (argument.equals("--verbose")) {
					classnameTagLevel = ClassnameTagLevel.NONE;
					levelTagLevel = LevelTagLevel.NONE;
					logLevel = LogLevel.DEBUG;
				} else if (argument.equals("--viewer")) {
					operationMode = OperationMode.VIEWER;
				} else if (argument.startsWith("-")) {
					help = true;
				} else {
					additionalParameters.add(argument);
				}
			}
		}
		
		if (dataDirectory == null && additionalParameters.size() >= 1) {
			Path dataDirectoryPathCandidate = Paths.get(additionalParameters.get(0));
			
			if (Files.isDirectory(dataDirectoryPathCandidate)) {
				additionalParameters.remove(0);
				
				dataDirectory = dataDirectoryPathCandidate;
			}
		}
	}
	
	public final List<String> getAdditionalParameters() {
		return Collections.unmodifiableList(additionalParameters);
	}
	
	public final ClassnameTagLevel getClassnameTagLevel() {
		return classnameTagLevel;
	}
	
	public Path getDataDirectory() {
		return dataDirectory;
	}
	
	public boolean getHelp() {
		return help;
	}
	
	public String getImageType() {
		return imageType;
	}
	
	public final LevelTagLevel getLevelTagLevel() {
		return levelTagLevel;
	}
	
	public final LogLevel getLogLevel() {
		return logLevel;
	}
	
	public OperationMode getOperationMode() {
		return operationMode;
	}
	
	public final Path getTargetDirectory() {
		return targetDirectory;
	}
	
	public enum OperationMode {
		EXPORT, VIEWER;
	}
}
