/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.map;

import java.util.LinkedList;
import java.util.List;

public class Map {
	public static final int DEFAULT_SINGLE_TILE_COLUMN_COUNT = 13;
	public static final int DEFAULT_SINGLE_TILE_ROW_COUNT = 10;
	public static final int DEFAULT_SINGLE_TILE_SIZE = 128;
	
	protected String mapName = null;
	protected List<MapTile> mapTiles = new LinkedList<>();
	protected int mapTilesColumnCount = 0;
	protected int mapTilesRowCount = 0;
	protected int singleTilesColumnCount = DEFAULT_SINGLE_TILE_COLUMN_COUNT;
	protected int singleTileSize = DEFAULT_SINGLE_TILE_SIZE;
	protected int singleTilesRowCount = DEFAULT_SINGLE_TILE_ROW_COUNT;
	
	public Map(String mapName) {
		super();
		
		this.mapName = mapName;
	}
	
	public String getMapName() {
		return mapName;
	}
	
	public List<MapTile> getMapTiles() {
		return mapTiles;
	}
	
	public int getMapTilesColumnCount() {
		return mapTilesColumnCount;
	}
	
	public int getMapTilesRowCount() {
		return mapTilesRowCount;
	}
	
	public int getSingleTilesColumnCount() {
		return singleTilesColumnCount;
	}
	
	public int getSingleTileSize() {
		return singleTileSize;
	}
	
	public int getSingleTilesRowCount() {
		return singleTilesRowCount;
	}
	
	public Map setMapTilesColumnCount(int columnCount) {
		this.mapTilesColumnCount = columnCount;
		
		return this;
	}
	
	public Map setMapTilesRowCount(int rowCount) {
		this.mapTilesRowCount = rowCount;
		
		return this;
	}
	
	public Map setSingleTilesColumnCount(int singleTilesColumnCount) {
		this.singleTilesColumnCount = singleTilesColumnCount;
		
		return this;
	}
	
	public Map setSingleTilesRowCount(int singleTilesRowCount) {
		this.singleTilesRowCount = singleTilesRowCount;
		
		return this;
	}
	
	public Map setTileSize(int tileSize) {
		this.singleTileSize = tileSize;
		
		return this;
	}
}
