/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.Parameters;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.CnkReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.InkReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.MczReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.NatReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.WmdReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.cnk.Cnk;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz.Mcz;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.AbstractPolySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometryPhysique;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometrySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Nat;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support.PathUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.MapTileEntry;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.Wmd;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ExceptionUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.FileContent;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ImageStitcher;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ObjBundle;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ObjCreator;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.StringUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.SwingUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers.CnkViewer;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers.InkViewer;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers.MczViewer;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers.NatViewer;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers.WmdViewer;

public class ViewerFrame extends JFrame {
	protected static final FileFilter FILE_FILTER_JPEG = new FileNameExtensionFilter("JPEG (*.jpeg)",
			"jpeg");
	protected static final FileFilter FILE_FILTER_OBJ = new FileNameExtensionFilter("Wavefront Object (*.obj)",
			"obj");
	protected static final FileFilter FILE_FILTER_PNG = new FileNameExtensionFilter("PNG (*.png)",
			"png");
	
	protected JMenuItem closeMenuItem = null;
	protected CnkReader cnkReader = null;
	protected JFileChooser exportDirectoryFileChooser = null;
	protected JFileChooser exportFileChooser = null;
	protected JMenuItem exportMenuItem = null;
	protected ImageStitcher imageStitcher = null;
	protected InkReader inkReader = null;
	protected MczReader mczReader = null;
	protected NatReader natReader = null;
	protected ObjCreator objCreator = null;
	protected JFileChooser openFileChooser = null;
	protected Parameters parameters = null;
	protected List<Object> porRomdTabObjects = new LinkedList<>();
	protected JTabbedPane tabbedPane = null;
	protected WmdReader wmdReader = null;
	
	public ViewerFrame(
			Parameters parameters) throws HeadlessException {
		super();
		
		this.parameters = parameters;
		
		openFileChooser = new JFileChooser();
		openFileChooser.setAcceptAllFileFilterUsed(false);
		openFileChooser.setMultiSelectionEnabled(true);
		openFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Supported PoR:RoMD data files (*.CNK, *.INK, *.MCZ, *.NAT, *.WMD)",
				"cnk",
				"ink",
				"nat",
				"mcz",
				"wmd"));
		openFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PoR:RoMD CNK (texture archive) files (*.CNK)",
				"cnk"));
		openFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PoR:RoMD INK (texture) files (*.INK)",
				"ink"));
		openFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PoR:RoMD MCZ (map tile) files (*.MCZ)",
				"mcz"));
		openFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PoR:RoMD NAT (model) files (*.NAT)",
				"nat"));
		openFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PoR:RoMD WMD (map) files (*.WMD)",
				"wmd"));
		
		if (parameters.getDataDirectory() != null) {
			openFileChooser.setCurrentDirectory(parameters.getDataDirectory().toFile());
		}
		
		exportFileChooser = new JFileChooser();
		exportFileChooser.setAcceptAllFileFilterUsed(false);
		exportFileChooser.setMultiSelectionEnabled(false);
		
		if (parameters.getTargetDirectory() != null) {
			exportFileChooser.setCurrentDirectory(parameters.getTargetDirectory().toFile());
		}
		
		exportDirectoryFileChooser = new JFileChooser();
		exportDirectoryFileChooser.setAcceptAllFileFilterUsed(false);
		exportDirectoryFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		exportDirectoryFileChooser.setMultiSelectionEnabled(false);
		
		if (parameters.getTargetDirectory() != null) {
			exportDirectoryFileChooser.setCurrentDirectory(parameters.getTargetDirectory().toFile());
		}
		
		JMenuItem openMenuItem = new JMenuItem();
		openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK, true));
		openMenuItem.setText("Open");
		openMenuItem.setMnemonic('O');
		openMenuItem.addActionListener(this::handleOpenAction);
		
		closeMenuItem = new JMenuItem();
		closeMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, KeyEvent.CTRL_DOWN_MASK, true));
		closeMenuItem.setText("Close");
		closeMenuItem.setMnemonic('c');
		closeMenuItem.addActionListener(this::handleCloseAction);
		
		exportMenuItem = new JMenuItem();
		exportMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK, true));
		exportMenuItem.setText("Export");
		exportMenuItem.setMnemonic('E');
		exportMenuItem.addActionListener(this::handleExportAction);
		
		JMenuItem quitMenuItem = new JMenuItem();
		quitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK, true));
		quitMenuItem.setText("Quit");
		quitMenuItem.setMnemonic('Q');
		quitMenuItem.addActionListener(this::handleQuitAction);
		
		JMenu viewerMenu = new JMenu();
		viewerMenu.setMnemonic('V');
		viewerMenu.setText("Viewer");
		viewerMenu.add(openMenuItem);
		viewerMenu.add(exportMenuItem);
		viewerMenu.add(closeMenuItem);
		viewerMenu.add(new JSeparator());
		viewerMenu.add(quitMenuItem);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(viewerMenu);
		
		tabbedPane = new JTabbedPane();
		
		setSize(1024, 768);
		setTitle("PoR:RoMD - Viewer");
		setJMenuBar(menuBar);
		setLayout(new BorderLayout());
		add(tabbedPane, BorderLayout.CENTER);
		
		updateMenuItemStates();
		
		inkReader = new InkReader();
		cnkReader = new CnkReader(inkReader);
		mczReader = new MczReader();
		natReader = new NatReader();
		wmdReader = new WmdReader();
		imageStitcher = new ImageStitcher();
		objCreator = new ObjCreator();
		
		for (String additionalParameter : parameters.getAdditionalParameters()) {
			try {
				open(Paths.get(additionalParameter));
			} catch (Throwable th) {
				SwingUtil.showError(this, th);
			}
		}
	}
	
	protected void closeCurrentTab() {
		int currentTabIndex = tabbedPane.getSelectedIndex();
		
		if (currentTabIndex >= 0) {
			tabbedPane.removeTabAt(currentTabIndex);
			porRomdTabObjects.remove(currentTabIndex);
		}
		
		updateMenuItemStates();
	}
	
	protected void exportCnk(Cnk cnk) throws IOException {
		int exportType = JOptionPane.showOptionDialog(
				this,
				"Export the CNK as atlas or each file individually into a directory?",
				"Export CNK",
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				new Object[] { "Atlas", "Individual files", "Cancel" },
				"Atlas");
		
		switch (exportType) {
			case 0:
				exportCnkToAtlas(cnk);
				break;
			
			case 1:
				exportCnkToDirectory(cnk);
				break;
		}
	}
	
	protected void exportCnkToAtlas(Cnk cnk) throws IOException {
		exportImage(cnk.getName(), () -> imageStitcher.stitchImage(cnk));
	}
	
	protected void exportCnkToDirectory(Cnk cnk) throws IOException {
		exportDirectoryFileChooser.removeChoosableFileFilter(FILE_FILTER_JPEG);
		exportDirectoryFileChooser.removeChoosableFileFilter(FILE_FILTER_PNG);
		
		exportDirectoryFileChooser.addChoosableFileFilter(FILE_FILTER_JPEG);
		exportDirectoryFileChooser.addChoosableFileFilter(FILE_FILTER_PNG);
		exportDirectoryFileChooser.setFileFilter(FILE_FILTER_PNG);
		
		exportDirectoryFileChooser.setSelectedFile(new File(PathUtil.stripExtension(cnk.getName())));
		
		if (exportDirectoryFileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			String exportImageType = null;
			
			if (exportDirectoryFileChooser.getFileFilter() == FILE_FILTER_JPEG) {
				exportImageType = "JPEG";
			} else if (exportDirectoryFileChooser.getFileFilter() == FILE_FILTER_PNG) {
				exportImageType = "PNG";
			}
			
			File selectedDirectoryFile = exportDirectoryFileChooser.getSelectedFile();
			Path selectedDirectoryPath = selectedDirectoryFile.toPath();
			
			for (int entryIndex = 0; entryIndex < cnk.getEntriesCount(); entryIndex++) {
				String entryName = cnk.getEntryName(entryIndex);
				Ink ink = cnk.getEntryInk(entryIndex);
				
				String imageName = PathUtil.stripExtension(entryName) + "." + exportImageType.toLowerCase();
				Path imagePath = selectedDirectoryPath.resolve(imageName);
				
				ImageIO.write(
						ink.getImage(),
						exportImageType,
						imagePath.toFile());
			}
		}
	}
	
	protected void exportCurrentTabObject() {
		int currentTabIndex = tabbedPane.getSelectedIndex();
		
		if (currentTabIndex >= 0) {
			Object porRomdObject = porRomdTabObjects.get(currentTabIndex);
			
			try {
				if (porRomdObject instanceof Cnk) {
					Cnk cnk = (Cnk)porRomdObject;
					
					exportCnk(cnk);
				} else if (porRomdObject instanceof Ink) {
					Ink ink = (Ink)porRomdObject;
					
					exportInk(ink);
				} else if (porRomdObject instanceof Mcz) {
					Mcz mcz = (Mcz)porRomdObject;
					
					exportMcz(mcz);
				} else if (porRomdObject instanceof Nat) {
					Nat nat = (Nat)porRomdObject;
					
					exportNat(nat);
				} else if (porRomdObject instanceof Wmd) {
					Wmd wmd = (Wmd)porRomdObject;
					
					exportWmd(wmd);
				} else {
					SwingUtil.showError(this, "Unknown object", "Do not know how to eport <%s>.",
							porRomdObject.getClass().getSimpleName());
				}
			} catch (Throwable th) {
				SwingUtil.showError(this, th);
			}
		}
	}
	
	protected void exportImage(String fileName, Supplier<BufferedImage> imageSupplier) throws IOException {
		exportFileChooser.removeChoosableFileFilter(FILE_FILTER_JPEG);
		exportFileChooser.removeChoosableFileFilter(FILE_FILTER_PNG);
		
		exportFileChooser.addChoosableFileFilter(FILE_FILTER_JPEG);
		exportFileChooser.addChoosableFileFilter(FILE_FILTER_PNG);
		exportFileChooser.setFileFilter(FILE_FILTER_PNG);
		
		exportFileChooser.setSelectedFile(new File(PathUtil.stripExtension(fileName) + ".png"));
		
		if (exportFileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			String exportImageType = null;
			
			if (exportFileChooser.getFileFilter() == FILE_FILTER_JPEG) {
				exportImageType = "JPEG";
			} else if (exportFileChooser.getFileFilter() == FILE_FILTER_PNG) {
				exportImageType = "PNG";
			}
			
			ImageIO.write(
					imageSupplier.get(),
					exportImageType,
					exportFileChooser.getSelectedFile());
		}
	}
	
	protected void exportInk(Ink ink) throws IOException {
		exportImage(ink.getName(), () -> ink.getImage());
	}
	
	protected void exportMcz(Mcz mcz) throws IOException {
		exportImage(mcz.getName(), () -> imageStitcher.stitchImage(mcz));
	}
	
	protected void exportNat(Nat nat) throws IOException {
		exportFileChooser.removeChoosableFileFilter(FILE_FILTER_JPEG);
		exportFileChooser.removeChoosableFileFilter(FILE_FILTER_PNG);
		
		exportFileChooser.addChoosableFileFilter(FILE_FILTER_OBJ);
		exportFileChooser.setFileFilter(FILE_FILTER_OBJ);
		
		exportFileChooser.setSelectedFile(new File(PathUtil.stripExtension(nat.getName()) + ".obj"));
		
		if (exportFileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = exportFileChooser.getSelectedFile();
			
			ObjBundle objBundle = objCreator.createObj(
					nat,
					PathUtil.stripExtension(selectedFile.getName()));
			
			Path selectedDirectoryPath = selectedFile.toPath().getParent();
			
			Files.write(
					selectedDirectoryPath.resolve(objBundle.getObjFileContent().getFileName()),
					objBundle.getObjFileContent().getContent().getBytes(StandardCharsets.UTF_8));
			
			Files.write(
					selectedDirectoryPath.resolve(objBundle.getMdlFileContent().getFileName()),
					objBundle.getMdlFileContent().getContent().getBytes(StandardCharsets.UTF_8));
			
			for (FileContent<byte[]> textureFileContent : objBundle.getTextureFileContents()) {
				Files.write(
						selectedDirectoryPath.resolve(textureFileContent.getFileName()),
						textureFileContent.getContent());
			}
		}
	}
	
	protected void exportWmd(Wmd wmd) throws IOException {
		exportImage(wmd.getName(), () -> imageStitcher.stitchImage(wmd));
	}
	
	protected void handleCloseAction(ActionEvent actionEvent) {
		closeCurrentTab();
	}
	
	protected void handleExportAction(ActionEvent actionEvent) {
		exportCurrentTabObject();
	}
	
	protected void handleOpenAction(ActionEvent actionEvent) {
		if (openFileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			for (File selectedFile : openFileChooser.getSelectedFiles()) {
				Path selectedFilePath = selectedFile.toPath();
				
				try {
					open(selectedFilePath);
				} catch (Throwable th) {
					openErrorView(selectedFilePath, th);
				}
			}
		}
	}
	
	protected void handleQuitAction(ActionEvent actionEvent) {
		tabbedPane.removeAll();
		
		setVisible(false);
		
		System.exit(0);
	}
	
	protected void open(Path filePath) throws IOException {
		switch (PathUtil.getExtension(filePath).toLowerCase()) {
			case "cnk":
				openCnk(filePath);
				break;
			
			case "ink":
				openInk(filePath);
				break;
			
			case "mcz":
				openMcz(filePath);
				break;
			
			case "nat":
				openNat(filePath);
				break;
			
			case "wmd":
				openWmd(filePath);
				break;
			
			default:
				SwingUtil.showError(this, "Unknown file", "Do not know how to open <%s>.",
						filePath.getFileName());
		}
	}
	
	protected void openCnk(Path cnkFilePath) throws IOException {
		Cnk cnk = cnkReader.read(cnkFilePath);
		
		CnkViewer cnkViewer = new CnkViewer();
		cnkViewer.setCnk(cnk);
		
		openTab(cnkFilePath, cnkViewer, cnk);
	}
	
	protected void openErrorView(Path filePath, Throwable throwable) {
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setText(ExceptionUtil.getAsText(throwable));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(textArea);
		
		openTab(filePath, scrollPane, null);
	}
	
	protected void openInk(Path inkFilePath) throws IOException {
		Ink ink = inkReader.read(inkFilePath);
		
		InkViewer inkViewer = new InkViewer();
		inkViewer.setInk(ink);
		
		openTab(inkFilePath, inkViewer, ink);
	}
	
	protected void openMcz(Path mczFilePath) throws IOException {
		Mcz mcz = mczReader.read(mczFilePath);
		
		MczViewer mczViewer = new MczViewer();
		mczViewer.setMcz(mcz);
		
		openTab(mczFilePath, mczViewer, mcz);
	}
	
	protected void openNat(Path natFilePath) throws IOException {
		Nat nat = natReader.read(natFilePath);
		
		Path dataPath = null;
		
		if (parameters.getDataDirectory() != null) {
			dataPath = parameters.getDataDirectory();
		} else {
			dataPath = natFilePath.getParent().getParent().getParent();
		}
		
		Path inkDirectoryPath = PathUtil.resolveCaseInsensitive(dataPath, "INK");
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			if (geometrySegment.getTexture() != null
					&& !geometrySegment.getTexture().isEmpty()) {
				String textureName = PathUtil.stripExtension(geometrySegment.getTexture());
				Path textureInkPath = PathUtil.tryResolveCaseInsensitive(
						inkDirectoryPath,
						textureName + ".ink");
				
				if (textureInkPath == null) {
					String numberPostfix = StringUtil.getNumberPostfix(textureName);
					
					if (numberPostfix != null) {
						String textureNameWithInsertedZero = textureName.substring(0, textureName.length() - numberPostfix.length())
								+ "0"
								+ numberPostfix;
						
						textureInkPath = PathUtil.tryResolveCaseInsensitive(
								inkDirectoryPath,
								textureNameWithInsertedZero + ".ink");
					}
				}
				
				if (textureInkPath == null) {
					throw new IOException(String.format("Could not resolve <%s> to INK/texture file.",
							geometrySegment.getTexture()));
				}
				
				Ink textureInk = inkReader.read(textureInkPath);
				
				geometrySegment.setTextureInk(textureInk);
			}
		}
		
		Path cnkFilePath = PathUtil.resolveCaseInsensitive(
				inkDirectoryPath,
				PathUtil.stripExtension(natFilePath.getFileName().toString()) + ".cnk");
		Cnk cnk = cnkReader.read(cnkFilePath);
		
		for (int geometryPhysiqueIndex = 0; geometryPhysiqueIndex < nat.getGeometryPhysiquesCount(); geometryPhysiqueIndex++) {
			GeometryPhysique geometryPhysique = nat.getGeometryPhysique(geometryPhysiqueIndex);
			
			for (int polySegmentIndex = 0; polySegmentIndex < geometryPhysique.getPolySegmentsCount(); polySegmentIndex++) {
				AbstractPolySegment polySegment = geometryPhysique.getPolySegment(polySegmentIndex);
				
				if (polySegment.getTexture() != null
						&& !polySegment.getTexture().isEmpty()) {
					String textureName = PathUtil.stripExtension(polySegment.getTexture());
					Ink textureInk = cnk.getEntryByName(textureName + ".ink");
					
					if (textureInk == null) {
						Path textureInkPath = PathUtil.tryResolveCaseInsensitive(
								inkDirectoryPath,
								textureName + ".ink");
						
						if (textureInkPath == null) {
							String numberPostfix = StringUtil.getNumberPostfix(textureName);
							
							if (numberPostfix != null) {
								String textureNameWithInsertedZero = textureName.substring(0, textureName.length() - numberPostfix.length())
										+ "0"
										+ numberPostfix;
								
								textureInkPath = PathUtil.tryResolveCaseInsensitive(
										inkDirectoryPath,
										textureNameWithInsertedZero + ".ink");
							}
						}
						
						if (textureInkPath == null) {
							throw new IOException(String.format("Could not resolve <%s> to INK/texture file.",
									polySegment.getTexture()));
						}
						
						textureInk = inkReader.read(textureInkPath);
					}
					
					polySegment.setTextureInk(textureInk);
				}
			}
		}
		
		NatViewer natViewer = new NatViewer();
		natViewer.setNat(nat);
		
		openTab(natFilePath, natViewer, nat);
	}
	
	protected void openTab(Path filePath, JComponent content, Object porRomdObject) {
		tabbedPane.addTab(filePath.getFileName().toString(), null, content, filePath.toString());
		tabbedPane.setSelectedComponent(content);
		
		porRomdTabObjects.add(porRomdObject);
		
		updateMenuItemStates();
	}
	
	protected void openWmd(Path wmdFilePath) throws IOException {
		Wmd wmd = wmdReader.read(wmdFilePath);
		
		Path dataPath = null;
		
		if (parameters.getDataDirectory() != null) {
			dataPath = parameters.getDataDirectory();
		} else {
			dataPath = wmdFilePath.getParent().getParent();
		}
		
		Path mczDirectoryPath = PathUtil.resolveCaseInsensitive(dataPath, "MCZ");
		
		for (int entryIndex = 0; entryIndex < wmd.getMapTilesEntriesCount(); entryIndex++) {
			MapTileEntry mapTileEntry = wmd.getMapTileEntry(entryIndex);
			Path mczFilePath = mczDirectoryPath.resolve(mapTileEntry.getName());
			Mcz mcz = mczReader.read(mczFilePath);
			
			mapTileEntry.setMcz(mcz);
		}
		
		WmdViewer wmdViewer = new WmdViewer();
		wmdViewer.setWmd(wmd);
		
		openTab(wmdFilePath, wmdViewer, wmd);
	}
	
	protected void updateMenuItemStates() {
		boolean tabsOpen = tabbedPane.getTabCount() > 0;
		
		closeMenuItem.setEnabled(tabsOpen);
		exportMenuItem.setEnabled(tabsOpen);
	}
}
