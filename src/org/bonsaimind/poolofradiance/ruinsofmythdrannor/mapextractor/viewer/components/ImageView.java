/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ChildView;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ChildViewFeedingMouseListener;

public class ImageView extends JComponent {
	protected BufferedImage bufferedImage = null;
	protected ChildView childView = new ChildView();
	protected boolean firstDraw = false;
	
	public ImageView() {
		super();
		
		ChildViewFeedingMouseListener.createAndRegisterOn(this, childView);
		
		setBackground(Color.WHITE);
	}
	
	public ImageView setImage(BufferedImage image) {
		this.bufferedImage = image;
		
		firstDraw = true;
		
		repaint();
		
		return this;
	}
	
	@Override
	protected void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);
		
		graphics.setColor(getBackground());
		graphics.fillRect(
				graphics.getClipBounds().x,
				graphics.getClipBounds().y,
				graphics.getClipBounds().width,
				graphics.getClipBounds().height);
		
		if (bufferedImage != null) {
			childView.setParentBounds(0, 0, getWidth(), getHeight());
			
			if (firstDraw) {
				childView.setChildSize(bufferedImage.getWidth(), bufferedImage.getHeight());
				childView.centerAndZoom();
				
				firstDraw = false;
			}
			
			graphics.drawImage(
					bufferedImage,
					childView.getChildX(),
					childView.getChildY(),
					childView.getChildWidth(),
					childView.getChildHeight(),
					null);
		}
	}
}
