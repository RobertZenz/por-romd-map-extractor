/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.components;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ChildView;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ChildViewFeedingMouseListener;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ImageMosaic;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ImageMosaic.Bounds;

public class ImageMosaicView extends JComponent {
	protected ChildView childView = new ChildView();
	protected boolean firstDraw = false;
	protected ImageMosaic imageMosaic = null;
	
	public ImageMosaicView() {
		super();
		
		ChildViewFeedingMouseListener.createAndRegisterOn(this, childView);
	}
	
	public ImageMosaicView setImageMosaic(ImageMosaic imageMosaic) {
		this.imageMosaic = imageMosaic;
		
		firstDraw = true;
		
		repaint();
		
		return this;
	}
	
	@Override
	protected void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);
		
		if (imageMosaic != null) {
			childView.setParentBounds(0, 0, getWidth(), getHeight());
			
			if (firstDraw) {
				childView.setChildSize(
						imageMosaic.getMosaicBounds().getWidth(),
						imageMosaic.getMosaicBounds().getHeight());
				childView.centerAndZoom();
				
				firstDraw = false;
			}
			
			for (int index = 0; index < imageMosaic.getImagesCount(); index++) {
				BufferedImage image = imageMosaic.getImage(index);
				Bounds imageBounds = imageMosaic.getImageBounds(index);
				
				if (childView.isRelativeInView(
						imageBounds.getX(),
						imageBounds.getY(),
						imageBounds.getEndX(),
						imageBounds.getEndY())) {
					graphics.drawImage(
							image,
							childView.translateRelativeCoordinateX(imageBounds.getX()),
							childView.translateRelativeCoordinateY(imageBounds.getY()),
							childView.translateValue(imageBounds.getWidth()),
							childView.translateValue(imageBounds.getHeight()),
							null);
				}
			}
		}
	}
}
