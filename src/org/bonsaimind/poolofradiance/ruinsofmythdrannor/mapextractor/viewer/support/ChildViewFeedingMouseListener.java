/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JComponent;

public class ChildViewFeedingMouseListener implements MouseListener, MouseMotionListener, MouseWheelListener {
	protected ChildView childView = null;
	protected MouseEvent lastMouseDraggedEvent = null;
	protected JComponent parent = null;
	
	public ChildViewFeedingMouseListener(
			JComponent parent,
			ChildView childView) {
		super();
		
		this.parent = parent;
		this.childView = childView;
	}
	
	public static ChildViewFeedingMouseListener createAndRegisterOn(JComponent parent, ChildView childView) {
		ChildViewFeedingMouseListener childViewFeedingMouseListener = new ChildViewFeedingMouseListener(
				parent,
				childView);
		
		parent.addMouseListener(childViewFeedingMouseListener);
		parent.addMouseMotionListener(childViewFeedingMouseListener);
		parent.addMouseWheelListener(childViewFeedingMouseListener);
		
		return childViewFeedingMouseListener;
	}
	
	@Override
	public void mouseClicked(MouseEvent mouseEvent) {
	}
	
	@Override
	public void mouseDragged(MouseEvent mouseEvent) {
		parent.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		
		int distanceDraggedX = 0;
		int distanceDraggedY = 0;
		
		if (lastMouseDraggedEvent != null) {
			distanceDraggedX = mouseEvent.getX() - lastMouseDraggedEvent.getX();
			distanceDraggedY = mouseEvent.getY() - lastMouseDraggedEvent.getY();
		}
		
		lastMouseDraggedEvent = mouseEvent;
		
		childView.translateBy(distanceDraggedX, distanceDraggedY);
		
		parent.repaint();
	}
	
	@Override
	public void mouseEntered(MouseEvent mouseEvent) {
	}
	
	@Override
	public void mouseExited(MouseEvent mouseEvent) {
	}
	
	@Override
	public void mouseMoved(MouseEvent mouseEvent) {
	}
	
	@Override
	public void mousePressed(MouseEvent mouseEvent) {
	}
	
	@Override
	public void mouseReleased(MouseEvent mouseEvent) {
		parent.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		
		lastMouseDraggedEvent = null;
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
		childView.zoomBy(-mouseWheelEvent.getWheelRotation() * 0.1d);
		
		parent.repaint();
	}
}
