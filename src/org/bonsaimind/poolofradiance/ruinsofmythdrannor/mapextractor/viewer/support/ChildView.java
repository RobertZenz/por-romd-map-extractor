/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support;

public class ChildView {
	protected static final double CENTER_ZOOM_FACTOR = 0.75d;
	protected static final double ZOOM_STEP = 0.05d;
	
	protected int childHeight = 0;
	protected int childWidth = 0;
	protected int childX = 0;
	protected int childY = 0;
	protected boolean forceCenteredWhenSmaller = true;
	protected double maximumZoom = 3.0d;
	protected double minimumZoom = 1.0d;
	protected int parentHeight = 0;
	protected int parentWidth = 0;
	protected int parentX = 0;
	protected int parentY = 0;
	protected int realChildHeight = 0;
	protected int realChildWidth = 0;
	protected double steppedZoom = 1.0d;
	protected double zoom = 1.0d;
	
	public ChildView() {
		super();
	}
	
	public ChildView center() {
		childWidth = (int)Math.round(realChildWidth * steppedZoom);
		childHeight = (int)Math.round(realChildHeight * steppedZoom);
		
		childX = parentX + ((parentWidth - childWidth) / 2);
		childY = parentY + ((parentHeight - childHeight) / 2);
		
		return this;
	}
	
	public ChildView centerAndZoom() {
		if (realChildWidth <= 0 || realChildHeight <= 0) {
			return this;
		}
		
		zoom = Math.min(
				(parentWidth / realChildWidth),
				(parentHeight / realChildHeight));
		
		if (realChildWidth < parentWidth
				|| realChildHeight < parentHeight) {
			zoom = zoom * CENTER_ZOOM_FACTOR;
		}
		
		clampChildBounds();
		clampZoom();
		
		center();
		
		return this;
	}
	
	public int getChildHeight() {
		return childHeight;
	}
	
	public int getChildWidth() {
		return childWidth;
	}
	
	public int getChildX() {
		return childX;
	}
	
	public int getChildY() {
		return childY;
	}
	
	public boolean isForceCenteredWhenSmaller() {
		return forceCenteredWhenSmaller;
	}
	
	public boolean isRelativeInView(int startX, int startY, int endX, int endY) {
		int translatedStartX = translateRelativeCoordinateX(startX);
		int translatedStartY = translateRelativeCoordinateY(startY);
		int translatedEndX = translateRelativeCoordinateX(endX);
		int translatedEndY = translateRelativeCoordinateY(endY);
		
		return (translatedEndX >= 0 && translatedStartX <= parentWidth)
				&& (translatedEndY >= 0 && translatedStartY <= parentHeight);
	}
	
	public ChildView setChildSize(int width, int height) {
		childX = 0;
		childY = 0;
		
		realChildWidth = width;
		realChildHeight = height;
		
		childWidth = (int)Math.round(realChildWidth * zoom);
		childHeight = (int)Math.round(realChildHeight * zoom);
		
		clampChildBounds();
		clampZoom();
		
		return this;
	}
	
	public ChildView setForceCenteredWhenSmaller(boolean forceCenteredWhenSmaller) {
		this.forceCenteredWhenSmaller = forceCenteredWhenSmaller;
		
		return this;
	}
	
	public ChildView setParentBounds(int x, int y, int width, int height) {
		parentX = x;
		parentY = y;
		parentWidth = width;
		parentHeight = height;
		
		clampChildBounds();
		clampZoom();
		
		return this;
	}
	
	public ChildView translateBy(int x, int y) {
		childX = childX + x;
		childY = childY + y;
		
		clampChildBounds();
		
		return this;
	}
	
	public int translateRelativeCoordinateX(int x) {
		return childX + (int)Math.round(x * steppedZoom);
	}
	
	public int translateRelativeCoordinateY(int y) {
		return childY + (int)Math.round(y * steppedZoom);
	}
	
	public int translateValue(int value) {
		return (int)Math.round(value * steppedZoom);
	}
	
	public ChildView zoomBy(double zoomChange) {
		zoom = Math.max(minimumZoom, zoom + zoomChange);
		
		clampZoom();
		
		int previousChildWidth = childWidth;
		int previousChildHeight = childHeight;
		
		childWidth = (int)Math.round(realChildWidth * steppedZoom);
		childHeight = (int)Math.round(realChildHeight * steppedZoom);
		
		if (previousChildWidth > childWidth) {
			childX = childX + ((previousChildWidth - childWidth) / 2);
		} else {
			childX = childX - ((childWidth - previousChildWidth) / 2);
		}
		
		if (previousChildHeight > childHeight) {
			childY = childY + ((previousChildHeight - childHeight) / 2);
		} else {
			childY = childY - ((childHeight - previousChildHeight) / 2);
		}
		
		return this;
	}
	
	protected int clampChildBound(int childOrigin, int childSize, int parentOrigin, int parentSize) {
		if (childSize <= parentSize) {
			if (childOrigin < parentOrigin) {
				return parentOrigin;
			} else if ((childOrigin + childSize) > parentSize) {
				return parentSize - childSize;
			}
		} else if (childSize > parentSize) {
			if (childOrigin > parentOrigin) {
				return parentOrigin;
			} else if ((childOrigin + childSize) < parentSize) {
				return parentSize - childSize;
			}
		}
		
		return childOrigin;
	}
	
	protected void clampChildBounds() {
		childX = clampChildBound(childX, childWidth, parentX, parentWidth);
		childY = clampChildBound(childY, childHeight, parentY, parentHeight);
		
		if (forceCenteredWhenSmaller
				&& parentWidth > childWidth
				&& parentHeight > childHeight) {
			center();
		}
	}
	
	protected void clampZoom() {
		if (realChildWidth > parentWidth
				|| realChildHeight > parentHeight) {
			minimumZoom = 0.25d;
		} else {
			minimumZoom = 1.0d;
		}
		
		zoom = Math.max(minimumZoom, zoom);
		zoom = Math.min(maximumZoom, zoom);
		
		steppedZoom = Math.round(zoom / ZOOM_STEP) * ZOOM_STEP;
	}
}
