/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ExceptionUtil;

public final class SwingUtil {
	private SwingUtil() {
	}
	
	public static void showError(Component parentComponent, String title, String text, Object... arguments) {
		JOptionPane.showMessageDialog(
				parentComponent,
				String.format(text, arguments),
				title,
				JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showError(Component parentComponent, Throwable throwable) {
		throwable.printStackTrace();
		
		showError(parentComponent, "An errror occurred", ExceptionUtil.getAsText(throwable));
	}
}
