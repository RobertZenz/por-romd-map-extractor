/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

public class ImageMosaic {
	protected List<Bounds> imageBounds = new LinkedList<>();
	protected List<BufferedImage> images = new LinkedList<>();
	protected Bounds mosaicBounds = new Bounds(0, 0, 0, 0);
	
	public ImageMosaic() {
		super();
	}
	
	public ImageMosaic add(BufferedImage image) {
		return add(image, 0, 0);
	}
	
	public ImageMosaic add(BufferedImage image, int x, int y) {
		return add(image, x, y, image.getWidth(), image.getHeight());
	}
	
	public ImageMosaic add(BufferedImage image, int x, int y, int width, int height) {
		images.add(image);
		imageBounds.add(new Bounds(
				x,
				y,
				width,
				height));
		
		updateMosaicBounds(imageBounds.get(imageBounds.size() - 1));
		
		return this;
	}
	
	public BufferedImage getImage(int imageIndex) {
		return images.get(imageIndex);
	}
	
	public Bounds getImageBounds(int imageIndex) {
		return imageBounds.get(imageIndex);
	}
	
	public int getImagesCount() {
		return images.size();
	}
	
	public Bounds getMosaicBounds() {
		return mosaicBounds;
	}
	
	public ImageMosaic packAtlas() {
		mosaicBounds.setX(0);
		mosaicBounds.setY(0);
		mosaicBounds.setWidth(0);
		mosaicBounds.setHeight(0);
		
		if (images.isEmpty()) {
			return this;
		}
		
		for (int index = 0; index < imageBounds.size(); index++) {
			Bounds singleBounds = imageBounds.get(index);
			
			singleBounds.setX(Integer.MIN_VALUE);
			singleBounds.setY(Integer.MIN_VALUE);
		}
		
		int totalWidth = 0;
		
		for (int index = 0; index < images.size(); index++) {
			BufferedImage image = images.get(index);
			
			totalWidth = totalWidth + image.getWidth();
		}
		
		int estimatedAtlasWidth = totalWidth / (int)Math.sqrt(images.size());
		
		int currentX = 0;
		
		for (int index = 0; index < images.size(); index++) {
			BufferedImage image = images.get(index);
			Bounds singleBounds = imageBounds.get(index);
			
			if (image.getWidth() > (estimatedAtlasWidth - currentX)
					&& image.getWidth() < estimatedAtlasWidth) {
				currentX = 0;
			}
			
			int maximumYInCandidateArea = findMaximumYExtend(
					imageBounds,
					currentX,
					currentX + image.getWidth());
			
			currentX = findLeftEdge(
					imageBounds,
					currentX,
					maximumYInCandidateArea);
			
			singleBounds.setX(currentX);
			singleBounds.setY(maximumYInCandidateArea);
			
			updateMosaicBounds(singleBounds);
			
			currentX = currentX + image.getWidth();
		}
		
		return this;
	}
	
	protected int findLeftEdge(List<Bounds> allBounds, int x, int y) {
		int leftEdge = 0;
		
		for (int index = 0; index < allBounds.size(); index++) {
			Bounds singleBounds = allBounds.get(index);
			
			if (singleBounds.getEndX() <= x
					&& singleBounds.getEndY() > y
					&& singleBounds.getY() <= y) {
				leftEdge = Math.max(
						leftEdge,
						singleBounds.getEndX());
			}
		}
		
		return leftEdge;
	}
	
	protected int findMaximumYExtend(List<Bounds> allBounds, int startX, int endX) {
		int maximumY = 0;
		
		for (int index = 0; index < allBounds.size(); index++) {
			Bounds singleBounds = allBounds.get(index);
			
			if (singleBounds.getEndX() > startX
					&& singleBounds.getX() < endX) {
				maximumY = Math.max(
						maximumY,
						singleBounds.getEndY());
			}
		}
		
		return maximumY;
		
	}
	
	protected void updateMosaicBounds(Bounds newlyAddedImageBounds) {
		if (newlyAddedImageBounds.getX() < mosaicBounds.getX()) {
			mosaicBounds.setX(newlyAddedImageBounds.getX());
		}
		
		if (newlyAddedImageBounds.getY() < mosaicBounds.getY()) {
			mosaicBounds.setY(newlyAddedImageBounds.getY());
		}
		
		if (newlyAddedImageBounds.getEndX() > mosaicBounds.getWidth()) {
			mosaicBounds.setWidth(newlyAddedImageBounds.getEndX());
		}
		
		if (newlyAddedImageBounds.getEndY() > mosaicBounds.getHeight()) {
			mosaicBounds.setHeight(newlyAddedImageBounds.getEndY());
		}
	}
	
	public class Bounds {
		protected int height = 0;
		protected int width = 0;
		protected int x = 0;
		protected int y = 0;
		
		public Bounds(int x, int y, int width, int height) {
			super();
			
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}
		
		public int getEndX() {
			return x + width;
		}
		
		public int getEndY() {
			return y + height;
		}
		
		public int getHeight() {
			return height;
		}
		
		public int getWidth() {
			return width;
		}
		
		public int getX() {
			return x;
		}
		
		public int getY() {
			return y;
		}
		
		public Bounds setHeight(int height) {
			this.height = height;
			
			return this;
		}
		
		public Bounds setWidth(int width) {
			this.width = width;
			
			return this;
		}
		
		public Bounds setX(int x) {
			this.x = x;
			
			return this;
		}
		
		public Bounds setY(int y) {
			this.y = y;
			
			return this;
		}
	}
}
