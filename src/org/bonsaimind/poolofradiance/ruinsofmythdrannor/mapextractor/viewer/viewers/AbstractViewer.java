/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import com.sibvisions.rad.ui.swing.ext.JVxSplitPane;

public class AbstractViewer extends JComponent {
	protected JTree propertiesTree = null;
	protected JVxSplitPane splitPane = null;
	
	public AbstractViewer() {
		super();
		
		propertiesTree = new JTree();
		propertiesTree.setModel(null);
		
		JScrollPane propertiesTreeScrollPane = new JScrollPane();
		propertiesTreeScrollPane.setPreferredSize(new Dimension(300, 0));
		propertiesTreeScrollPane.setViewportView(propertiesTree);
		
		splitPane = new JVxSplitPane(JVxSplitPane.HORIZONTAL_SPLIT);
		splitPane.setDividerAlignment(JVxSplitPane.DIVIDER_TOP_LEFT);
		splitPane.setDividerLocation(300);
		splitPane.setLeftComponent(propertiesTreeScrollPane);
		
		setLayout(new BorderLayout());
		add(splitPane, BorderLayout.CENTER);
	}
	
	protected DefaultMutableTreeNode createTreeNode(String format, Object... arguments) {
		DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode();
		treeNode.setUserObject(String.format(format, arguments));
		
		return treeNode;
	}
	
	protected void setMainViewComponent(Component component) {
		splitPane.setRightComponent(component);
	}
}
