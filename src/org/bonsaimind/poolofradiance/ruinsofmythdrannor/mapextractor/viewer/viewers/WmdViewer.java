/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers;

import java.awt.image.BufferedImage;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz.Mcz;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.MapTileEntry;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.Wmd;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.components.ImageMosaicView;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ImageMosaic;

public class WmdViewer extends AbstractViewer {
	protected ImageMosaicView imageMosaicView = null;
	protected Wmd wmd = null;
	
	public WmdViewer() {
		super();
		
		imageMosaicView = new ImageMosaicView();
		
		setMainViewComponent(imageMosaicView);
	}
	
	public WmdViewer setWmd(Wmd wmd) {
		this.wmd = wmd;
		
		if (wmd != null) {
			ImageMosaic imageMosaic = new ImageMosaic();
			
			DefaultMutableTreeNode mapTileEntriesTreeNode = createTreeNode("Entries");
			
			for (int mapTileEntryIndex = 0; mapTileEntryIndex < wmd.getMapTilesEntriesCount(); mapTileEntryIndex++) {
				int mapTileMapping = wmd.getMapTileMapping(mapTileEntryIndex);
				
				if (mapTileMapping >= 0) {
					MapTileEntry mapTileEntry = wmd.getMapTileEntry(mapTileMapping);
					Mcz mcz = mapTileEntry.getMcz();
					int mczX = (mapTileEntryIndex % wmd.getMapColumns()) * Mcz.MCZ_IMAGE_WIDTH;
					int mczY = (mapTileEntryIndex / wmd.getMapColumns()) * Mcz.MCZ_IMAGE_HEIGHT;
					
					DefaultMutableTreeNode mczEntriesTreeNode = createTreeNode("Entries");
					
					for (int mczEntryIndex = 0; mczEntryIndex < mcz.getEntriesCount(); mczEntryIndex++) {
						BufferedImage entryImage = mcz.getEntryImage(mczEntryIndex);
						
						imageMosaic.add(
								entryImage,
								mczX + ((mczEntryIndex % Mcz.MCZ_IMAGE_COLUMNS) * Mcz.ENTRY_IMAGE_WIDTH),
								mczY + ((mczEntryIndex / Mcz.MCZ_IMAGE_COLUMNS) * Mcz.ENTRY_IMAGE_HEIGHT));
						
						DefaultMutableTreeNode mczEntryTreeNode = createTreeNode("%d",
								Integer.valueOf(mczEntryIndex));
						mczEntryTreeNode.add(createTreeNode("Width: %dpx",
								Integer.valueOf(entryImage.getWidth())));
						mczEntryTreeNode.add(createTreeNode("Height: %dpx",
								Integer.valueOf(entryImage.getHeight())));
						mczEntriesTreeNode.add(mczEntryTreeNode);
					}
					
					DefaultMutableTreeNode mczTreeNode = createTreeNode("%d@%dx%d: %s",
							Integer.valueOf(mapTileEntryIndex),
							Integer.valueOf(mapTileEntryIndex % wmd.getMapColumns()),
							Integer.valueOf(mapTileEntryIndex / wmd.getMapColumns()),
							mcz.getName());
					mczTreeNode.add(createTreeNode("Entries count: %d",
							Integer.valueOf(mcz.getEntriesCount())));
					mczTreeNode.add(mczEntriesTreeNode);
					
					mapTileEntriesTreeNode.add(mczTreeNode);
				} else {
					mapTileEntriesTreeNode.add(createTreeNode("%d@%dx%d: <Blank>",
							Integer.valueOf(mapTileEntryIndex),
							Integer.valueOf(mapTileEntryIndex % wmd.getMapColumns()),
							Integer.valueOf(mapTileEntryIndex / wmd.getMapColumns())));
				}
			}
			
			imageMosaic.getMosaicBounds()
					.setWidth(wmd.getMapColumns() * Mcz.MCZ_IMAGE_WIDTH)
					.setHeight(wmd.getMapRows() * Mcz.MCZ_IMAGE_HEIGHT);
			imageMosaicView.setImageMosaic(imageMosaic);
			
			DefaultMutableTreeNode wmdTreeNode = createTreeNode(wmd.getName());
			wmdTreeNode.add(createTreeNode("Map Columns: %d",
					Integer.valueOf(wmd.getMapColumns())));
			wmdTreeNode.add(createTreeNode("Map Rows: %d",
					Integer.valueOf(wmd.getMapRows())));
			wmdTreeNode.add(createTreeNode("Entries count: %d",
					Integer.valueOf(wmd.getMapTilesEntriesCount())));
			wmdTreeNode.add(mapTileEntriesTreeNode);
			
			DefaultTreeModel treeModel = new DefaultTreeModel(wmdTreeNode);
			
			propertiesTree.setModel(treeModel);
			
			propertiesTree.expandPath(new TreePath(new Object[] { wmdTreeNode, mapTileEntriesTreeNode }));
		} else {
			imageMosaicView.setImageMosaic(null);
			propertiesTree.setModel(null);
		}
		
		return this;
	}
}
