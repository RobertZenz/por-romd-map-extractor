/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers;

import java.awt.image.BufferedImage;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz.Mcz;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.components.ImageMosaicView;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ImageMosaic;

public class MczViewer extends AbstractViewer {
	protected ImageMosaicView imageMosaicView = null;
	protected Mcz mcz = null;
	
	public MczViewer() {
		super();
		
		imageMosaicView = new ImageMosaicView();
		
		setMainViewComponent(imageMosaicView);
	}
	
	public MczViewer setMcz(Mcz mcz) {
		this.mcz = mcz;
		
		if (mcz != null) {
			ImageMosaic imageMosaic = new ImageMosaic();
			
			DefaultMutableTreeNode entriesTreeNode = createTreeNode("Entries");
			
			for (int entryIndex = 0; entryIndex < mcz.getEntriesCount(); entryIndex++) {
				BufferedImage entryImage = mcz.getEntryImage(entryIndex);
				
				imageMosaic.add(
						entryImage,
						(entryIndex % Mcz.MCZ_IMAGE_COLUMNS) * Mcz.ENTRY_IMAGE_WIDTH,
						(entryIndex / Mcz.MCZ_IMAGE_COLUMNS) * Mcz.ENTRY_IMAGE_HEIGHT);
				
				DefaultMutableTreeNode entryTreeNode = createTreeNode("%d",
						Integer.valueOf(entryIndex));
				entryTreeNode.add(createTreeNode("Width: %dpx",
						Integer.valueOf(entryImage.getWidth())));
				entryTreeNode.add(createTreeNode("Height: %dpx",
						Integer.valueOf(entryImage.getHeight())));
				entriesTreeNode.add(entryTreeNode);
			}
			
			imageMosaicView.setImageMosaic(imageMosaic);
			
			DefaultMutableTreeNode mczTreeNode = createTreeNode(mcz.getName());
			mczTreeNode.add(createTreeNode("Entries count: %d",
					Integer.valueOf(mcz.getEntriesCount())));
			mczTreeNode.add(entriesTreeNode);
			
			DefaultTreeModel treeModel = new DefaultTreeModel(mczTreeNode);
			
			propertiesTree.setModel(treeModel);
			
			propertiesTree.expandPath(new TreePath(new Object[] { mczTreeNode, entriesTreeNode }));
		} else {
			imageMosaicView.setImageMosaic(null);
			propertiesTree.setModel(null);
		}
		
		return this;
	}
}
