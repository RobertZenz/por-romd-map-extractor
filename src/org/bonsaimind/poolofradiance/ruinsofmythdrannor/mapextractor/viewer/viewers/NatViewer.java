/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.AbstractPolySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Bone;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Face;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometryPhysique;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometrySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Nat;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.PhysiqueVertice;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.PolyList;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.TextureCoordinate;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Transform;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Vertice;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.assets.Assets;
import org.jogamp.java3d.AmbientLight;
import org.jogamp.java3d.Appearance;
import org.jogamp.java3d.BoundingBox;
import org.jogamp.java3d.BoundingSphere;
import org.jogamp.java3d.BranchGroup;
import org.jogamp.java3d.Canvas3D;
import org.jogamp.java3d.ColoringAttributes;
import org.jogamp.java3d.DirectionalLight;
import org.jogamp.java3d.GraphicsConfigTemplate3D;
import org.jogamp.java3d.Group;
import org.jogamp.java3d.LineArray;
import org.jogamp.java3d.LineAttributes;
import org.jogamp.java3d.Material;
import org.jogamp.java3d.OrderedGroup;
import org.jogamp.java3d.PointArray;
import org.jogamp.java3d.PointAttributes;
import org.jogamp.java3d.PolygonAttributes;
import org.jogamp.java3d.RenderingAttributes;
import org.jogamp.java3d.Shape3D;
import org.jogamp.java3d.Texture;
import org.jogamp.java3d.TextureAttributes;
import org.jogamp.java3d.Transform3D;
import org.jogamp.java3d.TransformGroup;
import org.jogamp.java3d.TriangleArray;
import org.jogamp.java3d.utils.behaviors.vp.OrbitBehavior;
import org.jogamp.java3d.utils.geometry.GeometryInfo;
import org.jogamp.java3d.utils.geometry.NormalGenerator;
import org.jogamp.java3d.utils.geometry.Primitive;
import org.jogamp.java3d.utils.geometry.Sphere;
import org.jogamp.java3d.utils.image.TextureLoader;
import org.jogamp.java3d.utils.universe.SimpleUniverse;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Color4f;
import org.jogamp.vecmath.Point3d;
import org.jogamp.vecmath.TexCoord2f;
import org.jogamp.vecmath.Vector3d;
import org.jogamp.vecmath.Vector3f;

public class NatViewer extends AbstractViewer {
	protected Canvas3D canvas3D = null;
	protected BranchGroup modelBranchGroup = null;
	protected Nat nat = null;
	protected OrbitBehavior orbitBehavior = null;
	protected SimpleUniverse universe = null;
	
	public NatViewer() {
		super();
		
		GraphicsConfigTemplate3D graphicsConfigTemplate = new GraphicsConfigTemplate3D();
		
		GraphicsConfiguration graphicsConfiguration = GraphicsEnvironment
				.getLocalGraphicsEnvironment()
				.getDefaultScreenDevice()
				.getBestConfiguration(graphicsConfigTemplate);
		canvas3D = new Canvas3D(graphicsConfiguration);
		
		orbitBehavior = new OrbitBehavior();
		orbitBehavior.setMinRadius(0.1d);
		orbitBehavior.setReverseRotate(true);
		orbitBehavior.setReverseTranslate(true);
		orbitBehavior.setRotationCenter(new Point3d(0.0d, 0.0d, 0.0d));
		orbitBehavior.setSchedulingBounds(new BoundingSphere(new Point3d(0.0d, 0.0d, 0.0d), 900.0d));
		orbitBehavior.setTransFactors(8.0d, 8.0d);
		orbitBehavior.setZoomFactor(8.0d);
		
		universe = new SimpleUniverse(canvas3D);
		universe.getViewer().getView().setBackClipDistance(10000.0d);
		universe.getViewer().getView().setDepthBufferFreezeTransparent(true);
		universe.getViewer().getView().setFrontClipDistance(0.1d);
		universe.getViewingPlatform().setNominalViewingTransform();
		universe.getViewingPlatform().setViewPlatformBehavior(orbitBehavior);
		universe.addBranchGraph(createBackgroundBranchGroup());
		universe.addBranchGraph(createEnvironmentBranchGroup());
		universe.addBranchGraph(createAxisBranchGroup());
		
		setMainViewComponent(canvas3D);
	}
	
	public NatViewer setNat(Nat nat) {
		this.nat = nat;
		
		if (modelBranchGroup != null) {
			universe.getLocale().removeBranchGraph(modelBranchGroup);
			
			modelBranchGroup = null;
		}
		
		if (nat != null) {
			Group modelGroup = new OrderedGroup();
			
			for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
				GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
				
				Transform boneGlobalTransform = nat.getBoneGlobalTransform(geometrySegment.getName());
				
				Transform3D boneTransform3D = new Transform3D();
				boneTransform3D.setTranslation(new Vector3d(
						boneGlobalTransform.getX(),
						boneGlobalTransform.getY(),
						boneGlobalTransform.getZ()));
				
				TransformGroup geometrySegmentGroup = new TransformGroup();
				geometrySegmentGroup.setTransform(boneTransform3D);
				geometrySegmentGroup.addChild(createWireframeVertices(geometrySegment));
				geometrySegmentGroup.addChild(createWireframeEdges(geometrySegment));
				geometrySegmentGroup.addChild(createSurface(geometrySegment));
				
				modelGroup.addChild(geometrySegmentGroup);
			}
			
			for (int geometryPhysiqueIndex = 0; geometryPhysiqueIndex < nat.getGeometryPhysiquesCount(); geometryPhysiqueIndex++) {
				GeometryPhysique geometryPhysique = nat.getGeometryPhysique(geometryPhysiqueIndex);
				
				modelGroup.addChild(createWireframeVertices(geometryPhysique));
				modelGroup.addChild(createWireframeEdges(geometryPhysique));
				modelGroup.addChild(createSurface(geometryPhysique));
			}
			
			modelBranchGroup = new BranchGroup();
			modelBranchGroup.addChild(modelGroup);
			modelBranchGroup.compile();
			
			universe.getLocale().addBranchGraph(modelBranchGroup);
			
			DefaultTreeModel treeModel = new DefaultTreeModel(createNatTreeNode(nat));
			
			propertiesTree.setModel(treeModel);
			
			BoundingBox modelBoundingBox = calculateModelBounds(nat);
			Point3d modelCenterPoint = new Point3d();
			modelBoundingBox.getCenter(modelCenterPoint);
			
			Transform3D viewingPlatformTransform = new Transform3D();
			viewingPlatformTransform.lookAt(
					new Point3d(
							250.0d,
							modelCenterPoint.getY() * 1.5d,
							250.0d),
					modelCenterPoint,
					new Vector3d(0.0d, 1.0d, 0.0d));
			viewingPlatformTransform.invert();
			
			TransformGroup viewingPlatformTransformGroup = universe.getViewingPlatform().getViewPlatformTransform();
			viewingPlatformTransformGroup.setTransform(viewingPlatformTransform);
			
			orbitBehavior.setRotationCenter(modelCenterPoint);
		} else {
			propertiesTree.setModel(null);
		}
		
		return this;
	}
	
	protected BoundingBox calculateModelBounds(Nat nat) {
		BoundingBox bounds = new BoundingBox();
		Point3d point = new Point3d();
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			Transform boneGlobalTransform = nat.getBoneGlobalTransform(geometrySegment.getName());
			
			for (int verticeIndex = 0; verticeIndex < geometrySegment.getVerticesCount(); verticeIndex++) {
				Vertice vertice = geometrySegment.getVertice(verticeIndex);
				
				point.set(
						vertice.getX() + boneGlobalTransform.getX(),
						vertice.getY() + boneGlobalTransform.getY(),
						vertice.getZ() + boneGlobalTransform.getZ());
				
				bounds.combine(point);
			}
		}
		
		return bounds;
	}
	
	protected Shape3D createAxis(float x, float y, float z) {
		ColoringAttributes axisColoringAttributes = new ColoringAttributes();
		axisColoringAttributes.setColor(new Color3f(x, y, z));
		
		LineAttributes axisLineAttributes = new LineAttributes();
		axisLineAttributes.setLineAntialiasingEnable(true);
		axisLineAttributes.setLineWidth(1.0f);
		
		Appearance axisAppearance = new Appearance();
		axisAppearance.setColoringAttributes(axisColoringAttributes);
		axisAppearance.setLineAttributes(axisLineAttributes);
		
		LineArray axisLineArray = new LineArray(2, LineArray.COORDINATES);
		axisLineArray.setCoordinate(0, new Point3d(0.0d, 0.0d, 0.0d));
		axisLineArray.setCoordinate(0, new Point3d(x * 75.0d, y * 75.0d, z * 75.0d));
		
		Shape3D axisShape3D = new Shape3D();
		axisShape3D.setAppearance(axisAppearance);
		axisShape3D.setGeometry(axisLineArray);
		
		return axisShape3D;
	}
	
	protected BranchGroup createAxisBranchGroup() {
		BranchGroup axisBranchGroup = new BranchGroup();
		axisBranchGroup.addChild(createAxis(1.0f, 0.0f, 0.0f));
		axisBranchGroup.addChild(createAxis(0.0f, 1.0f, 0.0f));
		axisBranchGroup.addChild(createAxis(0.0f, 0.0f, 1.0f));
		
		return axisBranchGroup;
	}
	
	protected BranchGroup createBackgroundBranchGroup() {
		TextureLoader backgroundTextureLoader = new TextureLoader(Assets.getSkyboxPng());
		Texture backgroundTexture = backgroundTextureLoader.getTexture();
		
		Appearance backgroundAppearance = new Appearance();
		backgroundAppearance.setTexture(backgroundTexture);
		
		Sphere backgroundSphere = new Sphere(
				1000.0f,
				Primitive.GENERATE_TEXTURE_COORDS | Primitive.GENERATE_NORMALS_INWARD,
				backgroundAppearance);
		backgroundSphere.setAppearance(backgroundAppearance);
		
		BranchGroup backgroundBranchGroup = new BranchGroup();
		backgroundBranchGroup.addChild(backgroundSphere);
		
		return backgroundBranchGroup;
	}
	
	protected DefaultMutableTreeNode createBoneTreeNode(Bone bone) {
		DefaultMutableTreeNode boneTreeNode = createTreeNode("%s",
				bone.getName());
		
		if (bone.getTransform() != null) {
			DefaultMutableTreeNode transformTreeNode = createTreeNode("Transformation: %f %f %f",
					Double.valueOf(bone.getTransform().getX()),
					Double.valueOf(bone.getTransform().getY()),
					Double.valueOf(bone.getTransform().getZ()));
			transformTreeNode.add(createTreeNode("x %f",
					Double.valueOf(bone.getTransform().getX())));
			transformTreeNode.add(createTreeNode("y %f",
					Double.valueOf(bone.getTransform().getY())));
			transformTreeNode.add(createTreeNode("z %f",
					Double.valueOf(bone.getTransform().getZ())));
			
			boneTreeNode.add(transformTreeNode);
		}
		
		if (bone.getBonesCount() > 0) {
			for (int boneIndex = 0; boneIndex < nat.getBonesCount(); boneIndex++) {
				Bone childBone = bone.getBone(boneIndex);
				
				boneTreeNode.add(createBoneTreeNode(childBone));
			}
		}
		
		return boneTreeNode;
	}
	
	protected BranchGroup createEnvironmentBranchGroup() {
		AmbientLight ambientLight = new AmbientLight();
		ambientLight.setColor(new Color3f(0.4f, 0.4f, 0.4f));
		ambientLight.setInfluencingBounds(new BoundingSphere(
				new Point3d(0.0d, 0.0d, 0.0d),
				100000.0d));
		
		DirectionalLight directionalLight = new DirectionalLight();
		directionalLight.setColor(new Color3f(1.0f, 1.0f, 1.0f));
		directionalLight.setDirection(new Vector3f(-0.5f, -0.5f, -0.5f));
		directionalLight.setInfluencingBounds(new BoundingSphere(
				new Point3d(0.0d, 0.0d, 0.0d),
				100000.0d));
		
		BranchGroup environmentBranchGroup = new BranchGroup();
		environmentBranchGroup.addChild(ambientLight);
		environmentBranchGroup.addChild(directionalLight);
		
		return environmentBranchGroup;
	}
	
	protected DefaultMutableTreeNode createGeometryPhysiqueTreeNode(GeometryPhysique geometryPhysique) {
		DefaultMutableTreeNode geometryPhysiqueTreeNode = createTreeNode("%s",
				geometryPhysique.getName());
		
		for (int polySegmentIndex = 0; polySegmentIndex < geometryPhysique.getPolySegmentsCount(); polySegmentIndex++) {
			AbstractPolySegment polySegment = geometryPhysique.getPolySegment(polySegmentIndex);
			
			DefaultMutableTreeNode facesTreeNode = createTreeNode("Faces [%d]",
					Integer.valueOf(polySegment.getFacesCount()));
			
			for (int faceIndex = 0; faceIndex < polySegment.getFacesCount(); faceIndex++) {
				Face face = polySegment.getFace(faceIndex);
				
				DefaultMutableTreeNode faceTreeNode = createTreeNode("%d: %d %d",
						Integer.valueOf(faceIndex),
						Integer.valueOf(face.getVerticeIndex()),
						Integer.valueOf(face.getTextureCoordinateIndex()));
				faceTreeNode.add(createTreeNode("Vertice Index: %d",
						Integer.valueOf(face.getVerticeIndex())));
				faceTreeNode.add(createTreeNode("Texture Coordinate Index: %d",
						Integer.valueOf(face.getTextureCoordinateIndex())));
				
				facesTreeNode.add(faceTreeNode);
			}
			
			DefaultMutableTreeNode polySegmentTreeNode = createTreeNode("%d: %s",
					Integer.valueOf(polySegmentIndex),
					polySegment.getClass().getSimpleName());
			polySegmentTreeNode.add(createTreeNode("Texture: %s",
					polySegment.getTexture()));
			polySegmentTreeNode.add(createTreeNode("TextureState: %s",
					polySegment.getTextureState().name()));
			polySegmentTreeNode.add(facesTreeNode);
			
			geometryPhysiqueTreeNode.add(polySegmentTreeNode);
		}
		
		return geometryPhysiqueTreeNode;
	}
	
	protected DefaultMutableTreeNode createGeometrySegmentTreeNode(GeometrySegment geometrySegment) {
		DefaultMutableTreeNode verticesTreeNode = createTreeNode("Vertices (%d)",
				Integer.valueOf(geometrySegment.getVerticesCount()));
		
		for (int verticeIndex = 0; verticeIndex < geometrySegment.getVerticesCount(); verticeIndex++) {
			Vertice vertice = geometrySegment.getVertice(verticeIndex);
			
			DefaultMutableTreeNode verticeTreeNode = createTreeNode("%d: %f %f %f",
					Integer.valueOf(verticeIndex),
					Double.valueOf(vertice.getX()),
					Double.valueOf(vertice.getY()),
					Double.valueOf(vertice.getZ()));
			verticeTreeNode.add(createTreeNode("x: %f",
					Double.valueOf(vertice.getX())));
			verticeTreeNode.add(createTreeNode("y: %f",
					Double.valueOf(vertice.getY())));
			verticeTreeNode.add(createTreeNode("z: %f",
					Double.valueOf(vertice.getZ())));
			
			verticesTreeNode.add(verticeTreeNode);
		}
		
		DefaultMutableTreeNode textureCoordinatesTreeNode = createTreeNode("Texture Coordinates (%d)",
				Integer.valueOf(geometrySegment.getTextureCoordinatesCount()));
		
		for (int textureCoordinateIndex = 0; textureCoordinateIndex < geometrySegment.getTextureCoordinatesCount(); textureCoordinateIndex++) {
			TextureCoordinate textureCoordinate = geometrySegment.getTextureCoordinate(textureCoordinateIndex);
			
			DefaultMutableTreeNode textureCoordinateTreeNode = createTreeNode("%d: %f %f",
					Integer.valueOf(textureCoordinateIndex),
					Double.valueOf(textureCoordinate.getU()),
					Double.valueOf(textureCoordinate.getV()));
			textureCoordinateTreeNode.add(createTreeNode("u: %f",
					Double.valueOf(textureCoordinate.getU())));
			textureCoordinateTreeNode.add(createTreeNode("v: %f",
					Double.valueOf(textureCoordinate.getV())));
			
			textureCoordinatesTreeNode.add(textureCoordinateTreeNode);
		}
		
		DefaultMutableTreeNode facesTreeNode = createTreeNode("Faces (%d)",
				Integer.valueOf(geometrySegment.getFacesCount()));
		
		for (int faceIndex = 0; faceIndex < geometrySegment.getFacesCount(); faceIndex++) {
			Face face = geometrySegment.getFace(faceIndex);
			
			DefaultMutableTreeNode faceTreeNode = createTreeNode("%d: %d %d",
					Integer.valueOf(faceIndex),
					Integer.valueOf(face.getVerticeIndex()),
					Integer.valueOf(face.getTextureCoordinateIndex()));
			faceTreeNode.add(createTreeNode("Vertice: %d",
					Integer.valueOf(face.getVerticeIndex())));
			faceTreeNode.add(createTreeNode("Texture Coordinate: %d",
					Integer.valueOf(face.getTextureCoordinateIndex())));
			
			facesTreeNode.add(faceTreeNode);
		}
		
		DefaultMutableTreeNode geometrySegmentTreeNode = createTreeNode("%s",
				geometrySegment.getName());
		geometrySegmentTreeNode.add(createTreeNode("Texture: %s",
				geometrySegment.getTexture()));
		geometrySegmentTreeNode.add(createTreeNode("TextureState: %s",
				geometrySegment.getTextureState().name()));
		geometrySegmentTreeNode.add(verticesTreeNode);
		geometrySegmentTreeNode.add(textureCoordinatesTreeNode);
		geometrySegmentTreeNode.add(facesTreeNode);
		
		return geometrySegmentTreeNode;
	}
	
	protected DefaultMutableTreeNode createNatTreeNode(Nat nat) {
		DefaultMutableTreeNode bonesTreeNode = createTreeNode("Bones");
		
		for (int boneIndex = 0; boneIndex < nat.getBonesCount(); boneIndex++) {
			Bone bone = nat.getBone(boneIndex);
			
			bonesTreeNode.add(createBoneTreeNode(bone));
		}
		
		DefaultMutableTreeNode geometrySegmentsTreeNode = createTreeNode("GeometrySegments");
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			geometrySegmentsTreeNode.add(createGeometrySegmentTreeNode(geometrySegment));
		}
		
		DefaultMutableTreeNode geometryPhysiquesTreeNode = createTreeNode("GeometryPhysiques");
		
		for (int geometryPhysiqueIndex = 0; geometryPhysiqueIndex < nat.getGeometryPhysiquesCount(); geometryPhysiqueIndex++) {
			GeometryPhysique geometryPhysique = nat.getGeometryPhysique(geometryPhysiqueIndex);
			
			geometryPhysiquesTreeNode.add(createGeometryPhysiqueTreeNode(geometryPhysique));
		}
		
		DefaultMutableTreeNode natTreeNode = createTreeNode(nat.getName());
		natTreeNode.add(bonesTreeNode);
		natTreeNode.add(geometrySegmentsTreeNode);
		natTreeNode.add(geometryPhysiquesTreeNode);
		
		return natTreeNode;
	}
	
	protected Group createSurface(GeometryPhysique geometryPhysique) {
		Group group = new Group();
		
		for (int polySegmentIndex = 0; polySegmentIndex < geometryPhysique.getPolySegmentsCount(); polySegmentIndex++) {
			AbstractPolySegment polySegment = geometryPhysique.getPolySegment(polySegmentIndex);
			
			if (polySegment instanceof PolyList) {
				PolyList polyList = (PolyList)polySegment;
				
				TriangleArray surfaceTriangleArray = new TriangleArray(
						polyList.getFacesCount(),
						TriangleArray.COORDINATES | TriangleArray.NORMALS | TriangleArray.TEXTURE_COORDINATE_2);
				
				for (int faceIndex = 0; faceIndex < polyList.getFacesCount(); faceIndex += 3) {
					Face firstFace = polyList.getFace(faceIndex + 0);
					Face secondFace = polyList.getFace(faceIndex + 1);
					Face thirdFace = polyList.getFace(faceIndex + 2);
					
					Vertice firstVertice = geometryPhysique.getPhysiqueVertice(firstFace.getVerticeIndex());
					Vertice secondVertice = geometryPhysique.getPhysiqueVertice(secondFace.getVerticeIndex());
					Vertice thirdVertice = geometryPhysique.getPhysiqueVertice(thirdFace.getVerticeIndex());
					
					TextureCoordinate firstTextureCoordinate = geometryPhysique.getTextureCoordinate(firstFace.getTextureCoordinateIndex());
					TextureCoordinate secondTextureCoordinate = geometryPhysique.getTextureCoordinate(secondFace.getTextureCoordinateIndex());
					TextureCoordinate thirdTextureCoordinate = geometryPhysique.getTextureCoordinate(thirdFace.getTextureCoordinateIndex());
					
					surfaceTriangleArray.setCoordinate(faceIndex + 0, new Point3d(
							firstVertice.getX(),
							firstVertice.getY(),
							firstVertice.getZ()));
					surfaceTriangleArray.setTextureCoordinate(0, faceIndex + 0, new TexCoord2f(
							(float)firstTextureCoordinate.getU(),
							1.0f - (float)firstTextureCoordinate.getV()));
					
					surfaceTriangleArray.setCoordinate(faceIndex + 1, new Point3d(
							secondVertice.getX(),
							secondVertice.getY(),
							secondVertice.getZ()));
					surfaceTriangleArray.setTextureCoordinate(0, faceIndex + 1, new TexCoord2f(
							(float)secondTextureCoordinate.getU(),
							1.0f - (float)secondTextureCoordinate.getV()));
					
					surfaceTriangleArray.setCoordinate(faceIndex + 2, new Point3d(
							thirdVertice.getX(),
							thirdVertice.getY(),
							thirdVertice.getZ()));
					surfaceTriangleArray.setTextureCoordinate(0, faceIndex + 2, new TexCoord2f(
							(float)thirdTextureCoordinate.getU(),
							1.0f - (float)thirdTextureCoordinate.getV()));
				}
				
				GeometryInfo surfaceGeometryInfo = new GeometryInfo(surfaceTriangleArray);
				new NormalGenerator().generateNormals(surfaceGeometryInfo);
				
				Shape3D surfaceShape = new Shape3D();
				surfaceShape.setAppearance(createSurfaceAppearance(polyList.getTextureInk()));
				surfaceShape.setGeometry(surfaceGeometryInfo.getGeometryArray());
				
				group.addChild(surfaceShape);
			}
		}
		
		return group;
	}
	
	protected Shape3D createSurface(GeometrySegment geometrySegment) {
		TriangleArray surfaceTriangleArray = new TriangleArray(
				geometrySegment.getFacesCount(),
				TriangleArray.COORDINATES | TriangleArray.NORMALS | TriangleArray.TEXTURE_COORDINATE_2);
		
		for (int faceIndex = 0; faceIndex < geometrySegment.getFacesCount(); faceIndex += 3) {
			Face firstFace = geometrySegment.getFace(faceIndex + 0);
			Face secondFace = geometrySegment.getFace(faceIndex + 1);
			Face thirdFace = geometrySegment.getFace(faceIndex + 2);
			
			Vertice firstVertice = geometrySegment.getVertice(firstFace.getVerticeIndex());
			Vertice secondVertice = geometrySegment.getVertice(secondFace.getVerticeIndex());
			Vertice thirdVertice = geometrySegment.getVertice(thirdFace.getVerticeIndex());
			
			TextureCoordinate firstTextureCoordinate = geometrySegment.getTextureCoordinate(firstFace.getTextureCoordinateIndex());
			TextureCoordinate secondTextureCoordinate = geometrySegment.getTextureCoordinate(secondFace.getTextureCoordinateIndex());
			TextureCoordinate thirdTextureCoordinate = geometrySegment.getTextureCoordinate(thirdFace.getTextureCoordinateIndex());
			
			surfaceTriangleArray.setCoordinate(faceIndex + 0, new Point3d(
					firstVertice.getX(),
					firstVertice.getY(),
					firstVertice.getZ()));
			surfaceTriangleArray.setTextureCoordinate(0, faceIndex + 0, new TexCoord2f(
					(float)firstTextureCoordinate.getU(),
					1.0f - (float)firstTextureCoordinate.getV()));
			
			surfaceTriangleArray.setCoordinate(faceIndex + 1, new Point3d(
					secondVertice.getX(),
					secondVertice.getY(),
					secondVertice.getZ()));
			surfaceTriangleArray.setTextureCoordinate(0, faceIndex + 1, new TexCoord2f(
					(float)secondTextureCoordinate.getU(),
					1.0f - (float)secondTextureCoordinate.getV()));
			
			surfaceTriangleArray.setCoordinate(faceIndex + 2, new Point3d(
					thirdVertice.getX(),
					thirdVertice.getY(),
					thirdVertice.getZ()));
			surfaceTriangleArray.setTextureCoordinate(0, faceIndex + 2, new TexCoord2f(
					(float)thirdTextureCoordinate.getU(),
					1.0f - (float)thirdTextureCoordinate.getV()));
		}
		
		GeometryInfo surfaceGeometryInfo = new GeometryInfo(surfaceTriangleArray);
		new NormalGenerator().generateNormals(surfaceGeometryInfo);
		
		Shape3D surfaceShape = new Shape3D();
		surfaceShape.setAppearance(createSurfaceAppearance(geometrySegment.getTextureInk()));
		surfaceShape.setGeometry(surfaceGeometryInfo.getGeometryArray());
		
		return surfaceShape;
	}
	
	protected Appearance createSurfaceAppearance(Ink textureInk) {
		Material surfaceMaterial = new Material();
		surfaceMaterial.setAmbientColor(new Color3f(1.0f, 1.0f, 1.0f));
		surfaceMaterial.setDiffuseColor(new Color3f(1.0f, 1.0f, 1.0f));
		surfaceMaterial.setSpecularColor(new Color3f(1.0f, 1.0f, 1.0f));
		
		PolygonAttributes surfacePolygonAttributes = new PolygonAttributes();
		surfacePolygonAttributes.setPolygonMode(PolygonAttributes.POLYGON_FILL);
		
		TextureLoader surfaceTextureLoader = new TextureLoader(textureInk.getImage());
		
		Texture surfaceTexture = surfaceTextureLoader.getTexture();
		surfaceTexture.setMagFilter(Texture.FASTEST);
		surfaceTexture.setMinFilter(Texture.FASTEST);
		
		TextureAttributes surfaceTextureAttributes = new TextureAttributes();
		surfaceTextureAttributes.setPerspectiveCorrectionMode(TextureAttributes.NICEST);
		surfaceTextureAttributes.setTextureMode(TextureAttributes.COMBINE);
		surfaceTextureAttributes.setTextureBlendColor(new Color4f(1.0f, 1.0f, 1.0f, 1.0f));
		
		Appearance surfaceAppearance = new Appearance();
		surfaceAppearance.setMaterial(surfaceMaterial);
		surfaceAppearance.setPolygonAttributes(surfacePolygonAttributes);
		surfaceAppearance.setTexture(surfaceTexture);
		surfaceAppearance.setTextureAttributes(surfaceTextureAttributes);
		
		return surfaceAppearance;
	}
	
	protected Appearance createWireframeEdgeAppearance() {
		ColoringAttributes edgesColoringAttributes = new ColoringAttributes();
		edgesColoringAttributes.setColor(new Color3f(0.0f, 1.0f, 1.0f));
		
		LineAttributes edgesLineAttributes = new LineAttributes();
		edgesLineAttributes.setLineAntialiasingEnable(true);
		edgesLineAttributes.setLineWidth(1.0f);
		
		RenderingAttributes edgesRenderingAttributes = new RenderingAttributes();
		
		Appearance edgesAppearance = new Appearance();
		edgesAppearance.setColoringAttributes(edgesColoringAttributes);
		edgesAppearance.setLineAttributes(edgesLineAttributes);
		edgesAppearance.setRenderingAttributes(edgesRenderingAttributes);
		
		return edgesAppearance;
	}
	
	protected Shape3D createWireframeEdges(GeometryPhysique geometryPhysique) {
		List<Face> mergedFaces = new ArrayList<>();
		
		for (int polySegmentIndex = 0; polySegmentIndex < geometryPhysique.getPolySegmentsCount(); polySegmentIndex++) {
			AbstractPolySegment polySegment = geometryPhysique.getPolySegment(polySegmentIndex);
			
			for (int faceIndex = 0; faceIndex < polySegment.getFacesCount(); faceIndex++) {
				Face face = polySegment.getFace(faceIndex);
				
				mergedFaces.add(face);
			}
		}
		
		LineArray edgesLineArray = new LineArray(
				mergedFaces.size() * 2,
				LineArray.COORDINATES);
		
		for (int faceIndex = 0; faceIndex < mergedFaces.size(); faceIndex += 3) {
			Face firstFace = mergedFaces.get(faceIndex + 0);
			Face secondFace = mergedFaces.get(faceIndex + 1);
			Face thirdFace = mergedFaces.get(faceIndex + 2);
			
			PhysiqueVertice firstPhysiqueVertice = geometryPhysique.getPhysiqueVertice(firstFace.getVerticeIndex());
			PhysiqueVertice secondPhysiqueVertice = geometryPhysique.getPhysiqueVertice(secondFace.getVerticeIndex());
			PhysiqueVertice thirdPhysiqueVertice = geometryPhysique.getPhysiqueVertice(thirdFace.getVerticeIndex());
			
			Transform firstBoneTransform = nat.getBoneGlobalTransform(firstPhysiqueVertice.getBoneName());
			Transform secondBoneTransform = nat.getBoneGlobalTransform(secondPhysiqueVertice.getBoneName());
			Transform thirdBoneTransform = nat.getBoneGlobalTransform(thirdPhysiqueVertice.getBoneName());
			
			int edgesArrayOffset = faceIndex * 2;
			
			edgesLineArray.setCoordinate(edgesArrayOffset + 0, new Point3d(
					firstPhysiqueVertice.getX() + firstBoneTransform.getX(),
					firstPhysiqueVertice.getY() + firstBoneTransform.getY(),
					firstPhysiqueVertice.getZ() + firstBoneTransform.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 1, new Point3d(
					secondPhysiqueVertice.getX() + secondBoneTransform.getX(),
					secondPhysiqueVertice.getY() + secondBoneTransform.getY(),
					secondPhysiqueVertice.getZ() + secondBoneTransform.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 2, new Point3d(
					secondPhysiqueVertice.getX() + secondBoneTransform.getX(),
					secondPhysiqueVertice.getY() + secondBoneTransform.getY(),
					secondPhysiqueVertice.getZ() + secondBoneTransform.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 3, new Point3d(
					thirdPhysiqueVertice.getX() + thirdBoneTransform.getX(),
					thirdPhysiqueVertice.getY() + thirdBoneTransform.getY(),
					thirdPhysiqueVertice.getZ() + thirdBoneTransform.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 4, new Point3d(
					thirdPhysiqueVertice.getX() + thirdBoneTransform.getX(),
					thirdPhysiqueVertice.getY() + thirdBoneTransform.getY(),
					thirdPhysiqueVertice.getZ() + thirdBoneTransform.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 5, new Point3d(
					firstPhysiqueVertice.getX() + firstBoneTransform.getX(),
					firstPhysiqueVertice.getY() + firstBoneTransform.getY(),
					firstPhysiqueVertice.getZ() + firstBoneTransform.getZ()));
		}
		
		Shape3D edgesShape = new Shape3D();
		edgesShape.setAppearance(createWireframeEdgeAppearance());
		edgesShape.setGeometry(edgesLineArray);
		
		return edgesShape;
	}
	
	protected Shape3D createWireframeEdges(GeometrySegment geometrySegment) {
		LineArray edgesLineArray = new LineArray(
				geometrySegment.getFacesCount() * 2,
				LineArray.COORDINATES);
		
		for (int faceIndex = 0; faceIndex < geometrySegment.getFacesCount(); faceIndex += 3) {
			Face firstFace = geometrySegment.getFace(faceIndex + 0);
			Face secondFace = geometrySegment.getFace(faceIndex + 1);
			Face thirdFace = geometrySegment.getFace(faceIndex + 2);
			
			Vertice firstVertice = geometrySegment.getVertice(firstFace.getVerticeIndex());
			Vertice secondVertice = geometrySegment.getVertice(secondFace.getVerticeIndex());
			Vertice thirdVertice = geometrySegment.getVertice(thirdFace.getVerticeIndex());
			
			int edgesArrayOffset = faceIndex * 2;
			
			edgesLineArray.setCoordinate(edgesArrayOffset + 0, new Point3d(
					firstVertice.getX(),
					firstVertice.getY(),
					firstVertice.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 1, new Point3d(
					secondVertice.getX(),
					secondVertice.getY(),
					secondVertice.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 2, new Point3d(
					secondVertice.getX(),
					secondVertice.getY(),
					secondVertice.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 3, new Point3d(
					thirdVertice.getX(),
					thirdVertice.getY(),
					thirdVertice.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 4, new Point3d(
					thirdVertice.getX(),
					thirdVertice.getY(),
					thirdVertice.getZ()));
			edgesLineArray.setCoordinate(edgesArrayOffset + 5, new Point3d(
					firstVertice.getX(),
					firstVertice.getY(),
					firstVertice.getZ()));
		}
		
		Shape3D edgesShape = new Shape3D();
		edgesShape.setAppearance(createWireframeEdgeAppearance());
		edgesShape.setGeometry(edgesLineArray);
		
		return edgesShape;
	}
	
	protected Appearance createWireframeVerticeAppearance() {
		ColoringAttributes verticesColoringAttributes = new ColoringAttributes();
		verticesColoringAttributes.setColor(new Color3f(1.0f, 0.0f, 1.0f));
		
		PointAttributes verticesPointAttributes = new PointAttributes();
		verticesPointAttributes.setPointAntialiasingEnable(true);
		verticesPointAttributes.setPointSize(8.0f);
		
		RenderingAttributes verticesRenderingAttributes = new RenderingAttributes();
		
		Appearance verticesAppearance = new Appearance();
		verticesAppearance.setColoringAttributes(verticesColoringAttributes);
		verticesAppearance.setPointAttributes(verticesPointAttributes);
		verticesAppearance.setRenderingAttributes(verticesRenderingAttributes);
		
		return verticesAppearance;
	}
	
	protected Shape3D createWireframeVertices(GeometryPhysique geometryPhysique) {
		PointArray verticesPointArray = new PointArray(geometryPhysique.getPhysiqueVerticesCount(), PointArray.COORDINATES);
		
		for (int verticeIndex = 0; verticeIndex < geometryPhysique.getPhysiqueVerticesCount(); verticeIndex++) {
			PhysiqueVertice physiqueVertice = geometryPhysique.getPhysiqueVertice(verticeIndex);
			Transform boneTransform = nat.getBoneGlobalTransform(physiqueVertice.getBoneName());
			
			verticesPointArray.setCoordinate(verticeIndex, new Point3d(
					physiqueVertice.getX() + boneTransform.getX(),
					physiqueVertice.getY() + boneTransform.getY(),
					physiqueVertice.getZ() + boneTransform.getZ()));
		}
		
		Shape3D verticesShape = new Shape3D();
		verticesShape.setAppearance(createWireframeVerticeAppearance());
		verticesShape.setGeometry(verticesPointArray);
		
		return verticesShape;
	}
	
	protected Shape3D createWireframeVertices(GeometrySegment geometrySegment) {
		PointArray verticesPointArray = new PointArray(
				geometrySegment.getVerticesCount(),
				PointArray.COORDINATES);
		
		for (int verticeIndex = 0; verticeIndex < geometrySegment.getVerticesCount(); verticeIndex++) {
			Vertice vertice = geometrySegment.getVertice(verticeIndex);
			
			verticesPointArray.setCoordinate(verticeIndex, new Point3d(
					vertice.getX(),
					vertice.getY(),
					vertice.getZ()));
		}
		
		Shape3D verticesShape = new Shape3D();
		verticesShape.setAppearance(createWireframeVerticeAppearance());
		verticesShape.setGeometry(verticesPointArray);
		
		return verticesShape;
	}
}
