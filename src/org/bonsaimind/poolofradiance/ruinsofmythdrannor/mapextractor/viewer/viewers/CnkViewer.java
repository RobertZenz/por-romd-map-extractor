/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers;

import java.awt.image.BufferedImage;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.cnk.Cnk;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.StringUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.components.ImageMosaicView;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.support.ImageMosaic;

public class CnkViewer extends AbstractViewer {
	protected Cnk cnk = null;
	protected ImageMosaicView imageMosaicView = null;
	
	public CnkViewer() {
		super();
		
		imageMosaicView = new ImageMosaicView();
		
		setMainViewComponent(imageMosaicView);
	}
	
	public CnkViewer setCnk(Cnk cnk) {
		this.cnk = cnk;
		
		if (cnk != null) {
			ImageMosaic imageMosaic = new ImageMosaic();
			
			DefaultMutableTreeNode entriesTreeNode = createTreeNode("Entries");
			
			for (int entryIndex = 0; entryIndex < cnk.getEntriesCount(); entryIndex++) {
				Ink ink = cnk.getEntryInk(entryIndex);
				BufferedImage inkImage = ink.getImage();
				
				imageMosaic.add(inkImage);
				
				DefaultMutableTreeNode colorChannelsTreeNode = createTreeNode("Color Channels");
				colorChannelsTreeNode.add(createTreeNode("Red Bitmask: %s",
						StringUtil.toTwoByteBitMask(ink.getBitMaskRed())));
				colorChannelsTreeNode.add(createTreeNode("Green Bitmask: %s",
						StringUtil.toTwoByteBitMask(ink.getBitMaskGreen())));
				colorChannelsTreeNode.add(createTreeNode("Blue Bitmask: %s",
						StringUtil.toTwoByteBitMask(ink.getBitMaskBlue())));
				colorChannelsTreeNode.add(createTreeNode("Red Shift Value: %s",
						Integer.valueOf(ink.getBitShiftRed())));
				colorChannelsTreeNode.add(createTreeNode("Blue Shift Value: %s",
						Integer.valueOf(ink.getBitShiftBlue())));
				colorChannelsTreeNode.add(createTreeNode("Green Shift Value: %s",
						Integer.valueOf(ink.getBitShiftGreen())));
				
				DefaultMutableTreeNode inkTreeNode = createTreeNode("%d: %s",
						Integer.valueOf(entryIndex),
						ink.getName());
				inkTreeNode.add(createTreeNode("Length: %d bytes",
						Integer.valueOf(ink.getLength())));
				inkTreeNode.add(createTreeNode("Width: %dpx",
						Integer.valueOf(ink.getWidth())));
				inkTreeNode.add(createTreeNode("Height: %dpx",
						Integer.valueOf(ink.getHeight())));
				inkTreeNode.add(colorChannelsTreeNode);
				
				entriesTreeNode.add(inkTreeNode);
			}
			
			imageMosaic.packAtlas();
			
			imageMosaicView.setImageMosaic(imageMosaic);
			
			DefaultMutableTreeNode cnkTreeNode = createTreeNode(cnk.getName());
			cnkTreeNode.add(createTreeNode("File Length: %d",
					Integer.valueOf(cnk.getFileLength())));
			cnkTreeNode.add(createTreeNode("Entries Count: %d",
					Integer.valueOf(cnk.getEntriesCount())));
			cnkTreeNode.add(entriesTreeNode);
			
			DefaultTreeModel treeModel = new DefaultTreeModel(cnkTreeNode);
			
			propertiesTree.setModel(treeModel);
			
			propertiesTree.expandPath(new TreePath(new Object[] { cnkTreeNode, entriesTreeNode }));
		} else {
			imageMosaicView.setImageMosaic(null);
			propertiesTree.setModel(null);
		}
		
		return this;
	}
}
