/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.viewers;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.StringUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.components.ImageView;

public class InkViewer extends AbstractViewer {
	protected ImageView imageView = null;
	protected Ink ink = null;
	
	public InkViewer() {
		super();
		
		imageView = new ImageView();
		
		setMainViewComponent(imageView);
	}
	
	public InkViewer setInk(Ink ink) {
		this.ink = ink;
		
		if (ink != null) {
			imageView.setImage(ink.getImage());
			
			DefaultMutableTreeNode colorChannelsTreeNode = createTreeNode("Color Channels");
			colorChannelsTreeNode.add(createTreeNode("Red Bitmask: %s",
					StringUtil.toTwoByteBitMask(ink.getBitMaskRed())));
			colorChannelsTreeNode.add(createTreeNode("Green Bitmask: %s",
					StringUtil.toTwoByteBitMask(ink.getBitMaskGreen())));
			colorChannelsTreeNode.add(createTreeNode("Blue Bitmask: %s",
					StringUtil.toTwoByteBitMask(ink.getBitMaskBlue())));
			colorChannelsTreeNode.add(createTreeNode("Red Shift Value: %s",
					Integer.valueOf(ink.getBitShiftRed())));
			colorChannelsTreeNode.add(createTreeNode("Blue Shift Value: %s",
					Integer.valueOf(ink.getBitShiftBlue())));
			colorChannelsTreeNode.add(createTreeNode("Green Shift Value: %s",
					Integer.valueOf(ink.getBitShiftGreen())));
			
			DefaultMutableTreeNode inkTreeNode = createTreeNode(ink.getName());
			inkTreeNode.add(createTreeNode("Length: %d bytes",
					Integer.valueOf(ink.getLength())));
			inkTreeNode.add(createTreeNode("Width: %dpx",
					Integer.valueOf(ink.getWidth())));
			inkTreeNode.add(createTreeNode("Height: %dpx",
					Integer.valueOf(ink.getHeight())));
			inkTreeNode.add(colorChannelsTreeNode);
			
			DefaultTreeModel treeModel = new DefaultTreeModel(inkTreeNode);
			
			propertiesTree.setModel(treeModel);
		} else {
			imageView.setImage(null);
			propertiesTree.setModel(null);
		}
		
		return this;
	}
}
