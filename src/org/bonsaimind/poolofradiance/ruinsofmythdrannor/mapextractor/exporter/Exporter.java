/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.exporter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.Parameters;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.CnkReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.InkReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.MczReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.NatReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.WmdReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.cnk.Cnk;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz.Mcz;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.AbstractPolySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometryPhysique;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometrySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Nat;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support.PathUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.MapTileEntry;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.Wmd;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.FileContent;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ImageStitcher;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ObjBundle;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.ObjCreator;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.StringUtil;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;

public class Exporter {
	private static final Logger LOGGER = new Logger(Exporter.class);
	
	protected CnkReader cnkReader = null;
	protected ImageStitcher imageStitcher = null;
	protected InkReader inkReader = null;
	protected MczReader mczReader = null;
	protected NatReader natReader = null;
	protected ObjCreator objCreator = null;
	protected WmdReader wmdReader = null;
	
	public Exporter() {
		super();
		
		inkReader = new InkReader();
		cnkReader = new CnkReader(inkReader);
		mczReader = new MczReader();
		natReader = new NatReader();
		wmdReader = new WmdReader();
		
		imageStitcher = new ImageStitcher();
		objCreator = new ObjCreator();
	}
	
	public void export(Parameters parameters) {
		Path targetDirectoryPath = null;
		
		if (parameters.getTargetDirectory() != null) {
			targetDirectoryPath = parameters.getTargetDirectory();
		} else {
			targetDirectoryPath = Paths.get("");
		}
		
		String targetImageFileExtension = parameters.getImageType().toLowerCase();
		
		for (String additionalParameter : parameters.getAdditionalParameters()) {
			try {
				Path filePath = Paths.get(additionalParameter);
				String fileExtension = PathUtil.getExtension(filePath)
						.toLowerCase();
				
				switch (fileExtension) {
					case "cnk":
						exportCnk(
								filePath,
								parameters.getImageType(),
								targetDirectoryPath);
						break;
					
					case "ink":
						exportInk(
								filePath,
								parameters.getImageType(),
								getTargetFilePath(filePath, targetDirectoryPath, targetImageFileExtension));
						break;
					
					case "mcz":
						exportMcz(
								filePath,
								parameters.getImageType(),
								getTargetFilePath(filePath, targetDirectoryPath, targetImageFileExtension));
						break;
					
					case "nat":
						exportNat(
								filePath,
								getTargetFilePath(filePath, targetDirectoryPath, "obj"),
								parameters.getDataDirectory());
						break;
					
					case "wmd":
						exportWmd(
								filePath,
								parameters.getImageType(),
								getTargetFilePath(filePath, targetDirectoryPath, targetImageFileExtension),
								parameters.getDataDirectory());
						break;
					
					default:
						LOGGER.error("Don't understand how to process parameter <%s>, skipping.",
								additionalParameter);
						break;
					
				}
			} catch (Throwable th) {
				LOGGER.error("Failed to process parameter <%s>, skipping.",
						additionalParameter,
						th);
			}
		}
	}
	
	public void exportCnk(Path cnkFilePath, String imageType, Path targetDirectoryPath) throws IOException {
		Cnk cnk = cnkReader.read(cnkFilePath);
		
		Path cnkTargetDirectory = targetDirectoryPath.resolve(cnk.getName());
		Files.createDirectories(cnkTargetDirectory);
		
		for (int entryIndex = 0; entryIndex < cnk.getEntriesCount(); entryIndex++) {
			Ink entryInk = cnk.getEntryInk(entryIndex);
			Path targetFilePath = getTargetFilePath(
					entryInk.getName(),
					cnkTargetDirectory,
					imageType.toLowerCase());
			
			ImageIO.write(
					entryInk.getImage(),
					imageType,
					targetFilePath.toFile());
		}
	}
	
	public void exportInk(Path inkFilePath, String imageType, Path targetFilePath) throws IOException {
		Ink ink = inkReader.read(inkFilePath);
		
		ImageIO.write(
				ink.getImage(),
				imageType,
				targetFilePath.toFile());
	}
	
	public void exportMcz(Path mczFilePath, String imageType, Path targetFilePath) throws IOException {
		Mcz mcz = mczReader.read(mczFilePath);
		
		ImageIO.write(
				imageStitcher.stitchImage(mcz),
				imageType,
				targetFilePath.toFile());
	}
	
	public void exportNat(Path natFilePath, Path targetFilePath, Path dataDirectoryPath) throws IOException {
		Nat nat = natReader.read(natFilePath);
		
		Path inkDirectoryPath = null;
		
		if (dataDirectoryPath != null) {
			inkDirectoryPath = dataDirectoryPath.resolve("INK");
		} else {
			inkDirectoryPath = natFilePath.getParent().getParent().getParent().resolve("INK");
		}
		
		for (int geometrySegmentIndex = 0; geometrySegmentIndex < nat.getGeometrySegmentsCount(); geometrySegmentIndex++) {
			GeometrySegment geometrySegment = nat.getGeometrySegment(geometrySegmentIndex);
			
			if (geometrySegment.getTexture() != null
					&& !geometrySegment.getTexture().isEmpty()) {
				String textureName = PathUtil.stripExtension(geometrySegment.getTexture());
				Path textureInkPath = PathUtil.tryResolveCaseInsensitive(
						inkDirectoryPath,
						textureName + ".ink");
				
				if (textureInkPath == null) {
					String numberPostfix = StringUtil.getNumberPostfix(textureName);
					
					if (numberPostfix != null) {
						String textureNameWithInsertedZero = textureName.substring(0, textureName.length() - numberPostfix.length())
								+ "0"
								+ numberPostfix;
						
						textureInkPath = PathUtil.tryResolveCaseInsensitive(
								inkDirectoryPath,
								textureNameWithInsertedZero + ".ink");
					}
				}
				
				if (textureInkPath == null) {
					throw new IOException(String.format("Could not resolve <%s> to INK/texture file.",
							geometrySegment.getTexture()));
				}
				
				Ink textureInk = inkReader.read(textureInkPath);
				
				geometrySegment.setTextureInk(textureInk);
			}
		}
		
		Path cnkFilePath = PathUtil.resolveCaseInsensitive(
				inkDirectoryPath,
				PathUtil.stripExtension(natFilePath.getFileName().toString()) + ".cnk");
		Cnk cnk = cnkReader.read(cnkFilePath);
		
		for (int geometryPhysiqueIndex = 0; geometryPhysiqueIndex < nat.getGeometryPhysiquesCount(); geometryPhysiqueIndex++) {
			GeometryPhysique geometryPhysique = nat.getGeometryPhysique(geometryPhysiqueIndex);
			
			for (int polySegmentIndex = 0; polySegmentIndex < geometryPhysique.getPolySegmentsCount(); polySegmentIndex++) {
				AbstractPolySegment polySegment = geometryPhysique.getPolySegment(polySegmentIndex);
				
				if (polySegment.getTexture() != null
						&& !polySegment.getTexture().isEmpty()) {
					String textureName = PathUtil.stripExtension(polySegment.getTexture());
					Ink textureInk = cnk.getEntryByName(textureName + ".ink");
					
					if (textureInk == null) {
						Path textureInkPath = PathUtil.tryResolveCaseInsensitive(
								inkDirectoryPath,
								textureName + ".ink");
						
						if (textureInkPath == null) {
							String numberPostfix = StringUtil.getNumberPostfix(textureName);
							
							if (numberPostfix != null) {
								String textureNameWithInsertedZero = textureName.substring(0, textureName.length() - numberPostfix.length())
										+ "0"
										+ numberPostfix;
								
								textureInkPath = PathUtil.tryResolveCaseInsensitive(
										inkDirectoryPath,
										textureNameWithInsertedZero + ".ink");
							}
						}
						
						if (textureInkPath == null) {
							throw new IOException(String.format("Could not resolve <%s> to INK/texture file.",
									polySegment.getTexture()));
						}
						
						textureInk = inkReader.read(textureInkPath);
					}
					
					polySegment.setTextureInk(textureInk);
				}
			}
		}
		
		ObjBundle objBundle = objCreator.createObj(nat, PathUtil.stripExtension(nat.getName()));
		
		Path targetDirectory = targetFilePath.getParent();
		
		Files.write(
				targetFilePath,
				objBundle.getObjFileContent().getContent().getBytes(StandardCharsets.UTF_8));
		
		Files.write(
				targetDirectory.resolve(objBundle.getMdlFileContent().getFileName()),
				objBundle.getMdlFileContent().getContent().getBytes(StandardCharsets.UTF_8));
		
		for (FileContent<byte[]> textureFileContent : objBundle.getTextureFileContents()) {
			Files.write(
					targetDirectory.resolve(textureFileContent.getFileName()),
					textureFileContent.getContent());
		}
	}
	
	public void exportWmd(Path wmdFilePath, String imageType, Path targetFilePath, Path dataDirectoryPath) throws IOException {
		Wmd wmd = wmdReader.read(wmdFilePath);
		
		Path mczDirectoryPath = null;
		
		if (dataDirectoryPath != null) {
			mczDirectoryPath = dataDirectoryPath.resolve("MCZ");
		} else {
			mczDirectoryPath = wmdFilePath.getParent().getParent().resolve("MCZ");
		}
		
		for (int entryIndex = 0; entryIndex < wmd.getMapTilesEntriesCount(); entryIndex++) {
			MapTileEntry mapTileEntry = wmd.getMapTileEntry(entryIndex);
			Path mczFilePath = mczDirectoryPath.resolve(mapTileEntry.getName());
			Mcz mcz = mczReader.read(mczFilePath);
			
			mapTileEntry.setMcz(mcz);
		}
		
		ImageIO.write(
				imageStitcher.stitchImage(wmd),
				imageType,
				targetFilePath.toFile());
	}
	
	protected Path getTargetFilePath(Path filePath, Path targetDirectoryPath, String targetFileExtension) {
		return getTargetFilePath(
				filePath.getFileName().toString(),
				targetDirectoryPath,
				targetFileExtension);
	}
	
	protected Path getTargetFilePath(String sourceFileName, Path targetDirectoryPath, String targetFileExtension) {
		String targetFileName = PathUtil.stripExtension(sourceFileName)
				+ "."
				+ targetFileExtension;
		Path targetFilePath = targetDirectoryPath.resolve(targetFileName);
		
		return targetFilePath;
	}
}
