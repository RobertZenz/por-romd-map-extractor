/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor;

import java.io.IOException;
import java.nio.file.Files;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.Parameters.OperationMode;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.exporter.Exporter;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.viewer.ViewerFrame;

public final class Main {
	private static final Logger LOGGER = new Logger(Main.class);
	
	private Main() {
	}
	
	public static void main(String[] args) {
		Parameters parameters = new Parameters(args);
		
		if (parameters.getHelp()
				|| (parameters.getOperationMode() == OperationMode.EXPORT && parameters.getAdditionalParameters().isEmpty())) {
			System.out.println("Pool of Radiance: Ruins of Myth Drannor - Map Extractor " + Version.CURRENT);
			
			try {
				System.out.println(Help.getHelpText());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			System.exit(0);
		}
		
		Logger.setGlobalClassnameTagLevel(parameters.getClassnameTagLevel());
		Logger.setGlobalLevel(parameters.getLogLevel());
		Logger.setGlobalLevelTagLevel(parameters.getLevelTagLevel());
		
		if (parameters.getDataDirectory() != null
				&& !Files.isDirectory(parameters.getDataDirectory())) {
			LOGGER.error("Data directory <%s> does not exist or is not a directory.",
					parameters.getDataDirectory());
			
			System.exit(1);
		}
		
		if (parameters.getTargetDirectory() != null
				&& !Files.isDirectory(parameters.getTargetDirectory())) {
			LOGGER.error("Target directory <%s> does not exist or is not a directory.",
					parameters.getTargetDirectory());
			
			System.exit(1);
		}
		
		switch (parameters.getOperationMode()) {
			case EXPORT:
				runExport(parameters);
				break;
			
			case VIEWER:
				runViewer(parameters);
				break;
		}
	}
	
	private static final void createAndShowViewerFrame(Parameters parameters) {
		ViewerFrame viewerFrame = new ViewerFrame(parameters);
		
		viewerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		viewerFrame.setVisible(true);
	}
	
	private static final void runExport(Parameters parameters) {
		Exporter exporter = new Exporter();
		
		exporter.export(parameters);
	}
	
	private static final void runViewer(Parameters parameters) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				createAndShowViewerFrame(parameters);
			});
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}
}
