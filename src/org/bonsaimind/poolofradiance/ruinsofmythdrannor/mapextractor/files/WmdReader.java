/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support.BinaryReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.MapTileEntry;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd.Wmd;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;

/**
 * WMD files contain the index data to stitch together multiple MCZ files into a
 * map.
 * <p>
 * The WMD starts with a 40 byte header, which contains the following 32-bit
 * integers (little endianness) after each other:
 * <p>
 * <ol>
 * <li>0x00: Unknown</li>
 * <li>0x04: int32 - Number of horizontal tiles.</li>
 * <li>0x08: int32 - Number of vertical tiles.</li>
 * <li>0x0C: Unknown</li>
 * <li>0x10: Unknown</li>
 * <li>0x14: Unknown</li>
 * <li>0x18: Unknown</li>
 * <li>0x1C: Unknown</li>
 * <li>0x20: int32 - Number of total map tiles.</li>
 * <li>0x24: Unknown</li>
 * </ol>
 * <p>
 * Afterwards follows a section with "number of map tiles" 128-Byte structures
 * with each specifying a MCZ file which is used on this map. Only the first
 * 14-byte are known to be the name (might be padded with nulls), the rest is
 * unknown. Offsets are relative.
 * <p>
 * <ol>
 * <li>0x00: 14 byte string - Name of the entry, null terminated.</li>
 * <li>0x1e: Unknown data.</li>
 * </ol>
 * <p>
 * After this follows the index data to map each map tile to its location on the
 * map, with the relative offset being the index/position of the tile and the
 * value (32-bit integer (little endianness)) being the index from the map tile
 * section above it. Offsets are relative.
 * <p>
 * <ol>
 * <li>0x00: int32 - Index of tile at position 0.</li>
 * <li>0x04: int32 - Index of tile at position 1.</li>
 * <li>0x08: int32 - Index of tile at position 2.</li>
 * <li>...</li>
 * </ol>
 * <p>
 * The index might be less than zero to indicate blank tiles.
 * <p>
 * The rest of the file contains unknown data.
 */
public class WmdReader {
	protected static final int MAP_TILE_ENTRY_NAME_MAXIMUM_LENGTH = 14;
	protected static final int MAP_TILE_ENTRY_SIZE = 128;
	private static final Logger LOGGER = new Logger(WmdReader.class);
	
	public WmdReader() {
		super();
	}
	
	public Wmd read(Path wmdFilePath) throws IOException {
		LOGGER.debug("Reading WMD file from <%s>.",
				wmdFilePath);
		
		return read(
				wmdFilePath.getFileName().toString(),
				Files.newInputStream(wmdFilePath));
	}
	
	public Wmd read(String name, InputStream wmdInputStream) throws IOException {
		LOGGER.debug("Reading WMD <%s>.",
				name);
		
		try (BinaryReader reader = new BinaryReader(wmdInputStream)) {
			int unknownHeaderFieldAtOffset0x00 = reader.readInt32();
			int mapColumns = reader.readInt32();
			int napRows = reader.readInt32();
			int unknownHeaderFieldAtOffset0x0c = reader.readInt32();
			int unknownHeaderFieldAtOffset0x10 = reader.readInt32();
			int unknownHeaderFieldAtOffset0x14 = reader.readInt32();
			int unknownHeaderFieldAtOffset0x18 = reader.readInt32();
			int unknownHeaderFieldAtOffset0x1c = reader.readInt32();
			int mapTilesEntriesCount = reader.readInt32();
			int unknownHeaderFieldAtOffset0x24 = reader.readInt32();
			
			LOGGER.debug("WMD <%s> has size <%dx%d> with total tiles <%d>.",
					name,
					Integer.valueOf(mapColumns),
					Integer.valueOf(napRows),
					Integer.valueOf(mapTilesEntriesCount));
			
			MapTileEntry[] mapTileEntries = new MapTileEntry[mapTilesEntriesCount];
			
			for (int mapTileEntryIndex = 0; mapTileEntryIndex < mapTilesEntriesCount; mapTileEntryIndex++) {
				String mapTileEntryName = reader.readStringNullTerminated(MAP_TILE_ENTRY_NAME_MAXIMUM_LENGTH);
				byte[] unknownDataAfterOffset0x1e = reader.readBlock(MAP_TILE_ENTRY_SIZE - MAP_TILE_ENTRY_NAME_MAXIMUM_LENGTH);
				
				mapTileEntries[mapTileEntryIndex] = new MapTileEntry(
						mapTileEntryName,
						unknownDataAfterOffset0x1e);
			}
			
			int[] mapTilesMapping = new int[mapTilesEntriesCount];
			
			for (int mapTileMappingIndex = 0; mapTileMappingIndex < mapTilesEntriesCount; mapTileMappingIndex++) {
				mapTilesMapping[mapTileMappingIndex] = reader.readInt32();
			}
			
			byte[] unknownTrailingData = reader.readRemaining();
			
			return new Wmd(
					name,
					unknownHeaderFieldAtOffset0x00,
					mapColumns,
					napRows,
					unknownHeaderFieldAtOffset0x0c,
					unknownHeaderFieldAtOffset0x10,
					unknownHeaderFieldAtOffset0x14,
					unknownHeaderFieldAtOffset0x18,
					unknownHeaderFieldAtOffset0x1c,
					mapTilesEntriesCount,
					unknownHeaderFieldAtOffset0x24,
					mapTileEntries,
					mapTilesMapping,
					unknownTrailingData);
		}
	}
}
