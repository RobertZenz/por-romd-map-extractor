
/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd;

/**
 * WMD files contain the index data to stitch together multiple MCZ files into a
 * map.
 * <p>
 * The WMD starts with a 40 byte header, which contains the following 32-bit
 * integers (little endianness) after each other:
 * <p>
 * <ol>
 * <li>0x00: Unknown</li>
 * <li>0x04: int32 - Number of horizontal tiles.</li>
 * <li>0x08: int32 - Number of vertical tiles.</li>
 * <li>0x0C: Unknown</li>
 * <li>0x10: Unknown</li>
 * <li>0x14: Unknown</li>
 * <li>0x18: Unknown</li>
 * <li>0x1C: Unknown</li>
 * <li>0x20: int32 - Number of total map tiles.</li>
 * <li>0x24: Unknown</li>
 * </ol>
 * <p>
 * Afterwards follows a section with "number of map tiles" 128-Byte structures
 * with each specifying a MCZ file which is used on this map. Only the first
 * 14-byte are known to be the name (might be padded with nulls), the rest is
 * unknown. Offsets are relative.
 * <p>
 * <ol>
 * <li>0x00: 14 byte string - Name of the entry, null terminated.</li>
 * <li>0x1e: Unknown data.</li>
 * </ol>
 * <p>
 * After this follows the index data to map each map tile to its location on the
 * map, with the relative offset being the index/position of the tile and the
 * value (32-bit integer (little endianness)) being the index from the map tile
 * section above it. Offsets are relative.
 * <p>
 * <ol>
 * <li>0x00: int32 - Index of tile at position 0.</li>
 * <li>0x04: int32 - Index of tile at position 1.</li>
 * <li>0x08: int32 - Index of tile at position 2.</li>
 * <li>...</li>
 * </ol>
 * <p>
 * The index might be less than zero to indicate blank tiles.
 * <p>
 * The rest of the file contains unknown data.
 */
public class Wmd {
	protected int mapColumns = -1;
	protected int mapRows = -1;
	protected MapTileEntry[] mapTileEntries = null;
	protected int mapTilesEntriesCount = -1;
	protected int[] mapTilesMapping = null;
	protected String name = null;
	protected int unknownHeaderFieldAtOffset0x00 = -1;
	protected int unknownHeaderFieldAtOffset0x0c = -1;
	protected int unknownHeaderFieldAtOffset0x10 = -1;
	protected int unknownHeaderFieldAtOffset0x14 = -1;
	protected int unknownHeaderFieldAtOffset0x18 = -1;
	protected int unknownHeaderFieldAtOffset0x1c = -1;
	protected int unknownHeaderFieldAtOffset0x24 = -1;
	protected byte[] unknownTrailingData = null;
	
	public Wmd(
			String name,
			int unknownHeaderFieldAtOffset0x00,
			int mapColumns,
			int mapRows,
			int unknownHeaderFieldAtOffset0x0c,
			int unknownHeaderFieldAtOffset0x10,
			int unknownHeaderFieldAtOffset0x14,
			int unknownHeaderFieldAtOffset0x18,
			int unknownHeaderFieldAtOffset0x1c,
			int mapTilesEntriesCount,
			int unknownHeaderFieldAtOffset0x24,
			MapTileEntry[] mapTileEntries,
			int[] mapTilesMapping,
			byte[] unknownTrailingData) {
		super();
		
		this.name = name;
		this.unknownHeaderFieldAtOffset0x00 = unknownHeaderFieldAtOffset0x00;
		this.mapColumns = mapColumns;
		this.mapRows = mapRows;
		this.unknownHeaderFieldAtOffset0x0c = unknownHeaderFieldAtOffset0x0c;
		this.unknownHeaderFieldAtOffset0x10 = unknownHeaderFieldAtOffset0x10;
		this.unknownHeaderFieldAtOffset0x14 = unknownHeaderFieldAtOffset0x14;
		this.unknownHeaderFieldAtOffset0x18 = unknownHeaderFieldAtOffset0x18;
		this.unknownHeaderFieldAtOffset0x1c = unknownHeaderFieldAtOffset0x1c;
		this.mapTilesEntriesCount = mapTilesEntriesCount;
		this.unknownHeaderFieldAtOffset0x24 = unknownHeaderFieldAtOffset0x24;
		this.mapTileEntries = mapTileEntries;
		this.unknownTrailingData = unknownTrailingData;
		this.mapTilesMapping = mapTilesMapping;
	}
	
	public int getMapColumns() {
		return mapColumns;
	}
	
	public int getMapRows() {
		return mapRows;
	}
	
	public MapTileEntry getMapTileEntry(int mapTileEntry) {
		return mapTileEntries[mapTileEntry];
	}
	
	public int getMapTileMapping(int index) {
		return mapTilesMapping[index];
	}
	
	public int getMapTilesEntriesCount() {
		return mapTilesEntriesCount;
	}
	
	public String getName() {
		return name;
	}
	
	public int getUnknownHeaderFieldAtOffset0x00() {
		return unknownHeaderFieldAtOffset0x00;
	}
	
	public int getUnknownHeaderFieldAtOffset0x0c() {
		return unknownHeaderFieldAtOffset0x0c;
	}
	
	public int getUnknownHeaderFieldAtOffset0x10() {
		return unknownHeaderFieldAtOffset0x10;
	}
	
	public int getUnknownHeaderFieldAtOffset0x14() {
		return unknownHeaderFieldAtOffset0x14;
	}
	
	public int getUnknownHeaderFieldAtOffset0x18() {
		return unknownHeaderFieldAtOffset0x18;
	}
	
	public int getUnknownHeaderFieldAtOffset0x1c() {
		return unknownHeaderFieldAtOffset0x1c;
	}
	
	public int getUnknownHeaderFieldAtOffset0x24() {
		return unknownHeaderFieldAtOffset0x24;
	}
	
	public byte[] getUnknownTrailingData() {
		return unknownTrailingData;
	}
}
