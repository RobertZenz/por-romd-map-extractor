/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.wmd;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz.Mcz;

/**
 * Afterwards follows a section with "number of map tiles" 128-Byte structures
 * with each specifying a MCZ file which is used on this map. Only the first
 * 14-byte are known to be the name (might be padded with nulls), the rest is
 * unknown. Offsets are relative.
 * <p>
 * <ol>
 * <li>0x00: 14 byte string - Name of the entry, null terminated.</li>
 * <li>0x1e: Unknown data.</li>
 * </ol>
 * <p>
 */
public class MapTileEntry {
	protected Mcz mcz = null;
	protected String name = null;
	protected byte[] unknownDataAfterOffset0x1e = null;
	
	public MapTileEntry(
			String name,
			byte[] unknownDataAfterOffset0x1e) {
		super();
		
		this.name = name;
		this.unknownDataAfterOffset0x1e = unknownDataAfterOffset0x1e;
	}
	
	public Mcz getMcz() {
		return mcz;
	}
	
	public String getName() {
		return name;
	}
	
	public byte[] getUnknownDataAfterOffset0x1e() {
		return unknownDataAfterOffset0x1e;
	}
	
	public MapTileEntry setMcz(Mcz mcz) {
		this.mcz = mcz;
		
		return this;
	}
}
