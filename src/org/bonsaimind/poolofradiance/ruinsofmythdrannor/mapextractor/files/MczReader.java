/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz.Mcz;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support.BinaryReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;

/**
 * MCZ files contain 130 128x128 pixel images (SingleTile).
 * <p>
 * The MCZ file starts with with 260 4-byte values defining the size of each
 * individual image in the file, each size is composed of two 32-bit integers
 * (little endianness) which are added to get the length of each tile. The tiles
 * itself are arranged (in order) in 13 columns and 10 rows with each being
 * 128x128 pixels in size.
 * <p>
 * The JPEGs in the MCZ file are also little endianness, so they are BGR instead
 * of RGB.
 */
public class MczReader {
	private static final Logger LOGGER = new Logger(MczReader.class);
	
	public MczReader() {
	}
	
	public Mcz read(Path mczFilePath) throws IOException {
		LOGGER.debug("Reading MCZ file from <%s>.",
				mczFilePath);
		
		return read(
				mczFilePath.getFileName().toString(),
				Files.newInputStream(mczFilePath));
	}
	
	public Mcz read(String name, InputStream mczInputStream) throws IOException {
		LOGGER.debug("Reading MCZ <%s>.",
				name);
		
		try (BinaryReader reader = new BinaryReader(mczInputStream)) {
			int[] entryLengths = new int[Mcz.ENTRIES_COUNT];
			
			for (int entryIndex = 0; entryIndex < Mcz.ENTRIES_COUNT; entryIndex++) {
				int entryLength = reader.readInt32() + reader.readInt32();
				
				entryLengths[entryIndex] = entryLength;
				
				LOGGER.debug("Reading MCZ <%s> entry <%d> length <%d>.",
						name,
						Integer.valueOf(entryIndex),
						Integer.valueOf(entryLength));
			}
			
			BufferedImage[] entriesImages = new BufferedImage[Mcz.ENTRIES_COUNT];
			
			for (int entryIndex = 0; entryIndex < Mcz.ENTRIES_COUNT; entryIndex++) {
				int entryLength = entryLengths[entryIndex];
				
				LOGGER.debug("Reading MCZ <%s> entry <%d> with length <%d>.",
						name,
						Integer.valueOf(entryIndex),
						Integer.valueOf(entryLength));
				
				byte[] entryImageData = reader.readBlock(entryLength);
				BufferedImage entryImage = convertToImage(entryImageData);
				
				entriesImages[entryIndex] = entryImage;
			}
			
			return new Mcz(
					name,
					Mcz.ENTRIES_COUNT,
					entryLengths,
					entriesImages);
		}
	}
	
	protected BufferedImage convertToImage(
			byte[] entryImageData) {
		try {
			ImageInputStream entryImageInputStream = ImageIO.createImageInputStream(new ByteArrayInputStream(entryImageData));
			BufferedImage entryImage = ImageIO.read(entryImageInputStream);
			
			int[] partArgb = entryImage.getRGB(
					0,
					0,
					entryImage.getWidth(),
					entryImage.getHeight(),
					null,
					0,
					entryImage.getWidth());
			
			for (int index = 0; index < partArgb.length; index++) {
				int argb = partArgb[index];
				
				int alpha = (argb >>> 24) & 0xff;
				int red = (argb >>> 16) & 0xff;
				int green = (argb >>> 8) & 0xff;
				int blue = (argb >>> 0) & 0xff;
				
				// Actual swap.
				int tempRed = red;
				red = blue;
				blue = tempRed;
				
				int newArgb = 0;
				newArgb = newArgb | (alpha << 24);
				newArgb = newArgb | (red << 16);
				newArgb = newArgb | (green << 8);
				newArgb = newArgb | (blue << 0);
				
				partArgb[index] = newArgb;
			}
			
			entryImage.setRGB(
					0,
					0,
					entryImage.getWidth(),
					entryImage.getHeight(),
					partArgb,
					0,
					entryImage.getWidth());
			
			return entryImage;
		} catch (IOException e) {
			// Should not happen.
			
			throw new IllegalStateException(e);
		}
	}
}
