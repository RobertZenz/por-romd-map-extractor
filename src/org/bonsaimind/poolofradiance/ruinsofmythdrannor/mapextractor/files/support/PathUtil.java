/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public final class PathUtil {
	private PathUtil() {
	}
	
	public static final String addExtension(String name, String extension) {
		if (name.toLowerCase().endsWith("." + extension.toLowerCase())) {
			return name;
		} else {
			return name + "." + extension;
		}
	}
	
	public static final String getExtension(Path filePath) {
		return getExtension(filePath.getFileName().toString());
	}
	
	public static final String getExtension(String fileName) {
		int lastDotIndex = fileName.lastIndexOf(".");
		
		if (lastDotIndex >= 0) {
			return fileName.substring(lastDotIndex + 1);
		} else {
			return "";
		}
	}
	
	public static final Path resolveCaseInsensitive(Path path, String name) throws IOException {
		Path resolvedPath = tryResolveCaseInsensitive(path, name);
		
		if (resolvedPath == null) {
			throw new FileNotFoundException(path.resolve(name).toString());
		}
		
		return resolvedPath;
	}
	
	public static final Path resolveDataFile(Path dataDirectory, String type, String filename) throws IOException {
		Path dataTypeDirectory = resolveCaseInsensitive(dataDirectory, type);
		String fileNameWithExtension = addExtension(filename, type);
		
		return resolveCaseInsensitive(dataTypeDirectory, fileNameWithExtension);
	}
	
	public static String stripExtension(Path path) {
		return stripExtension(path.getFileName());
	}
	
	public static String stripExtension(String fileName) {
		int dotIndex = fileName.lastIndexOf(".");
		
		if (dotIndex > 0) {
			return fileName.substring(0, dotIndex);
		} else {
			return fileName;
		}
	}
	
	public static final Path tryResolveCaseInsensitive(Path path, String name) throws IOException {
		Path resolvedPath = path.resolve(name);
		
		if (Files.exists(resolvedPath)) {
			return resolvedPath;
		}
		
		Path upperCaseResolvedPath = path.resolve(name.toUpperCase());
		
		if (Files.exists(upperCaseResolvedPath)) {
			return upperCaseResolvedPath;
		}
		
		Path lowerCaseResolvedPath = path.resolve(name.toLowerCase());
		
		if (Files.exists(lowerCaseResolvedPath)) {
			return lowerCaseResolvedPath;
		}
		
		return null;
	}
}
