/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.PoolOfRadianceRuinsOfMythDrannor;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;

public class BinaryReader implements Closeable {
	private static final Logger LOGGER = new Logger(BinaryReader.class);
	
	protected byte[] buffer = new byte[4];
	protected InputStream inputStream = null;
	
	public BinaryReader(InputStream inputStream) {
		super();
		
		this.inputStream = inputStream;
	}
	
	@Override
	public void close() throws IOException {
		if (inputStream != null) {
			inputStream.close();
		}
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}
	
	public boolean isMoreAvailable() {
		try {
			return inputStream.available() > 0;
		} catch (IOException e) {
			return false;
		}
	}
	
	public void readBlock(byte[] block) throws IOException {
		readBlock(block, block.length);
	}
	
	public void readBlock(byte[] block, int length) throws IOException {
		int readTotal = 0;
		
		while (readTotal < length) {
			int readCurrent = inputStream.read(block, readTotal, length - readTotal);
			
			if (readCurrent < 0) {
				LOGGER.error("Tried to read <%s> bytes but only <%s> available.",
						Integer.valueOf(length),
						Integer.valueOf(readCurrent));
				
				throw new IOException(String.format("Tried to read <%s> bytes but only <%s> available.",
						Integer.valueOf(length),
						Integer.valueOf(readCurrent)));
			}
			
			readTotal = readTotal + readCurrent;
		}
	}
	
	public byte[] readBlock(int blockSize) throws IOException {
		byte[] block = new byte[blockSize];
		
		readBlock(block);
		
		return block;
	}
	
	public byte readByte() throws IOException {
		readBlock(buffer, 1);
		
		return buffer[0];
	}
	
	public int readInt16() throws IOException {
		readBlock(buffer, 2);
		
		int value = ((buffer[1] & 0xff) << 8)
				| ((buffer[0] & 0xff) << 0);
		
		return value;
	}
	
	public int readInt32() throws IOException {
		readBlock(buffer, 4);
		
		int value = ((buffer[3] & 0xff) << 24)
				| ((buffer[2] & 0xff) << 16)
				| ((buffer[1] & 0xff) << 8)
				| ((buffer[0] & 0xff) << 0);
		
		return value;
	}
	
	public int readInt8() throws IOException {
		readBlock(buffer, 1);
		
		int value = buffer[0] & 0xff;
		
		return value;
	}
	
	public byte[] readRemaining() throws IOException {
		try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
			byte[] readBuffer = new byte[1024 * 64];
			int read = -1;
			
			while ((read = inputStream.read(readBuffer)) >= 0) {
				byteArrayOutputStream.write(readBuffer, 0, read);
			}
			
			return byteArrayOutputStream.toByteArray();
		}
	}
	
	public String readStringNullTerminated(int maximumLength) throws IOException {
		byte[] bytes = readBlock(maximumLength);
		int realLength = findFirst(bytes, (byte)0);
		
		if (realLength < 0) {
			realLength = maximumLength;
		}
		
		return new String(bytes, 0, realLength, PoolOfRadianceRuinsOfMythDrannor.CHARSET);
	}
	
	public void skip(int count) throws IOException {
		inputStream.skip(count);
	}
	
	public void skipAsUnknown(int count) throws IOException {
		skip(count);
	}
	
	protected int findFirst(byte[] array, byte value) {
		for (int index = 0; index < array.length; index++) {
			if (array[index] == value) {
				return index;
			}
		}
		
		return -1;
	}
}
