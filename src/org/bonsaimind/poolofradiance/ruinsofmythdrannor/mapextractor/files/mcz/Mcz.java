/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.mcz;

import java.awt.image.BufferedImage;

public class Mcz {
	public static final int ENTRIES_COUNT = 130;
	public static final int ENTRY_IMAGE_HEIGHT = 128;
	public static final int ENTRY_IMAGE_WIDTH = 128;
	public static final int MCZ_IMAGE_COLUMNS = 13;
	public static final int MCZ_IMAGE_HEIGHT = 128 * 10;
	public static final int MCZ_IMAGE_ROWS = 10;
	public static final int MCZ_IMAGE_WIDTH = 128 * 13;
	
	protected int entriesCount = -1;
	protected BufferedImage[] entriesImages = null;
	protected int[] entryLengths = null;
	protected String name = null;
	
	public Mcz(
			String name,
			int entriesCount,
			int[] entryLengths,
			BufferedImage[] entriesImages) {
		super();
		
		this.name = name;
		this.entriesCount = entriesCount;
		this.entryLengths = entryLengths;
		this.entriesImages = entriesImages;
	}
	
	public int getEntriesCount() {
		return entriesCount;
	}
	
	public BufferedImage getEntryImage(int entryIndex) {
		return entriesImages[entryIndex];
	}
	
	public int getEntryLength(int entryIndex) {
		return entryLengths[entryIndex];
	}
	
	public String getName() {
		return name;
	}
}
