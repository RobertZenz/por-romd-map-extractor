/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.cnb;

/**
 * The CNB files are archives of NAB files (or any files).
 * <p>
 * The CNB file starts with meta-information about the file and a list of entry
 * names, followed by the entries themselves.
 * <p>
 * <ol>
 * <li>0x00: int32 - Number of entries in this file.</li>
 * <li>0x04: int32 - The total length of the file.</li>
 * <li>0x08: Entries index</li>
 * <li>0x++: Entries</li>
 * </ol>
 * <p>
 * The entries index consists of "number of entries" index structures (offsets
 * are relative).
 * <p>
 * <ol>
 * <li>0x00: 52 byte string - Name of the entry, null terminated, remaining
 * space after the null is filled with "=".</li>
 * <li>0x34: int32 - Offset of the entry in this file.</li>
 * </ol>
 * <p>
 * The remaining file consists of the NAB entries without additional
 * information.
 */
public class Cnb {
	protected int entriesCount = -1;
	protected byte[][] entriesData = null;
	protected String[] entryNames = null;
	protected int[] entryOffsets = null;
	protected int fileLength = -1;
	protected String name = null;
	
	public Cnb(
			String name,
			int entriesCount,
			int fileLength,
			String[] entryNames,
			int[] entryOffsets,
			byte[][] entriesData) {
		super();
		
		this.name = name;
		this.entriesCount = entriesCount;
		this.fileLength = fileLength;
		this.entryNames = entryNames;
		this.entryOffsets = entryOffsets;
		this.entriesData = entriesData;
	}
	
	public int getEntriesCount() {
		return entriesCount;
	}
	
	public byte[] getEntryData(int entryIndex) {
		return entriesData[entryIndex];
	}
	
	public String getEntryName(int entryIndex) {
		return entryNames[entryIndex];
	}
	
	public int getEntryOffset(int entryIndex) {
		return entryOffsets[entryIndex];
	}
	
	public int getFileLength() {
		return fileLength;
	}
	
	public String getName() {
		return name;
	}
}
