/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;

public class GeometrySegment {
	protected Face[] faces = null;
	protected String name = null;
	protected String texture = null;
	protected TextureCoordinate[] textureCoordinates = null;
	protected Ink textureInk = null;
	protected TextureState textureState = null;
	protected Vertice[] vertices = null;
	
	public GeometrySegment(
			String name,
			Vertice[] vertices,
			TextureCoordinate[] textureCoordinates,
			String texture,
			TextureState textureState,
			Face[] faces) {
		super();
		
		this.name = name;
		this.vertices = vertices;
		this.textureCoordinates = textureCoordinates;
		this.texture = texture;
		this.textureState = textureState;
		this.faces = faces;
	}
	
	public Face getFace(int faceIndex) {
		return faces[faceIndex];
	}
	
	public int getFacesCount() {
		return faces.length;
	}
	
	public String getName() {
		return name;
	}
	
	public String getTexture() {
		return texture;
	}
	
	public TextureCoordinate getTextureCoordinate(int textureCoordinateIndex) {
		return textureCoordinates[textureCoordinateIndex];
	}
	
	public int getTextureCoordinatesCount() {
		return textureCoordinates.length;
	}
	
	public Ink getTextureInk() {
		return textureInk;
	}
	
	public TextureState getTextureState() {
		return textureState;
	}
	
	public Vertice getVertice(int verticeIndex) {
		return vertices[verticeIndex];
	}
	
	public int getVerticesCount() {
		return vertices.length;
	}
	
	public void setTextureInk(Ink textureInk) {
		this.textureInk = textureInk;
	}
}
