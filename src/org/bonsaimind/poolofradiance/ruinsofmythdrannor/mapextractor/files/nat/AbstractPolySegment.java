/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;

public abstract class AbstractPolySegment {
	protected Face[] faces = null;
	protected String texture = null;
	protected Ink textureInk = null;
	protected TextureState textureState = null;
	
	protected AbstractPolySegment(
			String texture,
			TextureState textureState,
			Face[] faces) {
		super();
		
		this.texture = texture;
		this.textureState = textureState;
		this.faces = faces;
	}
	
	public Face getFace(int faceIndex) {
		return faces[faceIndex];
	}
	
	public int getFacesCount() {
		return faces.length;
	}
	
	public String getTexture() {
		return texture;
	}
	
	public Ink getTextureInk() {
		return textureInk;
	}
	
	public TextureState getTextureState() {
		return textureState;
	}
	
	public void setTextureInk(Ink textureInk) {
		this.textureInk = textureInk;
	}
}
