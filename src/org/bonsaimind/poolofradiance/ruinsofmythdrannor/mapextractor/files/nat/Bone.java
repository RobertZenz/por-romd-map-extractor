/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat;

public class Bone {
	protected Bone[] bones = null;
	protected String name = null;
	protected Transform transform = null;
	
	public Bone(
			String name,
			Transform transform,
			Bone[] bones) {
		super();
		
		this.name = name;
		this.transform = transform;
		this.bones = bones;
	}
	
	public Bone getBone(int boneIndex) {
		return bones[boneIndex];
	}
	
	public int getBonesCount() {
		if (bones == null) {
			return 0;
		}
		
		return bones.length;
	}
	
	public String getName() {
		return name;
	}
	
	public Transform getTransform() {
		return transform;
	}
}
