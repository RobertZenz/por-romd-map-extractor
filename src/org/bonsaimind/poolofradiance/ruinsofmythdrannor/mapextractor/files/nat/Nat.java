/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat;

import java.util.HashMap;
import java.util.Map;

public class Nat {
	protected Bone[] bones = null;
	protected Map<String, Bone> bonesByName = null;
	protected Map<String, Transform> bonesGlobalTransformsByName = null;
	protected GeometryPhysique[] geometryPhysiques = null;
	protected GeometrySegment[] geometrySegments = null;
	protected String name = null;
	
	public Nat(
			String name,
			Bone[] bones,
			GeometrySegment[] geometrySegments,
			GeometryPhysique[] geometryPhysiques) {
		super();
		
		this.name = name;
		this.bones = bones;
		this.geometrySegments = geometrySegments;
		this.geometryPhysiques = geometryPhysiques;
		
		buildBoneMaps();
	}
	
	public Bone getBone(int boneIndex) {
		return bones[boneIndex];
	}
	
	public Transform getBoneGlobalTransform(String boneName) {
		return bonesGlobalTransformsByName.get(boneName);
	}
	
	public int getBonesCount() {
		return bones.length;
	}
	
	public GeometryPhysique getGeometryPhysique(int geometryPhysiqueIndex) {
		return geometryPhysiques[geometryPhysiqueIndex];
	}
	
	public int getGeometryPhysiquesCount() {
		if (geometryPhysiques == null) {
			return 0;
		}
		
		return geometryPhysiques.length;
	}
	
	public GeometrySegment getGeometrySegment(int geometrySegmentIndex) {
		return geometrySegments[geometrySegmentIndex];
	}
	
	public int getGeometrySegmentsCount() {
		if (geometrySegments == null) {
			return 0;
		}
		
		return geometrySegments.length;
	}
	
	public String getName() {
		return name;
	}
	
	protected void buildBoneMaps() {
		bonesByName = new HashMap<>();
		bonesGlobalTransformsByName = new HashMap<>();
		
		if (bones == null) {
			return;
		}
		
		for (Bone bone : bones) {
			buildBoneMaps(bone, new Transform(0.0d, 0.0d, 0.0d));
		}
	}
	
	protected void buildBoneMaps(Bone bone, Transform parentTransform) {
		bonesByName.put(bone.getName(), bone);
		
		Transform boneGlobalTransform = null;
		
		if (bone.getTransform() != null) {
			boneGlobalTransform = new Transform(
					parentTransform.getX() + bone.getTransform().getX(),
					parentTransform.getY() + bone.getTransform().getY(),
					parentTransform.getZ() + bone.getTransform().getZ());
		} else {
			boneGlobalTransform = parentTransform;
		}
		
		bonesGlobalTransformsByName.put(bone.getName(), boneGlobalTransform);
		
		for (int subBoneIndex = 0; subBoneIndex < bone.getBonesCount(); subBoneIndex++) {
			Bone subBone = bone.getBone(subBoneIndex);
			
			buildBoneMaps(subBone, boneGlobalTransform);
		}
	}
}
