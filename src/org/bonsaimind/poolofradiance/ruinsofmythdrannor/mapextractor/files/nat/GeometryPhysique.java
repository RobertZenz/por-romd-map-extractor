/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat;

public class GeometryPhysique {
	protected String name = null;
	protected PhysiqueVertice[] physiqueVertices = null;
	protected AbstractPolySegment[] polySegments = null;
	protected TextureCoordinate[] textureCoordinates = null;
	
	public GeometryPhysique(
			String name,
			PhysiqueVertice[] physiqueVertices,
			TextureCoordinate[] textureCoordinates,
			AbstractPolySegment[] polySegments) {
		super();
		
		this.name = name;
		this.physiqueVertices = physiqueVertices;
		this.textureCoordinates = textureCoordinates;
		this.polySegments = polySegments;
	}
	
	public String getName() {
		return name;
	}
	
	public PhysiqueVertice getPhysiqueVertice(int physiqueVerticeIndex) {
		return physiqueVertices[physiqueVerticeIndex];
	}
	
	public int getPhysiqueVerticesCount() {
		return physiqueVertices.length;
	}
	
	public AbstractPolySegment getPolySegment(int polySegmentIndex) {
		return polySegments[polySegmentIndex];
	}
	
	public int getPolySegmentsCount() {
		return polySegments.length;
	}
	
	public TextureCoordinate getTextureCoordinate(int textureCoordinateIndex) {
		return textureCoordinates[textureCoordinateIndex];
	}
	
	public int getTextureCoordinatesCount() {
		return textureCoordinates.length;
	}
}
