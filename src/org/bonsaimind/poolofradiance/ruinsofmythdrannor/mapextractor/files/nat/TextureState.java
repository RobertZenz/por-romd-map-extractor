/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat;

public enum TextureState {
	OFF("off"), ON("on");
	
	private String value = null;
	
	private TextureState(String value) {
		this.value = value;
	}
	
	public static final TextureState getByValue(String value) {
		for (TextureState textureState : values()) {
			if (textureState.getValue().equals(value)) {
				return textureState;
			}
		}
		
		return null;
	}
	
	public String getValue() {
		return value;
	}
}
