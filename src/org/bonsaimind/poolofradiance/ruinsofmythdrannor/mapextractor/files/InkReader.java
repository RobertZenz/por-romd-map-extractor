/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support.BinaryReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;

/**
 * The INK files contain images with a 68-byte header prepended. The image data
 * itself is 2-bytes-per-pixel.
 * <p>
 * The INK files starts with meta-information about the image followed by the
 * image data itself:
 * <p>
 * <ol>
 * <li>0x00: int32 - Unknown field, value always "2".</li>
 * <li>0x04: int32 - Unknown field, value either "2" or "3". Possibly value
 * indicating RGB layout type?</li>
 * <li>0x08: int32 - Unknown field, value always "16". Possibly number of bits
 * for each pixel (2 byte)?</li>
 * <li>0x0c: int32 - Unknown field, value always "0".</li>
 * <li>0x10: int32 - Unknown field, value either "0" or "61440". Possibly
 * bitmask of unused bits (1111.0000 0000.0000)?</li>
 * <li>0x14: int32 - Bitmask for the red color channel.</li>
 * <li>0x18: int32 - Bitmask for the green color channel.</li>
 * <li>0x1c: int32 - Bitmask for the blue color channel.</li>
 * <li>0x20: int32 - Unknown field, value always "16". Possibly number of bits
 * for each pixel (2 byte)?</li>
 * <li>0x24: int32 - Bit-shift value for red color channel. Is subtracted by 8
 * to get the actual bit shift, if the result is positive it is a right shift,
 * if the result is negative it is a left shift.</li>
 * <li>0x28: int32 - Bit-shift value for green color channel. Is subtracted by 8
 * to get the actual bit shift, if the result is positive it is a right shift,
 * if the result is negative it is a left shift.</li>
 * <li>0x2c: int32 - Bit-shift value for blue color channel. Is subtracted by 8
 * to get the actual bit shift, if the result is positive it is a right shift,
 * if the result is negative it is a left shift.</li>
 * <li>0x30: int32 - Unknown field, value always "0".</li>
 * <li>0x34: int32 - Width (in pixel) of the image.</li>
 * <li>0x38: int32 - Height (in pixel) of the image.</li>
 * <li>0x3c: int32 - Unknown field, value always "0".</li>
 * <li>0x40: int32 - Length of the image (in bytes).</li>
 * <li>0x44: Image</li>
 * <li>0x++: Padding with zeros.</li>
 * </ol>
 * <p>
 * The image data itself is padded with zeros at the end.
 * <p>
 * The image is a 2-byte-per-pixel image with two possible RGB color channel
 * layouts:
 * <p>
 * <ol>
 * <li>5/6/5</li>
 * <li>4/4/4</li>
 * </ol>
 * <p>
 * The exact bitmasks to retrieve each color channel are specified in the header
 * bytes at offsets 0x14, 0x18, and 0xc with that being red, green, and blue
 * respectively. The two bytes of each pixel are little endian, meaning the
 * second comes first. The color channels are shifted to fill the most
 * significant bits, that value of which to shift the color channels is stored
 * in the header fields 0x24, 0x28, and 0x2c for red, green, and blue
 * respecticely. The shift value is substracted by 8, if the result is positive
 * the shift is a right shift by the result, if it is negative it is a left
 * shift by the result.
 * <p>
 * This is the first known layout:
 * 
 * <pre>
    |second-| |-first-|
    1001.0110 1101.0101
    rrrr.rggg gggb.bbbb
    
    red = rrrr.r000 0000.0000 >> (16 - 8) = 0000.0000 rrrr.r000
    green = 0000.0ggg ggg0.0000 >> (11 - 8) = 0000.0000 gggg.gg00
    blue = 0000.0000 000b.bbbb << -(5 - 8) = 0000.0000 bbbb.b000
 * </pre>
 * 
 * This is the second known layout:
 * 
 * <pre>
    |second-| |-first-|
    1001.0110 1101.0101
    0000.rrrr gggg.bbbb
    
    red = 0000.rrrr 0000.0000 >> (12 - 8) = 0000.0000 rrrr.0000
    green = 0000.0000 gggg.0000 >> (8 - 8) = 0000.0000 gggg.0000
    blue = 0000.0000 000b.bbbb << -(4 - 8) = 0000.0000 bbbb.0000
 * </pre>
 * 
 * The image itself has its origin in the bottom left corner, so it must be
 * horizontally flipped.
 */
public class InkReader {
	private static final Logger LOGGER = new Logger(InkReader.class);
	
	public InkReader() {
		super();
	}
	
	public Ink read(Path inkFilePath) throws IOException {
		LOGGER.debug("Reading INK file from <%s>.",
				inkFilePath);
		
		return read(
				inkFilePath.getFileName().toString(),
				Files.newInputStream(inkFilePath));
	}
	
	public Ink read(String name, InputStream inkInputStream) throws IOException {
		LOGGER.debug("Reading INK <%s>.",
				name);
		
		try (BinaryReader reader = new BinaryReader(inkInputStream)) {
			int unknownHeaderFieldAtOffset0x00 = reader.readInt32();
			int unknownHeaderFieldAtOffset0x04 = reader.readInt32();
			int unknownHeaderFieldAtOffset0x08 = reader.readInt32();
			int unknownHeaderFieldAtOffset0x0c = reader.readInt32();
			int unknownHeaderFieldAtOffset0x10 = reader.readInt32();
			int bitMaskRed = reader.readInt32();
			int bitMaskGreen = reader.readInt32();
			int bitMaskBlue = reader.readInt32();
			int unknownHeaderFieldAtOffset0x20 = reader.readInt32();
			int bitShiftRed = reader.readInt32();
			int bitShiftGreen = reader.readInt32();
			int bitShiftBlue = reader.readInt32();
			int unknownHeaderFieldAtOffset0x30 = reader.readInt32();
			
			int width = reader.readInt32();
			int height = reader.readInt32();
			
			LOGGER.debug("Read INK <%s> size as <%dx%d> pixel.",
					name,
					Integer.valueOf(width),
					Integer.valueOf(height));
			
			int unknownHeaderFieldAtOffset0x3c = reader.readInt32();
			
			int length = reader.readInt32();
			
			LOGGER.debug("Read INK <%s> data length as <%d> bytes.",
					name,
					Integer.valueOf(length));
			
			byte[] imageData = reader.readBlock(length);
			BufferedImage image = convertToImage(
					width,
					height,
					imageData,
					bitMaskRed,
					bitMaskGreen,
					bitMaskBlue,
					bitShiftRed,
					bitShiftGreen,
					bitShiftBlue);
			byte[] padding = reader.readRemaining();
			
			return new Ink(
					name,
					unknownHeaderFieldAtOffset0x00,
					unknownHeaderFieldAtOffset0x04,
					unknownHeaderFieldAtOffset0x08,
					unknownHeaderFieldAtOffset0x0c,
					unknownHeaderFieldAtOffset0x10,
					bitMaskRed,
					bitMaskGreen,
					bitMaskBlue,
					unknownHeaderFieldAtOffset0x20,
					bitShiftRed,
					bitShiftGreen,
					bitShiftBlue,
					unknownHeaderFieldAtOffset0x30,
					width,
					height,
					unknownHeaderFieldAtOffset0x3c,
					length,
					image,
					padding);
		}
	}
	
	protected BufferedImage convertToImage(
			int width,
			int height,
			byte[] imageData,
			int bitMaskRed,
			int bitMaskGreen,
			int bitMaskBlue,
			int bitShiftRed,
			int bitShiftGreen,
			int bitShiftBlue) {
		int[] argb = new int[width * height];
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int dataIndex = (y * width + x) * 2;
				// INKs are upside down, so we swap the y coordinate.
				int pixelIndex = (height - y - 1) * width + x;
				
				int value = (imageData[dataIndex + 1] & 0xff) << 8
						| (imageData[dataIndex + 0] & 0xff) << 0;
				
				int red = shift(value & bitMaskRed, bitShiftRed);
				int green = shift(value & bitMaskGreen, bitShiftGreen);
				int blue = shift(value & bitMaskBlue, bitShiftBlue);
				
				argb[pixelIndex] = 0
						| (0xff << 24)
						| (red << 16)
						| (green << 8)
						| (blue << 0);
			}
		}
		
		BufferedImage bufferedImage = new BufferedImage(
				width,
				height,
				BufferedImage.TYPE_INT_RGB);
		bufferedImage.setRGB(
				0,
				0,
				bufferedImage.getWidth(),
				bufferedImage.getHeight(),
				argb,
				0,
				bufferedImage.getWidth());
		
		return bufferedImage;
	}
	
	protected int shift(int value, int shift) {
		int actualShift = shift - 8;
		
		if (actualShift > 0) {
			return value >>> actualShift;
		} else {
			return value << -actualShift;
		}
	}
}
