/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink;

import java.awt.image.BufferedImage;

/**
 * The INK files contain images with a 68-byte header prepended. The image data
 * itself is 2-bytes-per-pixel.
 * <p>
 * The INK files starts with meta-information about the image followed by the
 * image data itself:
 * <p>
 * <ol>
 * <li>0x00: int32 - Unknown field, value always "2".</li>
 * <li>0x04: int32 - Unknown field, value either "2" or "3". Possibly value
 * indicating RGB layout type?</li>
 * <li>0x08: int32 - Unknown field, value always "16". Possibly number of bits
 * for each pixel (2 byte)?</li>
 * <li>0x0c: int32 - Unknown field, value always "0".</li>
 * <li>0x10: int32 - Unknown field, value either "0" or "61440". Possibly
 * bitmask of unused bits (1111.0000 0000.0000)?</li>
 * <li>0x14: int32 - Bitmask for the red color channel.</li>
 * <li>0x18: int32 - Bitmask for the green color channel.</li>
 * <li>0x1c: int32 - Bitmask for the blue color channel.</li>
 * <li>0x20: int32 - Unknown field, value always "16". Possibly number of bits
 * for each pixel (2 byte)?</li>
 * <li>0x24: int32 - Bit-shift value for red color channel. Is subtracted by 8
 * to get the actual bit shift, if the result is positive it is a right shift,
 * if the result is negative it is a left shift.</li>
 * <li>0x28: int32 - Bit-shift value for green color channel. Is subtracted by 8
 * to get the actual bit shift, if the result is positive it is a right shift,
 * if the result is negative it is a left shift.</li>
 * <li>0x2c: int32 - Bit-shift value for blue color channel. Is subtracted by 8
 * to get the actual bit shift, if the result is positive it is a right shift,
 * if the result is negative it is a left shift.</li>
 * <li>0x30: int32 - Unknown field, value always "0".</li>
 * <li>0x34: int32 - Width (in pixel) of the image.</li>
 * <li>0x38: int32 - Height (in pixel) of the image.</li>
 * <li>0x3c: int32 - Unknown field, value always "0".</li>
 * <li>0x40: int32 - Length of the image (in bytes).</li>
 * <li>0x44: Image</li>
 * <li>0x++: Padding to the next 512 byte block.</li>
 * </ol>
 * <p>
 * The image data itself is padded with zeros at the end to the next 512 byte
 * block.
 */
public class Ink {
	protected int bitMaskBlue = -1;
	protected int bitMaskGreen = -1;
	protected int bitMaskRed = -1;
	protected int bitShiftBlue = -1;
	protected int bitShiftGreen = -1;
	protected int bitShiftRed = -1;
	protected int height = -1;
	protected BufferedImage image = null;
	protected int length = -1;
	protected String name = null;
	protected byte[] padding = null;
	protected int unknownHeaderFieldAtOffset0x00 = -1;
	protected int unknownHeaderFieldAtOffset0x04 = -1;
	protected int unknownHeaderFieldAtOffset0x08 = -1;
	protected int unknownHeaderFieldAtOffset0x0c = -1;
	protected int unknownHeaderFieldAtOffset0x10 = -1;
	protected int unknownHeaderFieldAtOffset0x20 = -1;
	protected int unknownHeaderFieldAtOffset0x30 = -1;
	protected int unknownHeaderFieldAtOffset0x3c = -1;
	protected int width = -1;
	
	public Ink(
			String name,
			int unknownHeaderFieldAtOffset0x00,
			int unknownHeaderFieldAtOffset0x04,
			int unknownHeaderFieldAtOffset0x08,
			int unknownHeaderFieldAtOffset0x0c,
			int unknownHeaderFieldAtOffset0x10,
			int bitMaskRed,
			int bitMaskGreen,
			int bitMaskBlue,
			int unknownHeaderFieldAtOffset0x20,
			int bitShiftRed,
			int bitShiftGreen,
			int bitShiftBlue,
			int unknownHeaderFieldAtOffset0x30,
			int width,
			int height,
			int unknownHeaderFieldAtOffset0x3c,
			int length,
			BufferedImage image,
			byte[] padding) {
		super();
		
		this.name = name;
		this.unknownHeaderFieldAtOffset0x00 = unknownHeaderFieldAtOffset0x00;
		this.unknownHeaderFieldAtOffset0x04 = unknownHeaderFieldAtOffset0x04;
		this.unknownHeaderFieldAtOffset0x08 = unknownHeaderFieldAtOffset0x08;
		this.unknownHeaderFieldAtOffset0x0c = unknownHeaderFieldAtOffset0x0c;
		this.unknownHeaderFieldAtOffset0x10 = unknownHeaderFieldAtOffset0x10;
		this.bitMaskRed = bitMaskRed;
		this.bitMaskGreen = bitMaskGreen;
		this.bitMaskBlue = bitMaskBlue;
		this.unknownHeaderFieldAtOffset0x20 = unknownHeaderFieldAtOffset0x20;
		this.bitShiftRed = bitShiftRed;
		this.bitShiftGreen = bitShiftGreen;
		this.bitShiftBlue = bitShiftBlue;
		this.unknownHeaderFieldAtOffset0x30 = unknownHeaderFieldAtOffset0x30;
		this.width = width;
		this.height = height;
		this.unknownHeaderFieldAtOffset0x3c = unknownHeaderFieldAtOffset0x3c;
		this.length = length;
		this.image = image;
		this.padding = padding;
	}
	
	public int getBitMaskBlue() {
		return bitMaskBlue;
	}
	
	public int getBitMaskGreen() {
		return bitMaskGreen;
	}
	
	public int getBitMaskRed() {
		return bitMaskRed;
	}
	
	public int getBitShiftBlue() {
		return bitShiftBlue;
	}
	
	public int getBitShiftGreen() {
		return bitShiftGreen;
	}
	
	public int getBitShiftRed() {
		return bitShiftRed;
	}
	
	public int getHeight() {
		return height;
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public int getLength() {
		return length;
	}
	
	public String getName() {
		return name;
	}
	
	public byte[] getPadding() {
		return padding;
	}
	
	public int getUnknownHeaderFieldAtOffset0x00() {
		return unknownHeaderFieldAtOffset0x00;
	}
	
	public int getUnknownHeaderFieldAtOffset0x04() {
		return unknownHeaderFieldAtOffset0x04;
	}
	
	public int getUnknownHeaderFieldAtOffset0x08() {
		return unknownHeaderFieldAtOffset0x08;
	}
	
	public int getUnknownHeaderFieldAtOffset0x0c() {
		return unknownHeaderFieldAtOffset0x0c;
	}
	
	public int getUnknownHeaderFieldAtOffset0x10() {
		return unknownHeaderFieldAtOffset0x10;
	}
	
	public int getUnknownHeaderFieldAtOffset0x20() {
		return unknownHeaderFieldAtOffset0x20;
	}
	
	public int getUnknownHeaderFieldAtOffset0x30() {
		return unknownHeaderFieldAtOffset0x30;
	}
	
	public int getUnknownHeaderFieldAtOffset0x3c() {
		return unknownHeaderFieldAtOffset0x3c;
	}
	
	public int getWidth() {
		return width;
	}
}
