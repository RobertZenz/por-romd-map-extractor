/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.PoolOfRadianceRuinsOfMythDrannor;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.AbstractPolySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Bone;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Face;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometryPhysique;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.GeometrySegment;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Nat;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.PhysiqueVertice;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.PolyList;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.PolyStrip;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.TextureCoordinate;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.TextureState;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Transform;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.nat.Vertice;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;

public class NatReader {
	protected static final String BLOCK_IDENTIFIER_GEOMETRY_SEGMENT = "geometry_seg";
	protected static final String BLOCK_IDENTIFIER_HIERARCHY = "hierarchy";
	protected static final String LINE_IDENTIFIER_GEOMETRY_SEGMENT = "seg";
	protected static final String LINE_IDENTIFIER_GEOMETRY_SEGMENT_GROUP = "geometry_seg";
	protected static final String LINE_IDENTIFIER_TEXTURE_COORDINATE_OR_TRANSFORM = "t";
	protected static final String LINE_IDENTIFIER_VERTICE = "v";
	
	private static final Logger LOGGER = new Logger(NatReader.class);
	
	public NatReader() {
		super();
	}
	
	public Nat read(Path natFilePath) throws IOException {
		LOGGER.debug("Reading NAT file from <%s>.",
				natFilePath);
		
		return read(
				natFilePath.getFileName().toString(),
				Files.newInputStream(natFilePath));
	}
	
	public Nat read(String name, InputStream natInputStream) throws IOException {
		LOGGER.debug("Reading NAT <%s>.",
				name);
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(natInputStream, PoolOfRadianceRuinsOfMythDrannor.CHARSET))) {
			List<HierarchicalLine> hierarchicalLines = parseIntoHierarchicalLines(name, reader);
			
			Bone[] bones = null;
			GeometrySegment[] geometrySegments = null;
			GeometryPhysique[] geometryPhysiques = null;
			
			for (HierarchicalLine hierarchicalLine : hierarchicalLines) {
				if (hierarchicalLine.getField(0).equals("geometry_physique")) {
					geometryPhysiques = processIntoGeometryPhysiques(name, hierarchicalLine);
				} else if (hierarchicalLine.getField(0).equals("geometry_seg")) {
					geometrySegments = processIntoGeometrySegments(name, hierarchicalLine);
				} else if (hierarchicalLine.getField(0).equals("hierarchy")) {
					bones = processIntoBones(name, hierarchicalLine);
				} else {
					LOGGER.info("Processing NAT <%s> encountered unknown line <%d><%s>.",
							name,
							Integer.valueOf(hierarchicalLine.getLineNumber()),
							hierarchicalLine.getLine());
				}
			}
			
			return new Nat(
					name,
					bones,
					geometrySegments,
					geometryPhysiques);
		}
	}
	
	protected List<HierarchicalLine> parseIntoHierarchicalLines(String name, BufferedReader reader) throws IOException {
		String currentLine = reader.readLine();
		String nextLine = reader.readLine();
		
		List<HierarchicalLine> hierarchicalLines = new LinkedList<>();
		List<HierarchicalLine> hierarchicalLinesStack = new LinkedList<>();
		
		HierarchicalLine currentHierarchicalLine = null;
		
		int currentLineNumber = 1;
		
		while ((currentLine = nextLine) != null &&
				(nextLine = reader.readLine()) != null) {
			String trimmedCurrentLine = currentLine.trim();
			String trimmedNextLine = nextLine.trim();
			
			if (trimmedNextLine.equals("{")
					|| trimmedNextLine.equals("[")) {
				if (currentHierarchicalLine != null) {
					hierarchicalLinesStack.add(currentHierarchicalLine);
				}
				
				currentHierarchicalLine = new HierarchicalLine(currentLineNumber, trimmedCurrentLine);
				
				currentLine = nextLine;
				nextLine = reader.readLine();
			} else if (trimmedCurrentLine.equals("}")
					|| trimmedCurrentLine.equals("]")) {
				if (!hierarchicalLinesStack.isEmpty()) {
					HierarchicalLine parentHierachicalLine = hierarchicalLinesStack.remove(hierarchicalLinesStack.size() - 1);
					parentHierachicalLine.getSubLines().add(currentHierarchicalLine);
					
					currentHierarchicalLine = parentHierachicalLine;
				} else {
					hierarchicalLines.add(currentHierarchicalLine);
					
					currentHierarchicalLine = null;
				}
			} else if (!trimmedCurrentLine.isEmpty()
					&& !trimmedCurrentLine.startsWith("#")
					&& !trimmedCurrentLine.equals("NAT")
					&& !trimmedCurrentLine.startsWith("version ")) {
				currentHierarchicalLine.getSubLines().add(new HierarchicalLine(currentLineNumber, trimmedCurrentLine));
			}
			
			currentLineNumber++;
		}
		
		if (!hierarchicalLinesStack.isEmpty()) {
			LOGGER.warning("Parsing NAT <%s> reached end of file but hierarchical-lines-stack still has <%d> lines remaining.",
					name,
					Integer.valueOf(hierarchicalLinesStack.size()));
		}
		
		if (currentHierarchicalLine != null) {
			hierarchicalLines.add(currentHierarchicalLine);
		}
		
		return hierarchicalLines;
	}
	
	protected Bone processIntoBone(String natName, HierarchicalLine hierarchicalLine) {
		String name = hierarchicalLine.getField(1);
		Transform transform = null;
		List<Bone> bones = new LinkedList<>();
		
		for (HierarchicalLine subLine : hierarchicalLine.getSubLines()) {
			if (subLine.getField(0).equals("bone")) {
				bones.add(processIntoBone(natName, subLine));
			} else if (subLine.getField(0).equals("t")) {
				transform = new Transform(
						Double.parseDouble(subLine.getField(1)),
						Double.parseDouble(subLine.getField(3)),
						Double.parseDouble(subLine.getField(2)));
			} else {
				LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
						natName,
						Integer.valueOf(subLine.getLineNumber()),
						subLine.getLine(),
						Integer.valueOf(hierarchicalLine.getLineNumber()),
						hierarchicalLine.getLine());
			}
		}
		
		return new Bone(
				name,
				transform,
				bones.toArray(new Bone[bones.size()]));
	}
	
	protected Bone[] processIntoBones(String natName, HierarchicalLine hierarchicalLine) {
		List<Bone> bones = new LinkedList<>();
		
		for (HierarchicalLine subLine : hierarchicalLine.getSubLines()) {
			if (subLine.getField(0).equals("bone")) {
				bones.add(processIntoBone(natName, subLine));
			}
		}
		
		return bones.toArray(new Bone[bones.size()]);
	}
	
	protected GeometryPhysique processIntoGeometryPhysique(String natName, HierarchicalLine hierarchicalLine) {
		String name = hierarchicalLine.getField(0);
		PhysiqueVertice[] physiqueVertices = null;
		TextureCoordinate[] textureCoordinates = null;
		List<AbstractPolySegment> polySegments = new LinkedList<>();
		
		String currentTexture = null;
		TextureState currentTextureState = null;
		
		for (HierarchicalLine line : hierarchicalLine.getSubLines()) {
			if (line.getField(0).equals("polyList")) {
				int faceCount = Integer.parseInt(line.getField(1));
				Face[] faces = new Face[faceCount];
				int faceIndex = 0;
				
				for (HierarchicalLine faceLine : line.getSubLines()) {
					if (faceLine.getField(0).equals("f")) {
						faces[faceIndex++] = new Face(
								Integer.parseInt(faceLine.getField(1)),
								Integer.parseInt(faceLine.getField(2)));
					} else {
						LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
								natName,
								Integer.valueOf(faceLine.getLineNumber()),
								faceLine.getLine(),
								Integer.valueOf(line.getLineNumber()),
								line.getLine());
					}
				}
				
				polySegments.add(new PolyList(
						currentTexture,
						currentTextureState,
						faces));
			} else if (line.getField(0).equals("polyStrip")) {
				int faceCount = Integer.parseInt(line.getField(1));
				Face[] faces = new Face[faceCount];
				int faceIndex = 0;
				
				for (HierarchicalLine faceLine : line.getSubLines()) {
					if (faceLine.getField(0).equals("f")) {
						faces[faceIndex++] = new Face(
								Integer.parseInt(faceLine.getField(1)),
								Integer.parseInt(faceLine.getField(2)));
					} else {
						LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
								natName,
								Integer.valueOf(faceLine.getLineNumber()),
								faceLine.getLine(),
								Integer.valueOf(line.getLineNumber()),
								line.getLine());
					}
				}
				
				polySegments.add(new PolyStrip(
						currentTexture,
						currentTextureState,
						faces));
			} else if (line.getField(0).equals("textList")) {
				int textureCoordinatesCount = Integer.parseInt(line.getField(1));
				textureCoordinates = new TextureCoordinate[textureCoordinatesCount];
				int textureCoordinateIndex = 0;
				
				for (HierarchicalLine textureCoordinateLine : line.getSubLines()) {
					if (textureCoordinateLine.getField(0).equals("t")) {
						textureCoordinates[textureCoordinateIndex++] = new TextureCoordinate(
								Double.parseDouble(textureCoordinateLine.getField(1)),
								Double.parseDouble(textureCoordinateLine.getField(2)));
					} else {
						LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
								natName,
								Integer.valueOf(textureCoordinateLine.getLineNumber()),
								textureCoordinateLine.getLine(),
								Integer.valueOf(line.getLineNumber()),
								line.getLine());
					}
				}
			} else if (line.getField(0).equals("texture")) {
				currentTexture = line.getField(1);
			} else if (line.getField(0).equals("textureState")) {
				currentTextureState = TextureState.getByValue(line.getField(1));
			} else if (line.getField(0).equals("vertList")) {
				int pyhsiqueVerticesCount = Integer.parseInt(line.getField(1));
				physiqueVertices = new PhysiqueVertice[pyhsiqueVerticesCount];
				int physiqueVerticeIndex = 0;
				
				for (HierarchicalLine vertListSubLine : line.getSubLines()) {
					if (vertListSubLine.getField(0).equals("v_phys")) {
						for (HierarchicalLine vPhysSubLine : vertListSubLine.getSubLines()) {
							if (vPhysSubLine.getField(0).equals("xyz")) {
								physiqueVertices[physiqueVerticeIndex++] = new PhysiqueVertice(
										vPhysSubLine.getField(1),
										Double.parseDouble(vPhysSubLine.getField(2)),
										Double.parseDouble(vPhysSubLine.getField(3)),
										Double.parseDouble(vPhysSubLine.getField(5)),
										Double.parseDouble(vPhysSubLine.getField(4)));
							} else {
								LOGGER.info("Processing NAT <%s> encountered unknown line <%d><%s> in <%d><%s>.",
										natName,
										Integer.valueOf(vPhysSubLine.getLineNumber()),
										vPhysSubLine.getLine(),
										Integer.valueOf(vertListSubLine.getLineNumber()),
										vertListSubLine.getLine());
							}
						}
					} else {
						LOGGER.info("Processing NAT <%s> encountered unknown line <%d><%s> in <%d><%s>.",
								natName,
								Integer.valueOf(vertListSubLine.getLineNumber()),
								vertListSubLine.getLine(),
								Integer.valueOf(line.getLineNumber()),
								line.getLine());
					}
				}
			} else {
				LOGGER.info("Processing NAT <%s> encountered unknown line <%d><%s> in <%d><%s>.",
						natName,
						Integer.valueOf(line.getLineNumber()),
						line.getLine(),
						Integer.valueOf(hierarchicalLine.getLineNumber()),
						hierarchicalLine.getLine());
			}
		}
		
		return new GeometryPhysique(
				name,
				physiqueVertices,
				textureCoordinates,
				polySegments.toArray(new AbstractPolySegment[polySegments.size()]));
	}
	
	protected GeometryPhysique[] processIntoGeometryPhysiques(String natName, HierarchicalLine hierarchicalLine) {
		List<GeometryPhysique> geometryPhysiques = new LinkedList<>();
		
		for (HierarchicalLine line : hierarchicalLine.getSubLines()) {
			if (line.getField(0).equals("physique")) {
				geometryPhysiques.add(processIntoGeometryPhysique(natName, line));
			} else {
				LOGGER.info("Processing NAT <%s> encountered unknown line <%d><%s> in <%d><%s>.",
						natName,
						Integer.valueOf(line.getLineNumber()),
						line.getLine(),
						Integer.valueOf(hierarchicalLine.getLineNumber()),
						hierarchicalLine.getLine());
			}
		}
		
		return geometryPhysiques.toArray(new GeometryPhysique[geometryPhysiques.size()]);
	}
	
	protected GeometrySegment processIntoGeometrySegment(String natName, HierarchicalLine hierarchicalLine) {
		String name = hierarchicalLine.getField(1);
		Vertice[] vertices = null;
		TextureCoordinate[] textureCoordinates = null;
		String texture = null;
		TextureState textureState = null;
		Face[] faces = null;
		
		for (HierarchicalLine line : hierarchicalLine.getSubLines()) {
			if (line.getField(0).equals("polyList")) {
				int facesCount = Integer.parseInt(line.getField(1));
				faces = new Face[facesCount];
				int faceIndex = 0;
				
				for (HierarchicalLine faceLine : line.getSubLines()) {
					if (faceLine.getField(0).equals("f")) {
						faces[faceIndex++] = new Face(
								Integer.parseInt(faceLine.getField(1)),
								Integer.parseInt(faceLine.getField(2)));
					} else {
						LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
								natName,
								Integer.valueOf(faceLine.getLineNumber()),
								faceLine.getLine(),
								Integer.valueOf(line.getLineNumber()),
								line.getLine());
					}
				}
			} else if (line.getField(0).equals("textList")) {
				int textureCoordinatesCount = Integer.parseInt(line.getField(1));
				textureCoordinates = new TextureCoordinate[textureCoordinatesCount];
				int textureCoordinateIndex = 0;
				
				for (HierarchicalLine textureCoordinateLine : line.getSubLines()) {
					if (textureCoordinateLine.getField(0).equals("t")) {
						textureCoordinates[textureCoordinateIndex++] = new TextureCoordinate(
								Double.parseDouble(textureCoordinateLine.getField(1)),
								Double.parseDouble(textureCoordinateLine.getField(2)));
					} else {
						LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
								natName,
								Integer.valueOf(textureCoordinateLine.getLineNumber()),
								textureCoordinateLine.getLine(),
								Integer.valueOf(line.getLineNumber()),
								line.getLine());
					}
				}
			} else if (line.getField(0).equals("texture")) {
				texture = line.getField(1);
			} else if (line.getField(0).equals("textureState")) {
				textureState = TextureState.getByValue(line.getField(1));
			} else if (line.getField(0).equals("vertList")) {
				int verticesCount = Integer.parseInt(line.getField(1));
				vertices = new Vertice[verticesCount];
				int verticeIndex = 0;
				
				for (HierarchicalLine verticeLine : line.getSubLines()) {
					if (verticeLine.getField(0).equals("v")) {
						vertices[verticeIndex++] = new Vertice(
								Double.parseDouble(verticeLine.getField(1)),
								Double.parseDouble(verticeLine.getField(3)),
								Double.parseDouble(verticeLine.getField(2)));
					} else {
						LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
								natName,
								Integer.valueOf(verticeLine.getLineNumber()),
								verticeLine.getLine(),
								Integer.valueOf(line.getLineNumber()),
								line.getLine());
					}
				}
			} else {
				LOGGER.info("Processing NAT <%s> encountered unhandled line <%d><%s> in block <%d><%s>.",
						natName,
						Integer.valueOf(line.getLineNumber()),
						line.getLine(),
						Integer.valueOf(hierarchicalLine.getLineNumber()),
						hierarchicalLine.getLine());
			}
		}
		
		return new GeometrySegment(
				name,
				vertices,
				textureCoordinates,
				texture,
				textureState,
				faces);
	}
	
	protected GeometrySegment[] processIntoGeometrySegments(String natName, HierarchicalLine hierarchicalLine) {
		List<GeometrySegment> geometrySegments = new LinkedList<>();
		
		for (HierarchicalLine line : hierarchicalLine.getSubLines()) {
			if (line.getField(0).equals("seg")) {
				geometrySegments.add(processIntoGeometrySegment(natName, line));
			} else {
				LOGGER.info("Processing NAT <%s> encountered unknown line <%d><%s> in <%d><%s>.",
						natName,
						Integer.valueOf(line.getLineNumber()),
						line.getLine(),
						Integer.valueOf(hierarchicalLine.getLineNumber()),
						hierarchicalLine.getLine());
			}
		}
		
		return geometrySegments.toArray(new GeometrySegment[geometrySegments.size()]);
	}
	
	protected class HierarchicalLine {
		protected String[] fields = null;
		protected String line = null;
		protected int lineNumber = -1;
		protected List<HierarchicalLine> subLines = new LinkedList<>();
		
		public HierarchicalLine(
				int lineNumber,
				String line) {
			super();
			
			this.lineNumber = lineNumber;
			this.line = line;
			
			fields = line.split(" ");
		}
		
		public String getField(int fieldIndex) {
			return fields[fieldIndex];
		}
		
		public String getLine() {
			return line;
		}
		
		public int getLineNumber() {
			return lineNumber;
		}
		
		public List<HierarchicalLine> getSubLines() {
			return subLines;
		}
	}
}
