/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.cnk.Cnk;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.ink.Ink;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.files.support.BinaryReader;
import org.bonsaimind.poolofradiance.ruinsofmythdrannor.mapextractor.support.logging.Logger;

/**
 * The CNK files are archives of INK files (or any files).
 * <p>
 * The CNK file starts with meta-information about the file and a list of entry
 * names, followed by the entries themselves.
 * <p>
 * <ol>
 * <li>0x00: int32 - Number of entries in this file.</li>
 * <li>0x04: int32 - The total length of the file.</li>
 * <li>0x08: Entries index</li>
 * <li>0x++: Entries</li>
 * </ol>
 * <p>
 * The entries index consists of "number of entries" index structures (offsets
 * are relative).
 * <p>
 * <ol>
 * <li>0x00: 52 byte string - Name of the entry, null terminated, remaining
 * space after the null is filled with "=".</li>
 * <li>0x34: int32 - Offset of the entry in this file.</li>
 * </ol>
 * <p>
 * The remaining file consists of the INK entries without additional
 * information.
 */
public class CnkReader {
	protected static final int MAX_ENTRY_NAME_LENGTH = 52;
	private static final Logger LOGGER = new Logger(CnkReader.class);
	
	protected InkReader inkReader = null;
	
	public CnkReader(
			InkReader inkReader) {
		super();
		
		this.inkReader = inkReader;
	}
	
	public Cnk read(Path cnkFilePath) throws IOException {
		LOGGER.debug("Reading CNK file from <%s>.",
				cnkFilePath);
		
		return read(
				cnkFilePath.getFileName().toString(),
				Files.newInputStream(cnkFilePath));
	}
	
	public Cnk read(String name, InputStream cnkInputStream) throws IOException {
		LOGGER.debug("Reading CNK <%s>.",
				name);
		
		try (BinaryReader reader = new BinaryReader(cnkInputStream)) {
			int entriesCount = reader.readInt32();
			int fileLength = reader.readInt32();
			
			LOGGER.debug("CNK file <%s> has <%d> entries and a total length of <%d>.",
					name,
					Integer.valueOf(entriesCount),
					Integer.valueOf(fileLength));
			
			String[] entryNames = new String[entriesCount];
			int[] entryOffsets = new int[entriesCount];
			
			for (int entryIndex = 0; entryIndex < entriesCount; entryIndex++) {
				String entryName = reader.readStringNullTerminated(MAX_ENTRY_NAME_LENGTH);
				int entryOffset = reader.readInt32();
				
				LOGGER.debug("CNK <%s> entry <%s> is at offset <0x%08x>.",
						name,
						entryName,
						Integer.valueOf(entryOffset));
				
				entryNames[entryIndex] = entryName;
				entryOffsets[entryIndex] = entryOffset;
			}
			
			Ink[] entriesInks = new Ink[entriesCount];
			
			for (int entryIndex = 0; entryIndex < entriesCount; entryIndex++) {
				String entryName = entryNames[entryIndex];
				int entryOffset = entryOffsets[entryIndex];
				int entryLength = 0;
				
				if (entryIndex < (entriesCount - 1)) {
					int nextEntryOffset = entryOffsets[entryIndex + 1];
					entryLength = nextEntryOffset - entryOffset;
				} else {
					entryLength = fileLength - entryOffset;
				}
				
				LOGGER.debug("Reading CNK <%s> entry <%s> with length <%d>.",
						name,
						entryName,
						Integer.valueOf(entryLength));
				
				byte[] entryData = reader.readBlock(entryLength);
				Ink ink = inkReader.read(entryName, new ByteArrayInputStream(entryData));
				
				entriesInks[entryIndex] = ink;
			}
			
			return new Cnk(
					name,
					entriesCount,
					fileLength,
					entryNames,
					entryOffsets,
					entriesInks);
		}
	}
}
