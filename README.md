Pool of Radiance: Ruins of Myth Drannor Map Extractor
=====================================================

This program allows you to view or extract many PoR:RoMD files and their
contents into easier usable file formats.

The program has two possible modes of operations:

 1. Viewer (Default): A GUI which allows you to inspect and export files.
 2. Exporter: Directly exporting given files.

The target directory is either set through `--target-directory` parameter or
is assumed to be the current directory. The files are created with their
respective names in the target directory, in the case of archives a directory
with an appropriate name will be created.


Available options:

  --data-directory=PATH          The path to the PoR:RoMD "data" directory.
  --debug                         Enable debug logging. Shorthand for
                                    --log-classname-tag-level=SIMPLE_CLASSNAME
                                    --log-level-tag-level=FULL
                                    --log-level=DEBUG
  --errors                        Enable error only logging. Shorthand for
                                    --log-classname-tag-level=NONE
                                    --log-level-tag-level=NONE
                                    --log-level=ERROR
  --export                        Export given files directly to the target or
                                  current directory.
  --help                          Print this help text and exit.
  --image-type=JPEG               The type of the image to write. Can be JPEG or
                                  PNG (or any other type supported by the JVM).
  --log-classname-tag-level=NONE  The level with which to include the classname
                                  of the class which prints the message in
                                  the output. Can be either FULL_CLASSNAME,
                                  NONE or SIMPLE_CLASSNAME.
  --log-level-tag-level=NONE      The level with which to include the lgo level
                                  of the message in the output. Can be either
                                  FULL or NONE.
  --log-level=INFO                The log level from which to print messages.
                                  Can either be DEBUG, ERROR, INFO, NONE or
                                  WARNING.
  --quiet                         Disable any logging. Shorthand for
                                    --log-classname-tag-level=NONE
                                    --log-level-tag-level=NONE
                                    --log-level=NONE
  --target-directory=PATH         Write the image files to this directory
                                  instead of the current one.
  --verbose                       Enable verbose logging. Shorthand for
                                    --log-classname-tag-level=NONE
                                    --log-level-tag-level=NONE
                                    --log-level=DEBUG
  --viewer                        Open the GUI, this is the default.


Supported files are:

 * CNK: Texture archive
 * INK: Texture
 * MCZ: Map tile
 * NAT: Model ("static" models only)
 * WMD: Map


Usage examples:

To export a whole map from a WMD file you must pass the path to the WMD file,
and if the WMD file does not reside in the PoR:RoMD data directory also
the appropriate data directory where to find the MCZ files.

  java -jar por-romd-map-extractor.jar --export "/path/to/por-romd/data/WMD/drannor.wmd"

Please note that some of the maps, like "drannor" are rather large when
processed to an image (23000x11000 pixels) and require as much memory.

To write single MCZ files you can simply pass the MCZ file you want converted to
the program:

  java -jar por-romd-map-extractor.jar --export "/path/to/por-romd/data/MCZ/twr101.mcz"

If you want to convert all WMD files, you can simply pass them all to
the program:

  java -jar por-romd-map-extractor.jar --export "/path/to/por-romd/data/WMD/"*".wmd"

The same goes for all other files, multiple (different) files can always be
specified to be exported.


Technical notes
---------------

### Common

Unless noted otherwise, little endian must be assumed.

### Maps

The PoR:RoMD maps are defined in the WMD files, which contain an index of all tiles for the map. These tiles are contained in MCZ files, with each being split into single tiles again. That was most likely done to be able to load small amounts of the map from the disk to memory without having to load the whole map at all.

### Glossar

 * "Map": A "Map" is the complete map as it appears in the game which maps directly to a WMD file (or a single MCZ file if converting only a single MCZ file).
 * "MapTile": A "MapTile" maps directly to a single MCZ file and is the combination of all tiles in that file in a single image.
 * "SingleTile": A "SingleTile" is a single tile contained inside a MCZ file.

### CNB files

The CNB files are archives of NAB files (or any files).

The CNB file starts with meta-information about the file and a list of entry names, followed by the entries themselves.

 1. 0x00: int32 - Number of entries in this file.
 2. 0x04: int32 - The total length of the file.
 3. 0x08: Entries index
 4. 0x++: Entries

The entries index consists of "number of entries" entry index structures (offsets are relative).

 1. 0x00: 52 byte string - Name of the entry, null terminated, remaining space after the null is filled with "=".
 2. 0x34: int32 - Offset of the entry in this file.

The remaining file consists of the NAB entries without additional information.

### CNK files

The CNK files are archives of INK files (or any files).

The CNK file starts with meta-information about the file and a list of entry names, followed by the entries themselves.

 1. 0x00: int32 - Number of entries in this file.
 2. 0x04: int32 - The total length of the file.
 3. 0x08: Entries index
 4. 0x++: Entries

The entries index consists of "number of entries" entry index structures (offsets are relative).

 1. 0x00: 52 byte string - Name of the entry, null terminated, remaining space after the null is filled with "=".
 2. 0x34: int32 - Offset of the entry in this file.

The remaining file consists of the INK entries without additional information.

### INK files

The INK files contain images with a 68-byte header prepended. The image data itself is 2-bytes-per-pixel.

The INK files starts with meta-information about the image followed by the image data itself:

 1. 0x00: int32 - Unknown field, value always "2".
 2. 0x04: int32 - Unknown field, value either "2" or "3". Possibly value indicating RGB layout type?
 3. 0x08: int32 - Unknown field, value always "16". Possibly number of bits for each pixel (2 byte)?
 4. 0x0c: int32 - Unknown field, value always "0".
 5. 0x10: int32 - Unknown field, value either "0" or "61440". Possibly bitmask of unused bits (1111.0000 0000.0000)?
 6. 0x14: int32 - Bitmask for the red color channel.
 7. 0x18: int32 - Bitmask for the green color channel.
 8. 0x1c: int32 - Bitmask for the blue color channel.
 9. 0x20: int32 - Unknown field, value always "16". Possibly number of bits for each pixel (2 byte)?
 10. 0x24: int32 - Bit-shift value for red color channel. Is subtracted by 8 to get the actual bit shift, if the result is positive it is a right shift, if the result is negative it is a left shift.
 11. 0x28: int32 - Bit-shift value for green color channel. Is subtracted by 8 to get the actual bit shift, if the result is positive it is a right shift, if the result is negative it is a left shift.
 12. 0x2c: int32 - Bit-shift value for green color channel. Is subtracted by 8 to get the actual bit shift, if the result is positive it is a right shift, if the result is negative it is a left shift.
 13. 0x30: int32 - Unknown field, value always "0".
 14. 0x34: int32 - Width (in pixel) of the image.
 15. 0x38: int32 - Height (in pixel) of the image.
 16. 0x3c: int32 - Unknown field, value always "0".
 17. 0x40: int32 - Length of the image (in bytes).
 18. 0x44: Image
 19. 0x++: Padding with zeros.

The image data itself is padded with zeros at the end.

The image is a 2-byte-per-pixel image with two possible RGB color channel layouts:

 1. 5/6/5
 2. 4/4/4

The exact bitmasks to retrieve each color channel are specified in the header bytes at offsets 0x14, 0x18, and 0xc with that being red, green, and blue respectively. The two bytes of each pixel are little endian, meaning the second comes first. The color channels are shifted to fill the most significant bits, that value of which to shift the color channels is stored in the header fields 0x24, 0x28, and 0x2c for red, green, and blue respecticely. The shift value is substracted by 8, if the result is positive the shift is a right shift by the result, if it is negative it is a left shift by the result.

This is the first known layout:

    |second-| |-first-|
    1001.0110 1101.0101
    rrrr.rggg gggb.bbbb
    
    red = rrrr.r000 0000.0000 >> (16 - 8) = 0000.0000 rrrr.r000
    green = 0000.0ggg ggg0.0000 >> (11 - 8) = 0000.0000 gggg.gg00
    blue = 0000.0000 000b.bbbb << -(5 - 8) = 0000.0000 bbbb.b000

This is the second known layout:

    |second-| |-first-|
    1001.0110 1101.0101
    0000.rrrr gggg.bbbb
    
    red = 0000.rrrr 0000.0000 >> (12 - 8) = 0000.0000 rrrr.0000
    green = 0000.0000 gggg.0000 >> (8 - 8) = 0000.0000 gggg.0000
    blue = 0000.0000 000b.bbbb << -(4 - 8) = 0000.0000 bbbb.0000

The image itself has its origin in the bottom left corner, so it must be horizontally flipped.

### MCZ files

MCZ files contain 130 128x128 pixel images (SingleTile).

The MCZ file starts with with 260 4-byte values defining the size of each individual image in the file, each size is composed of two 32-bit integers (little endianness) which are added to get the length of each tile. The tiles itself are arranged (in order) in 13 columns and 10 rows with each being 128x128 pixels in size.

The JPEGs in the MCZ file are also little endianness, so they are BGR instead of RGB.

### NAT files

NAT files contain the models in a clear-text structure.

The NAT file is c lear-text file containing simple structure content, for example:

```
group
{
    # Comment
    subgroup
    {
        value1
        value2
        value3
    }
}
```

It always starts with the following header:

```
NAT
version 3.3

```

The possible structure follows.

Please note that the axis are the *assumed* for this application, the `x`-axis is left, `y`-axis is up and `z`-axis is depth.

#### hierarchy section

The `hierarchy` section contains the bone hierarchy for the model.

```
hierarchy
[
    # bones
]
```

A bone can contain multiple other bones.

##### bone-line

The definition of a single bone.

```
bone string-bone-name
{
    # sub-bones
}
```

##### t-line

Transformation/translation of the bone.

```
# t double-x double-y double-z
t 1.0 2.0 3.0
```

##### e-line

Unknown purpose, possible rotation definition?

This line is optional.

#### geometry_seg section

The `geometry_seg` section contains the definition of the model, vertices, texture coordinates, and face definitions. The `geometry_seg` section is split into multiple `seg` sections which form a part of the model.

##### seg

The `seg` section contains the actual definition of a part of the model.

```
seg string-bone-name
{
    # definitions
}
```

The vertice coordinates must be transformed/translated by the bone of the same name. Note that bones form a hierarchy, so all transformations/translations must be applied.

##### vertList

The `vertList` section contains the vertice definitions.

```
vertList int-number-of-vertices
{
    # vertice-definitions
}
```

##### v-line

A single vertice definition.

```
# v double-x double-y double-z
v 1.0 2.0 3.0
```

##### textList

The `textList` section contains the texture coordinates (UV).

```
textList int-number-of-texture-coordinates
{
    # texture-coordinates-definitions
}
```

##### t-line

A single texture coordinate definition.

```
# t double-u double-v
t 0.25 0.25
```

##### textureState

The state of the texture, whether it is applied or not. Most likely a switch whether this part of the model is visible or not.

```
# textureState state
textureState on
```

Possible values are `on` and `off`.

##### texture

The name of the texture file that should be used for this part of the model.

```
# texture string-texture-name
texture something1.tga
```

The name maps either to a INK file itself (extension replaced), for to an INK contained within the CNK with the same filename as the model.

##### polyList

The `polyList` section contains the face definitions of the model. It is a list of polygon vertices and texture coordinates, each three lines form a polygon.

```
polyList int-number-of-faces
{
    # face-vertice-definitions
{
```

##### f-line

A single vertice in the face definition.

```
# f int-vertice-index int-texture-coordinate-index
f 23 167
```

Three lines form a single polygon with the given coordinates.

#### geometry_physique section

The `geometry_physique` section contains the definition of the model, vertices, texture coordinates, and face definitions. It is most likely used for animated sections of models. The `geometry_physique` section is split into multiple `physique` sections which form a part of the model.

##### physique section

The `physique` section contains the actual definition of a part of the model.

It contains vertice definitions, texture coordinates, and afterwards textures and associated faces in order.

##### vertList section

The `vertList` section contains the vertice definitions.

```
vertList int-number-of-vertices
{
    # vertice-definitions
}
```

##### v_phys section

The `v_phys` section contains the definition of vertices.

```
v_phys int-number-of-vertices
{
    # vertice-definitions
}
```

##### xyz-line

The `xyz` line contains the actual definition of a vertice.

```
# xyz string-bone-name double-unknown double-x double-y double-z
xyz Bip01_Spine1 1.000000 1.560868 5.073912 -0.043177
```

Each vertice directly contains its associated bone name. The second field is unknown.

##### textList

The `textList` section contains the texture coordinates (UV).

```
textList int-number-of-texture-coordinates
{
    # texture-coordinates-definitions
}
```

##### t-line

A single texture coordinate definition.

```
# t double-u double-v
t 0.25 0.25
```

##### textureState

The state of the texture, whether it is applied or not. Most likely a switch whether this part of the model is visible or not.

```
# textureState state
textureState on
```

Possible values are `on` and `off`.

##### texture

The name of the texture file that should be used for this part of the model.

```
# texture string-texture-name
texture something1.tga
```

The name maps either to a INK file itself (extension replaced), for to an INK contained within the CNK with the same filename as the model.

##### polyList

The `polyList` section contains the face definitions of the model. It is a list of polygon vertices and texture coordinates, each three lines form a polygon.

```
polyList int-number-of-faces
{
    # face-vertice-definitions
{
```

##### f-line

A single vertice in the face definition.

```
# f int-vertice-index int-texture-coordinate-index
f 23 167
```

Three lines form a single polygon with the given coordinates.

#### polyStrip section

Unknown how to handle. Must be face definitions similar to `polyList` but in unknown format. Applying the same technique as for `polyList` does not work and does only yield garbage geometry *most of the time*. There are exceptions, for example the skull in `vol.nat`.

##### f-line

A single vertice in the face definition.

```
# f int-vertice-index int-texture-coordinate-index
f 23 167
```

### WMD files

WMD files contain the index data to stitch together multiple MCZ files into a map.

The WMD starts with a 40 byte header, which contains the following 32-bit integers (little endianness) after each other:

 1. 0x00: Unknown
 2. 0x04: int32 - Number of horizontal tiles.
 3. 0x08: int32 - Number of vertical tiles.
 4. 0x0C: Unknown
 5. 0x10: Unknown
 6. 0x14: Unknown
 7. 0x18: Unknown
 8. 0x1C: Unknown
 9. 0x20: int32 - Number of total map tiles.
 10. 0x24: Unknown

Afterwards follows a section with "number of map tiles" 128-Byte structures with each specifying a MCZ file which is used on this map. Only the first 14-byte are known to be the name (might be padded with nulls), the rest is unknown. Offsets are relative.

 1. 0x00: 14 byte string - Name of the entry, null terminated.
 2. 0x1e: Unknown data.

After this follows the index data to map each map tile to its location on the map, with the relative offset being the index/position of the tile and the value (32-bit integer (little endianness)) being the index from the map tile section above it. Offsets are relative.

 1. 0x00: int32 - Index of tile at position 0.
 2. 0x04: int32 - Index of tile at position 1.
 3. 0x08: int32 - Index of tile at position 2.
 4. ...

The index might be less than zero to indicate blank tiles.

The rest of the file contains unknown data.
